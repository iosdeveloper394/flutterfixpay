import 'dart:convert' as json;
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/shareWithFriend.dart';
import 'package:fixpay/Redeem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoyalityPointsQr extends StatefulWidget {
  @override
  _LoyalityPointsQrState createState() => _LoyalityPointsQrState();
}

class _LoyalityPointsQrState extends State<LoyalityPointsQr> {
  List list = new List();
  int totalPoints = 0;
  String imgaeProfile;
  String date = "Date";
  String nameBrand = "Name";

  @override
  void initState() {
    // TODO: implement initState
    apiMethod(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Image.asset(
                  'assets/images/asset3@3x.png',
                  color: Color(0xFF1dbcba),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    child: Image(
                      height: 80,
                      width: 80,
                      image: AssetImage('assets/images/12.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    SizedBox(
                      height: 50,
                    ),

                    Text(
                      "Loyalty points",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    // Container(
                    //   height: 100,
                    //   width: 100,
                    //   child:  Image(
                    //     height: 100,
                    //     width: 100,
                    //     image: AssetImage('assets/images/burger.png'),
                    //     fit: BoxFit.cover,
                    //   ),
                    //   decoration: BoxDecoration(
                    //       border: Border.all(color: Colors.orange),
                    //       color: Colors.white,
                    //       shape: BoxShape.circle
                    //   ),
                    // ),

                    Container(
                      height: 120,
                      width: 120,
                      child: imgaeProfile == null
                          ? Icon(
                              Icons.account_circle_outlined,
                              size: 120,
                              color: Color(0xFF1dbcba),
                            )
                          : CircleAvatar(
                              radius: 25.0,
                              backgroundImage: NetworkImage(
                                  'http://hustledev-001-site8.itempurl.com/' +
                                      imgaeProfile),
                              backgroundColor: Colors.transparent,
                            ),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.orange),
                          color: Colors.white,
                          shape: BoxShape.circle),
                    ),

                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      nameBrand.toString(),
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),

                    Text(
                      date.toString(),
                      style: TextStyle(fontSize: 8
                          // fontWeight: FontWeight.bold
                          ),
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    Container(
                      width: 200,
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 12.0),
                            child: Text(
                              "Total Points",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 45.0),
                            child: Text(
                              totalPoints.toString(),
                              style: TextStyle(
                                  color: Color(0xFF1dbcba),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                      ),
                      height: 50,
                    ),

                    SizedBox(
                      height: 10,
                    ),

                    Container(
                      width: 260,
                      height: 240,
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Text(
                              "Recent Points",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.all(8.0),
                              child: ListView.builder(
                                shrinkWrap: true,
                                padding: EdgeInsets.only(top: 10),
                                itemCount:
                                    list.length == null ? 0 : list.length,
                                itemBuilder: (BuildContext context, int index) {
                                  print("test1234 ${list}");
                                  // print(list.length);

                                  return listViewMethod(context, index);
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Redeem()),
                              );

                              // _openPopup(context) {

                              // showDialog(
                              //     context: context,
                              //     builder: (BuildContext context) {
                              //       return AlertDialog(
                              //         scrollable: true,
                              //         content: Padding(
                              //           padding: const EdgeInsets.all(8.0),
                              //           child: Form(
                              //             child: Column(
                              //               children: <Widget>[
                              //
                              //                 SizedBox(
                              //                   height: 30,
                              //                 ),
                              //
                              //                 GestureDetector(
                              //                   onTap: (){
                              //
                              //
                              //                     Navigator.push(
                              //                       context,
                              //                       MaterialPageRoute(builder: (context) => Redeem()),
                              //                     );
                              //                   },
                              //                   child: Container(
                              //                     width: 180,
                              //                     child:
                              //                     Center(
                              //                       child: Text("Redeem for myself?",
                              //                         style: TextStyle(
                              //                             color: Color(0xFFfdda65),
                              //                             fontWeight: FontWeight.bold
                              //                         ),
                              //
                              //                       ),
                              //                     ),
                              //                     decoration: BoxDecoration(
                              //                       borderRadius: BorderRadius.circular(5),
                              //                       color: Color(0xFF1dbcba),
                              //                     ),
                              //                     height: 40,
                              //                   ),
                              //                 ),
                              //
                              //
                              //                 SizedBox(
                              //                   height: 20,
                              //                 ),
                              //
                              //                 GestureDetector(
                              //                   onTap: (){
                              //
                              //
                              //                     Navigator.pushReplacement(
                              //                       context,
                              //                       MaterialPageRoute(builder: (context) => ShareWithFriend()),
                              //                     );
                              //
                              //                   },
                              //                   child: Container(
                              //                     width: 180,
                              //                     child:
                              //                     Center(
                              //                       child: Text("Share with a Friend?",
                              //                         style: TextStyle(
                              //                             color: Color(0xFF1dbcba),
                              //
                              //                             fontWeight: FontWeight.bold
                              //                         ),
                              //
                              //                       ),
                              //                     ),
                              //                     decoration: BoxDecoration(
                              //                       borderRadius: BorderRadius.circular(5),
                              //                       color: Color(0xFFfdda65),
                              //                     ),
                              //                     height: 40,
                              //                   ),
                              //                 ),
                              //
                              //                 SizedBox(
                              //                   height: 30,
                              //                 ),
                              //
                              //
                              //
                              //
                              //               ],
                              //             ),
                              //           ),
                              //         ),
                              //
                              //       );
                              //     });
                            },
                            child: Container(
                              width: 90,
                              height: 25,
                              child: Center(
                                child: Text(
                                  'Reedem',
                                  style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      color: Colors.black,
                                      fontSize: 17),
                                ),
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),
                                color: Color(0xFF1dbcba),
                              ),
                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    String _merchantId = merchantId.toString();

    var params = {'MerchantId': _merchantId};

    progressShow(context);

    await HttpsRequest()
        .fetchPostMerchantsForCustomer(Constant.loyaltyPoints, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        list = convertDataToJson['merchatto']['cashbackRecentList'];

        print("ye hai list loy${list}");

        totalPoints =
            convertDataToJson['merchatto']['totalBalance']['totalBalance'];
        date = convertDataToJson['merchatto']['merchant']['createDate'];
        nameBrand = convertDataToJson['merchatto']['merchant']['brandName'];
        imgaeProfile = convertDataToJson['merchatto']['merchant']['image'];

        String myDate = date;
        DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm").parse(myDate);
        date = "${tempDate.day} ${tempDate.month} ${tempDate.year}";
        print("ye hai date convert");
        print(date);

        SharedPreferences prefs13 = await SharedPreferences.getInstance();
        prefs13.setString('imgaeProfileRedeem', imgaeProfile);

        SharedPreferences prefs12 = await SharedPreferences.getInstance();
        prefs12.setString('nameBrandRedeem', nameBrand);

        SharedPreferences prefs14 = await SharedPreferences.getInstance();
        prefs14.setString('dateRedeem', date);

        setState(() {});
      } else {
        // list = new List();
        Fluttertoast.showToast(
            msg: "issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }

  Widget listViewMethod(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 5.0,
        ),
        decoration: BoxDecoration(
          color: Color(0xFFEBFFFE),
          boxShadow: [
            BoxShadow(
              blurRadius: 1.0,
              color: Colors.black26,
              offset: Offset(0.0, 1.0),
              spreadRadius: 1.0,
            )
          ],
        ),
        height: 40,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 25.0),
              child:
                  Container(width: 130, child: Text(list[index]['fullName'])),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Container(
                width: 50,
                height: 20,
                child: Center(
                  child: Text(
                    list[index]['amount'].toString(),
                    style: TextStyle(color: Colors.black, fontSize: 10),
                  ),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Color(0xFF1dbcba),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
