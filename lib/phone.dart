import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Redeem.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Phone extends StatefulWidget {
  @override
  _PhoneState createState() => _PhoneState();
}

class _PhoneState extends State<Phone> {
  final newPhoneNumber = TextEditingController();
  String phoneNum;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Data();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 80),
                  child: Image(
                    height: ResponsiveFlutter.of(context).verticalScale(140),
                    // width: 80,
                    image: AssetImage('assets/images/asset6@3x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: ResponsiveFlutter.of(context).verticalScale(98),
                        // child: Image(
                        //   image: AssetImage('assets/images/asset3@3x.png'),
                        // ),
                        child:
                        Image.asset('assets/images/asset3@3x.png',
                          color: Color(0xFF1dbcba),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(100),
                    ),
                    Container(
                      height: ResponsiveFlutter.of(context).verticalScale(160),
                      width: ResponsiveFlutter.of(context).scale(250),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment
                            .center, //Center Column contents vertically,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                            width:
                                ResponsiveFlutter.of(context).verticalScale(200),
                            child: Center(
                                child: Text(
                              phoneNum.toString(),
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                                color: Colors.blueGrey.shade800
                              ),
                            )),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFFFFF),
                                borderRadius: BorderRadius.circular(20)),
                          ),
                          SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(10)),
                          Container(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                            width:
                                ResponsiveFlutter.of(context).verticalScale(200),
                            child: Center(
                                child: TextField(
                              keyboardType: TextInputType.number,
                              controller: newPhoneNumber,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration.collapsed(
                                  hintText: "New Phone", hintStyle: TextStyle(

                                  fontFamily: 'Gotham',
                                  fontWeight: FontWeight.w300,
                                  fontSize:
                                  ResponsiveFlutter.of(context).fontSize(1.5),
                                  color: Colors.blueGrey.shade800
                              )),
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  fontWeight: FontWeight.w300,
                                  fontSize:
                                  ResponsiveFlutter.of(context).fontSize(1.5),
                                  color: Colors.blueGrey.shade800

                              ),
                            )),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFFFFF),
                                borderRadius: BorderRadius.circular(20)),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Color(0xFF1dbcba),
                          borderRadius: BorderRadius.circular(5)),
                    ),
                    SizedBox(
                        height: ResponsiveFlutter.of(context).verticalScale(160)),
                    GestureDetector(
                      onTap: () async {
                        String _currentPhone = phoneNum;
                        String _newPhone = newPhoneNumber.text;

                        if (_newPhone == null || _newPhone == "") {
                          Fluttertoast.showToast(
                              msg: "Write Your New Phone Number",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else {
                          if (_newPhone.length < 7) {
                            Fluttertoast.showToast(
                                msg: "Phone Number is Invalid",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);
                            return;
                          } else {
                            if (_newPhone.length > 12) {
                              Fluttertoast.showToast(
                                  msg: "Phone Number is Invalid",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                              return;
                            } else {
                              SharedPreferences prefs4 =
                                  await SharedPreferences.getInstance();
                              var id = prefs4.getString('cusId');
                              print(" ha ye id  ${id}");
                              print("ye hai new phone  ${_newPhone}");
                              print("ye hai Current phone ${_currentPhone}");

                              progressShow(context);

                              Map params = {
                                'CustomerId': id,
                                'OldPhoneNumber': _currentPhone,
                                'NewPhoneNumber': _newPhone,
                              };

                              await HttpsRequest()
                                  .fetchPostTokenData(
                                      Constant.changePhoneNum, params)
                                  .then((response) async {
                                progressHide(context);
                                print("asasas ${response.statusCode}");

                                if (response.statusCode == 200) {
                                  newPhoneNumber.clear();
                                  Fluttertoast.showToast(
                                      msg: "Phone Number Updated Successfully",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1);

                                  SharedPreferences prefs7 =
                                      await SharedPreferences.getInstance();
                                  prefs7.setString('phoneNum', _newPhone);

                                  Data();

                                  // gotoNextScreen(context);

                                } else {
                                  Fluttertoast.showToast(
                                      msg: "Write Your Correct Phone Number",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1);
                                }
                              });
                            }
                          }
                        }
                      },
                      child: Container(
                        height: ResponsiveFlutter.of(context).verticalScale(50),
                        width: ResponsiveFlutter.of(context).scale(220),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .center, //Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.save_alt,
                              size: ResponsiveFlutter.of(context).fontSize(4),
                              color: Color(0xFF1dbcba),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(70),
                            ),
                            Text(
                              ("Save Changes"),
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.8),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Color(0xFFFFFFFF),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Phone",
                    style: TextStyle(

                      color: Colors.white,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> Data() async {
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    phoneNum = prefs1.getString('phoneNum');
    print("ye hai phoneNum phone mai${phoneNum}");

    setState(() {});
  }
}
