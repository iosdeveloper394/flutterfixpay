import 'dart:convert' as json;
import 'dart:io';

import 'package:fixpay/BottomNavigation/SettingsNavBar.dart';
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ItemPinCode.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Redeem.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/myFriends.dart';
import 'package:fixpay/redeemPinCode.dart';
import 'package:fixpay/shareWithFriend.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TotalItems extends StatefulWidget {
  final bool isFromQr;
  final bool isFromPoint;
  final bool isFromItem = true;

  const TotalItems({
    Key key,
    this.isFromQr,
    this.isFromPoint,
  }) : super(key: key);
  @override
  _TotalItemsState createState() => _TotalItemsState();
}

class _TotalItemsState extends State<TotalItems> {
  List list = new List();
  int totalPoints = 0;
  String imgaeProfile =
      "http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg";
  String date = "12 3 2021";
  String nameBrand = "abc";

  @override
  void initState() {
    // TODO: implement initState
    // apiMethod(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: ResponsiveFlutter.of(context).verticalScale(98),

                // child: Image(
                //   image: AssetImage('assets/images/asset3@3x.png'),
                //   // fit: BoxFit.cover,
                // ),

                child: Image.asset(
                  'assets/images/asset3@3x.png',
                  color: Color(0xFF1dbcba),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                child: Image.asset(
                  'assets/images/asset6@2x.png',
                  height: ResponsiveFlutter.of(context).verticalScale(100),
                  color: Color(0xFFfdda65),
                ),

                // Image(
                //   height: ResponsiveFlutter.of(context).verticalScale(100),
                //   // width: 80,
                //   image: AssetImage('assets/images/asset6@2x.png'),
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(
                    top: ResponsiveFlutter.of(context).moderateScale(22)),
                child: Text(
                  "Total Items",
                  style: TextStyle(
                    fontFamily: 'Gotham',
                    fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: [
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(60),
                  ),

                  Container(
                    width: ResponsiveFlutter.of(context).scale(140),
                    height: ResponsiveFlutter.of(context).verticalScale(140),
                    child: imgaeProfile == null
                        ? Icon(
                            Icons.account_circle_outlined,
                            size: ResponsiveFlutter.of(context).fontSize(16),
                            color: Color(0xFF1dbcba),
                          )
                        : CircleAvatar(
                            radius: 25.0,
                            backgroundImage: NetworkImage(imgaeProfile),
                            backgroundColor: Colors.transparent,
                          ),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.orange),
                        color: Colors.white,
                        shape: BoxShape.circle),
                  ),

                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(12),
                  ),

                  Text(
                    nameBrand.toString(),
                    style: TextStyle(
                      // fontFamily: 'Gotham',
                      fontSize: ResponsiveFlutter.of(context).fontSize(3.2),
                      // fontWeight: FontWeight.bold
                      color: Colors.blueGrey.shade800,
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w400,
                    ),
                  ),

                  Text(
                    date.toString(),
                    style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                      // fontWeight: FontWeight.bold
                    ),
                  ),

                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(10),
                  ),

                  Container(
                    width: ResponsiveFlutter.of(context).scale(280),
                    height: ResponsiveFlutter.of(context).verticalScale(350),
                    child: Column(
                      children: [
                        SizedBox(
                          height:
                              ResponsiveFlutter.of(context).verticalScale(10),
                        ),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(
                                ResponsiveFlutter.of(context).moderateScale(8)),
                            child: ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(
                                  top: ResponsiveFlutter.of(context)
                                      .moderateScale(10)),
                              itemCount: list.length = 10,
                              // itemCount: list.length == null ? 0 : list.length,
                              itemBuilder: (BuildContext context, int index) {
                                // print("test1234 ${list}");
                                // print(list.length);

                                return listViewMethod(context, index);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                    ),
                  ),

                  // new GestureDetector(
                  //   onTap: (){
                  //
                  //     _openPopup(context);
                  //   },
                  //   child: Container(
                  //     width: 260,
                  //
                  //     child:
                  //
                  //     Row(
                  //       children: [
                  //         SizedBox(
                  //           height: 10,
                  //         ),
                  //         Padding(
                  //           padding: const EdgeInsets.only(left:30.0),
                  //           child: Text("Whooper with fries",
                  //             style: TextStyle(
                  //                 fontSize: 12
                  //             ),),
                  //         ),
                  //
                  //
                  //         Padding(
                  //           padding: const EdgeInsets.only(left:50.0),
                  //           child: Container(
                  //             width: 60,
                  //             height: 20,
                  //             child: Center(
                  //               child: Text(
                  //                 'Reedem',
                  //                 style: TextStyle(color: Colors.black,
                  //                     fontSize: 12,
                  //                     fontWeight: FontWeight.bold),
                  //               ),
                  //             ),
                  //             decoration: BoxDecoration(
                  //               borderRadius: BorderRadius.circular(5),
                  //               color: Color(0xFF1dbcba),
                  //             ),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //
                  //     decoration: BoxDecoration(
                  //       borderRadius: BorderRadius.circular(10),
                  //       color: Colors.white,
                  //
                  //     ),
                  //     height: 40,
                  //   ),
                  // ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // _openPopup(context) {
  //
  //
  //   showDialog(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return AlertDialog(
  //           scrollable: true,
  //           content: Padding(
  //             padding: const EdgeInsets.all(8.0),
  //             child: Form(
  //               child: Column(
  //                 children: <Widget>[
  //
  //                   SizedBox(
  //                     height: 30,
  //                   ),
  //
  //                   Container(
  //                     width: 180,
  //                     child:
  //                         Center(
  //                           child: Text("Generate QR",
  //                             style: TextStyle(
  //                                 color: Color(0xFFfdda65),
  //                                 fontWeight: FontWeight.bold
  //                             ),
  //
  //                           ),
  //                         ),
  //                     decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.circular(5),
  //                       color: Color(0xFF1dbcba),
  //                     ),
  //                     height: 40,
  //                   ),
  //
  //
  //                   SizedBox(
  //                     height: 20,
  //                   ),
  //
  //                   Container(
  //                     width: 180,
  //                     child:
  //                     Center(
  //                       child: Text("Generate QR",
  //                         style: TextStyle(
  //                             color: Color(0xFFfdda65),
  //                             fontWeight: FontWeight.bold
  //                         ),
  //
  //                       ),
  //                     ),
  //                     decoration: BoxDecoration(
  //                       borderRadius: BorderRadius.circular(5),
  //                       color: Color(0xFF1dbcba),
  //                     ),
  //                     height: 40,
  //                   ),
  //
  //                   SizedBox(
  //                     height: 30,
  //                   ),
  //
  //
  //
  //
  //                 ],
  //               ),
  //             ),
  //           ),
  //
  //         );
  //       });
  // }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    String _merchantId = merchantId.toString();

    var params = {'MerchantId': _merchantId};

    progressShow(context);

    await HttpsRequest()
        .fetchPostMerchantsForCustomer(Constant.totalItems, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        list = convertDataToJson['merchatto']['cashbackRecentItemList'];

        print("ye hai list loy${list}");

        // totalPoints = convertDataToJson['merchatto']['totalBalance']['totalBalance'];
        date = convertDataToJson['merchatto']['merchant']['createDate'];
        nameBrand = convertDataToJson['merchatto']['merchant']['brandName'];
        imgaeProfile = convertDataToJson['merchatto']['merchant']['image'];

        String myDate = date;
        DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm").parse(myDate);
        date = "${tempDate.day} ${tempDate.month} ${tempDate.year}";
        print("ye hai date convert");
        print(date);

        SharedPreferences prefs13 = await SharedPreferences.getInstance();
        prefs13.setString('imgaeProfileRedeem', imgaeProfile);

        SharedPreferences prefs12 = await SharedPreferences.getInstance();
        prefs12.setString('nameBrandRedeem', nameBrand);

        SharedPreferences prefs14 = await SharedPreferences.getInstance();
        prefs14.setString('dateRedeem', date);

        setState(() {});
      } else if (response.statusCode == 500) {
        // list = new List();
        // Fluttertoast.showToast(
        //     msg: "No Item",
        //     toastLength: Toast.LENGTH_SHORT,
        //     gravity: ToastGravity.CENTER,
        //     timeInSecForIosWeb: 1);
      } else {
        Fluttertoast.showToast(
            msg: "No Inter Net",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }

  Widget listViewMethod(BuildContext context, int index) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(
              top: ResponsiveFlutter.of(context).moderateScale(10)),
          child: GestureDetector(
            onTap: () {
              widget.isFromQr != true
                  ? showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          scrollable: true,
                          content: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Form(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Reedem Your Items",
                                    style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(2.8),

                                      color: Colors.blueGrey.shade800,
                                      // fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(3),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(15),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RedeemPinCode()),
                                      );
                                    },
                                    child: Container(
                                      width: ResponsiveFlutter.of(context)
                                          .scale(200),
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(50),
                                      child: Center(
                                        child: Text(
                                          "Redeem for Yourself?",
                                          style: TextStyle(
                                              fontFamily: 'Gotham',
                                              color: Color(0xFFfdda65),
                                              fontWeight: FontWeight.w300,
                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(2)),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Color(0xFF1dbcba),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(10),
                                  ),
                                  Text(
                                    "OR",
                                    style: TextStyle(
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w300,
                                        fontSize: ResponsiveFlutter.of(context)
                                            .fontSize(2)),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(10),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      // Navigator.pushReplacement(
                                      //   context,
                                      //   MaterialPageRoute(builder: (context) => ShareWithFriend(isFromItem: true)),
                                      // );

                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              scrollable: true,
                                              content: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Form(
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                          "Share your Items",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .blueGrey
                                                                  .shade800,
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          2.8),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(3),
                                                        ),
                                                        Text(
                                                          "Share Via",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 12),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  15),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            // widget.isFromItem != true?
                                                            //
                                                            // Navigator.pushReplacement(
                                                            //   context,
                                                            //   MaterialPageRoute(builder: (context) => SettingsBottomNav(isFromMerchant: true)),
                                                            // ):

                                                            Navigator
                                                                .pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      SettingsBottomNav(
                                                                          isFromItem:
                                                                              true)),
                                                            );
                                                          },
                                                          child: Container(
                                                            width: ResponsiveFlutter
                                                                    .of(context)
                                                                .scale(200),
                                                            height: ResponsiveFlutter
                                                                    .of(context)
                                                                .verticalScale(
                                                                    50),
                                                            child: Center(
                                                              child: Text(
                                                                "Profile QR",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Gotham',
                                                                    color: Color(
                                                                        0xFFfdda65),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300),
                                                              ),
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Color(
                                                                  0xFF1dbcba),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  10),
                                                        ),
                                                        Text(
                                                          "OR",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          2)),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  10),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            Navigator
                                                                .pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => MyFriends(
                                                                      isFromItem:
                                                                          true,
                                                                      isFromShareFriend:
                                                                          true)),
                                                            );
                                                          },
                                                          child: Container(
                                                            width: ResponsiveFlutter
                                                                    .of(context)
                                                                .scale(200),
                                                            height: ResponsiveFlutter
                                                                    .of(context)
                                                                .verticalScale(
                                                                    50),
                                                            child: Center(
                                                              child: Text(
                                                                "Select Your Friend",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Gotham',
                                                                    color: Color(
                                                                        0xFF1dbcba),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300),
                                                              ),
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Color(
                                                                  0xFFfdda65),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  15),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            );
                                          });
                                    },
                                    child: Container(
                                      width: ResponsiveFlutter.of(context)
                                          .scale(200),
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(50),
                                      child: Center(
                                        child: Text(
                                          "Share with a Friend?",
                                          style: TextStyle(
                                              fontFamily: 'Gotham',
                                              color: Color(0xFF1dbcba),
                                              fontWeight: FontWeight.w300,
                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(2)),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Color(0xFFfdda65),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(15),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      })
                  : Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RedeemPinCode()),
                    );
            },
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: 5.0,
              ),
              decoration: BoxDecoration(
                color: Color(0xFFEBFFFE),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 1.0,
                    color: Colors.black26,
                    offset: Offset(0.0, 1.0),
                    spreadRadius: 1.0,
                  )
                ],
              ),
              height: ResponsiveFlutter.of(context).verticalScale(50),
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Pizza".toString(),
                    // Text(list[index]['brandName'].toString(),

                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        color: Colors.blueGrey.shade800,
                        // fontFamily: 'Gotham',
                        fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                  ),
                  SizedBox(
                    width: ResponsiveFlutter.of(context).scale(110),
                  ),
                  Container(
                    width: ResponsiveFlutter.of(context).scale(70),
                    height: ResponsiveFlutter.of(context).verticalScale(26),
                    child: Center(
                      child: Text(
                        "Reedem",
                        // list[index]['amount'].toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w400,
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.8)),
                      ),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF1dbcba),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              top: ResponsiveFlutter.of(context).moderateScale(10)),
          child: GestureDetector(
            onTap: () {
              widget.isFromQr != true
                  ? showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          scrollable: true,
                          content: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Form(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Reedem Your Items",
                                    style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(2.8),

                                      color: Colors.blueGrey.shade800,
                                      // fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(3),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(15),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RedeemPinCode()),
                                      );
                                    },
                                    child: Container(
                                      width: ResponsiveFlutter.of(context)
                                          .scale(200),
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(50),
                                      child: Center(
                                        child: Text(
                                          "Redeem for Yourself?",
                                          style: TextStyle(
                                              fontFamily: 'Gotham',
                                              color: Color(0xFFfdda65),
                                              fontWeight: FontWeight.w300,
                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(2)),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Color(0xFF1dbcba),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(10),
                                  ),
                                  Text(
                                    "OR",
                                    style: TextStyle(
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w300,
                                        fontSize: ResponsiveFlutter.of(context)
                                            .fontSize(2)),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(10),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      // Navigator.pushReplacement(
                                      //   context,
                                      //   MaterialPageRoute(builder: (context) => ShareWithFriend(isFromItem: true)),
                                      // );

                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              scrollable: true,
                                              content: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Form(
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                          "Share your Items",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .blueGrey
                                                                  .shade800,
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          2.8),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w400),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(3),
                                                        ),
                                                        Text(
                                                          "Share Via",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 12),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  15),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            // widget.isFromItem != true?
                                                            //
                                                            // Navigator.pushReplacement(
                                                            //   context,
                                                            //   MaterialPageRoute(builder: (context) => SettingsBottomNav(isFromMerchant: true)),
                                                            // ):

                                                            Navigator
                                                                .pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      SettingsBottomNav(
                                                                          isFromItem:
                                                                              true)),
                                                            );
                                                          },
                                                          child: Container(
                                                            width: ResponsiveFlutter
                                                                    .of(context)
                                                                .scale(200),
                                                            height: ResponsiveFlutter
                                                                    .of(context)
                                                                .verticalScale(
                                                                    50),
                                                            child: Center(
                                                              child: Text(
                                                                "Profile QR",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Gotham',
                                                                    color: Color(
                                                                        0xFFfdda65),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300),
                                                              ),
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Color(
                                                                  0xFF1dbcba),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  10),
                                                        ),
                                                        Text(
                                                          "OR",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          2)),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  10),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            Navigator
                                                                .pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => MyFriends(
                                                                      isFromItem:
                                                                          true,
                                                                      isFromShareFriend:
                                                                          true)),
                                                            );
                                                          },
                                                          child: Container(
                                                            width: ResponsiveFlutter
                                                                    .of(context)
                                                                .scale(200),
                                                            height: ResponsiveFlutter
                                                                    .of(context)
                                                                .verticalScale(
                                                                    50),
                                                            child: Center(
                                                              child: Text(
                                                                "Select Your Friend",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Gotham',
                                                                    color: Color(
                                                                        0xFF1dbcba),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300),
                                                              ),
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Color(
                                                                  0xFFfdda65),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  15),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            );
                                          });
                                    },
                                    child: Container(
                                      width: ResponsiveFlutter.of(context)
                                          .scale(200),
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(50),
                                      child: Center(
                                        child: Text(
                                          "Share with a Friend?",
                                          style: TextStyle(
                                              fontFamily: 'Gotham',
                                              color: Color(0xFF1dbcba),
                                              fontWeight: FontWeight.w300,
                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(2)),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Color(0xFFfdda65),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(15),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      })
                  : Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RedeemPinCode()),
                    );
            },
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: 5.0,
              ),
              decoration: BoxDecoration(
                color: Color(0xFFEBFFFE),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 1.0,
                    color: Colors.black26,
                    offset: Offset(0.0, 1.0),
                    spreadRadius: 1.0,
                  )
                ],
              ),
              height: ResponsiveFlutter.of(context).verticalScale(50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Ice Cream".toString(),
                    // Text(list[index]['brandName'].toString(),

                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        color: Colors.blueGrey.shade800,
                        // fontFamily: 'Gotham',
                        fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                  ),
                  SizedBox(
                    width: ResponsiveFlutter.of(context).scale(80),
                  ),
                  Container(
                    width: ResponsiveFlutter.of(context).scale(70),
                    height: ResponsiveFlutter.of(context).verticalScale(26),
                    child: Center(
                      child: Text(
                        "Reedem",
                        // list[index]['amount'].toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w400,
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.8)),
                      ),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF1dbcba),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              top: ResponsiveFlutter.of(context).moderateScale(10)),
          child: GestureDetector(
            onTap: () {
              widget.isFromQr != true
                  ? showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          scrollable: true,
                          content: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Form(
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "Share your Items",
                                    style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(2.8),

                                      color: Colors.blueGrey.shade800,
                                      // fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(3),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(30),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RedeemPinCode()),
                                      );
                                    },
                                    child: Container(
                                      width: ResponsiveFlutter.of(context)
                                          .scale(200),
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(50),
                                      child: Center(
                                        child: Text(
                                          "Redeem for Yourself?",
                                          style: TextStyle(
                                              fontFamily: 'Gotham',
                                              color: Color(0xFFfdda65),
                                              fontWeight: FontWeight.w300),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Color(0xFF1dbcba),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(10),
                                  ),
                                  Text(
                                    "OR",
                                    style: TextStyle(
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w300,
                                        fontSize: ResponsiveFlutter.of(context)
                                            .fontSize(2)),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(10),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      // Navigator.pushReplacement(
                                      //   context,
                                      //   MaterialPageRoute(builder: (context) => ShareWithFriend(isFromItem: true)),
                                      // );

                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return AlertDialog(
                                              scrollable: true,
                                              content: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Form(
                                                  child: Align(
                                                    alignment: Alignment.center,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                          "Share your Items",
                                                          style: TextStyle(
                                                            fontFamily:
                                                                'Gotham',
                                                            fontSize:
                                                                ResponsiveFlutter.of(
                                                                        context)
                                                                    .fontSize(
                                                                        2.8),

                                                            color: Colors
                                                                .blueGrey
                                                                .shade800,
                                                            // fontFamily: 'Gotham',
                                                            fontWeight:
                                                                FontWeight.w400,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(3),
                                                        ),
                                                        Text(
                                                          "Share Via",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize: 12),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  15),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            // widget.isFromItem != true?
                                                            //
                                                            // Navigator.pushReplacement(
                                                            //   context,
                                                            //   MaterialPageRoute(builder: (context) => SettingsBottomNav(isFromMerchant: true)),
                                                            // ):

                                                            Navigator
                                                                .pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      SettingsBottomNav(
                                                                          isFromItem:
                                                                              true)),
                                                            );
                                                          },
                                                          child: Container(
                                                            width: ResponsiveFlutter
                                                                    .of(context)
                                                                .scale(200),
                                                            height: ResponsiveFlutter
                                                                    .of(context)
                                                                .verticalScale(
                                                                    50),
                                                            child: Center(
                                                              child: Text(
                                                                "Profile QR",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Gotham',
                                                                    color: Color(
                                                                        0xFFfdda65),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300),
                                                              ),
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Color(
                                                                  0xFF1dbcba),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  10),
                                                        ),
                                                        Text(
                                                          "OR",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300,
                                                              fontSize:
                                                                  ResponsiveFlutter.of(
                                                                          context)
                                                                      .fontSize(
                                                                          2)),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  10),
                                                        ),
                                                        GestureDetector(
                                                          onTap: () {
                                                            Navigator
                                                                .pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) => MyFriends(
                                                                      isFromItem:
                                                                          true,
                                                                      isFromShareFriend:
                                                                          true)),
                                                            );
                                                          },
                                                          child: Container(
                                                            width: ResponsiveFlutter
                                                                    .of(context)
                                                                .scale(200),
                                                            height: ResponsiveFlutter
                                                                    .of(context)
                                                                .verticalScale(
                                                                    50),
                                                            child: Center(
                                                              child: Text(
                                                                "Select Your Friend",
                                                                style: TextStyle(
                                                                    fontFamily:
                                                                        'Gotham',
                                                                    color: Color(
                                                                        0xFF1dbcba),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300),
                                                              ),
                                                            ),
                                                            decoration:
                                                                BoxDecoration(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          5),
                                                              color: Color(
                                                                  0xFFfdda65),
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: ResponsiveFlutter
                                                                  .of(context)
                                                              .verticalScale(
                                                                  15),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            );
                                          });
                                    },
                                    child: Container(
                                      width: ResponsiveFlutter.of(context)
                                          .scale(200),
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(50),
                                      child: Center(
                                        child: Text(
                                          "Share with a Friend?",
                                          style: TextStyle(
                                              fontFamily: 'Gotham',
                                              color: Color(0xFF1dbcba),
                                              fontWeight: FontWeight.w300),
                                        ),
                                      ),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Color(0xFFfdda65),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(15),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      })
                  : Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RedeemPinCode()),
                    );
            },
            child: Container(
              margin: EdgeInsets.symmetric(
                horizontal: 5.0,
              ),
              decoration: BoxDecoration(
                color: Color(0xFFEBFFFE),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 1.0,
                    color: Colors.black26,
                    offset: Offset(0.0, 1.0),
                    spreadRadius: 1.0,
                  )
                ],
              ),
              height: ResponsiveFlutter.of(context).verticalScale(50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Burger".toString(),
                    // Text(list[index]['brandName'].toString(),

                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        color: Colors.blueGrey.shade800,
                        // fontFamily: 'Gotham',
                        fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                  ),
                  SizedBox(
                    width: ResponsiveFlutter.of(context).scale(100),
                  ),
                  Container(
                    width: ResponsiveFlutter.of(context).scale(70),
                    height: ResponsiveFlutter.of(context).verticalScale(26),
                    child: Center(
                      child: Text(
                        "Reedem",
                        // list[index]['amount'].toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w400,
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.8)),
                      ),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xFF1dbcba),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
