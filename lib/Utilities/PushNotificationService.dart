import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io' show Platform;
import 'package:fixpay/SessionManager/SessionManager.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'dart:async';

class PushNotificationService {
  final FirebaseMessaging _fcm;

  PushNotificationService(this._fcm);
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  Future initialise() async {
    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }

    // If you want to test the push notification locally,
    // you need to get the token and input to the Firebase console
    // https://console.firebase.google.com/project/YOUR_PROJECT_ID/notification/compose
    String token = await _fcm.getToken();
    SessionManager().setFirebaseToken(token);
    print("FirebaseMessaging token: $token");

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        // final title = message['notification']['title'] ?? '';
        // final body = message['notification']['body'] ?? '';
        //response : {google.c.sender.id: 863296286876, google.c.a.c_l: Hello, google.c.a.e: 1, aps: {alert: {title: Hello, body: testing}, badge: 0.0, mutable-content: 1}, gcm.n.e: 1, google.c.a.c_id: 5522225035077780437, gcm.message_id: 1622797643716576, google.c.a.udt: 0, google.c.a.ts: 1622797643}
        print("onMessage: $message");
        final title = message['aps']['alert']['title'] ?? '';
        final body = message['aps']['alert']['body'] ?? '';
        
        phoneNumberCheckerForNotification(title, body);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
      },
    );
  }

  void phoneNumberCheckerForNotification(title, body) {
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettingsMAC = new MacOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: initializationSettingsMAC);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    // print("calling A");
    showNotification(title, body);
  }

  Future<void> _demoNotification(String title, String body) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'channel_ID', 'channel name', 'channel description',
        importance: Importance.max,
        playSound: true,
        // sound: RawResourceAndroidNotificationSound(sound),
        showProgress: true,
        enableVibration: true,
        styleInformation: BigTextStyleInformation(''),
        priority: Priority.high,
        ticker: 'test ticker');

    var iOSChannelSpecifics = IOSNotificationDetails();
    var MACChannelSpecifics = MacOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSChannelSpecifics,
        macOS: MACChannelSpecifics);


    // print("calling B");
    await flutterLocalNotificationsPlugin
        .show(0, title, body, platformChannelSpecifics, payload: 'test');
  }

  // Future onSelectNotification(String payload) async {
  //   showDialog(
  //     context: context,
  //     builder: (_) {
  //       return new AlertDialog(
  //         title: Text("Verification Code"),
  //         content: Text("${widget.phoneNo}"),
  //       );
  //     },
  //   );
  // }

  void showNotification(String title, String body) async {
    await _demoNotification(title, body);
  }
}