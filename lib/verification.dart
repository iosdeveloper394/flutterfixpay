import 'dart:async';
import 'dart:convert' as json;
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/Newpassword.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/signIn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VerificationScreen extends StatefulWidget {
  final Color backgroundColor = Colors.white;
  final TextStyle styleTextUnderTheLoader = TextStyle(
      fontFamily: 'Gotham',
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      color: Colors.black);

  @override
  _VerificationScreenState createState() => _VerificationScreenState();
}

class _VerificationScreenState extends State<VerificationScreen> {
  final pinCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          // width: screenSize.width,

          child: Column(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: ResponsiveFlutter.of(context).verticalScale(98),
                  child: Image(
                    image: AssetImage('assets/images/asset3@3x.png'),
                  ),
                ),
              ),
              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),
              Text(
                "Verification",
                style: TextStyle(
                    fontFamily: 'Gotham',
                    fontSize: ResponsiveFlutter.of(context).fontSize(3),
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),
              Image(
                height: ResponsiveFlutter.of(context).verticalScale(110),
                width: ResponsiveFlutter.of(context).verticalScale(110),
                image: AssetImage('assets/images/asset13@3x.png'),
                // fit: BoxFit.cover,
              ),
              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "Enter the verification code that",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          fontSize: ResponsiveFlutter.of(context).fontSize(3),
                          color: Colors.grey),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "we just sent you on your",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          fontSize: ResponsiveFlutter.of(context).fontSize(3),
                          color: Colors.grey),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "email address or phone",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          fontSize: ResponsiveFlutter.of(context).fontSize(3),
                          color: Colors.grey),
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "number",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          fontSize: ResponsiveFlutter.of(context).fontSize(3),
                          color: Colors.grey),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),
              Container(
                // margin: EdgeInsets.symmetric(horizontal: 80),

                width: ResponsiveFlutter.of(context).scale(160),
                child: PinCodeTextField(
                  keyboardType: TextInputType.number,
                  controller: pinCodeController,
                  backgroundColor: Color(0xFFEBFFFE),
                  length: 4,
                  obscureText: false,
                  animationType: AnimationType.fade,
                  pinTheme: PinTheme(
                    shape: PinCodeFieldShape.box,
                    inactiveFillColor: Color(0xFF1dbcba),
                    inactiveColor: Color(0xFF1dbcba),
                    activeFillColor: Color(0xFF1dbcba),
                    selectedColor: Color(0xFF1dbcba),

                    selectedFillColor: Color(0xFF1dbcba),

                    activeColor: Color(0xFF1dbcba),
                    // selectedColor: Colors.blueGrey,
                    // borderRadius: BorderRadius.circular(0),
                    fieldHeight: 30,
                    fieldWidth: 30,
                    // activeFillColor: Colors.black,
                  ),
                  animationDuration: Duration(milliseconds: 300),
                  cursorColor: Color(0xFF1dbcba),
                  enableActiveFill: true,
                  // errorAnimationController: errorController,
                  // controller: textEditingController,
                  onCompleted: (v) {
                    print("Completed");
                  },
                  // onChanged: (value) {
                  //   print(value);
                  //   setState(() {
                  //     var currentText = value;
                  //   });
                  // },
                  beforeTextPaste: (text) {
                    print("Allowing to paste $text");
                    //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                    //but you can show anything you want here, like your pop up saying wrong paste format or etc
                    return true;
                  },
                ),
              ),
              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(10),
              ),
              Container(
                height: ResponsiveFlutter.of(context).verticalScale(35),
                width: ResponsiveFlutter.of(context).scale(150),
                child: RaisedButton(
                  color: Color(0xFFfad668),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  onPressed: () async {
                    SharedPreferences prefs4 =
                        await SharedPreferences.getInstance();
                    var email = prefs4.getString('emailForget');
                    print("email ha ye ${email}");

                    String _pinCode = pinCodeController.text;
                    print("aaaa${_pinCode}");

                    var myInt = int.parse(_pinCode);
                    assert(myInt is int);

                    print("myInt${myInt}"); // 12345

                    Map params = {'Email': email, 'VerificationCode': myInt};

                    print("aaaa${myInt}");

                    progressShow(context);
                    await HttpsRequest()
                        .fetchPostForgetPass(Constant.verifiCode, params)
                        .then((response) async {
                      progressHide(context);

                      print("asasas ${response.statusCode}");
                      if (response.statusCode == 200) {
                        var convertDataToJson = json.jsonDecode(response.body);
                        var id = convertDataToJson['id'];
                        print(" ha ye responce  ${convertDataToJson}");
                        print(" ha ye id  ${id}");

                        SharedPreferences prefs1 =
                            await SharedPreferences.getInstance();
                        prefs1.setString('cusId', id);

                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => NewPassword()),
                        );
                      } else {
                        Fluttertoast.showToast(
                            msg: "Enter Correct Verification Code",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1);
                      }
                    });
                  },
                  child: Text(
                    "Verify",
                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                        color: Colors.white),
                  ),
                ),
              ),
              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "If you did'nt recieve code ",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          fontSize: ResponsiveFlutter.of(context).fontSize(3),
                          color: Colors.grey),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(10),
              ),
              GestureDetector(
                onTap: () async {
                  SharedPreferences prefs4 =
                      await SharedPreferences.getInstance();
                  var email = prefs4.getString('emailForget');
                  print("email ha ye ${email}");

                  String _email = email;

                  Map params = {'Email': _email};

                  print("aaaa${_email}");

                  progressShow(context);
                  await HttpsRequest()
                      .fetchPostForgetPass(Constant.forgetPass, params)
                      .then((response) async {
                    progressHide(context);

                    print("asasas ${response.statusCode}");
                    if (response.statusCode == 200) {
                      Fluttertoast.showToast(
                          msg: "Verification Code Sent",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1);
                    } else {
                      Fluttertoast.showToast(
                          msg: "No Internet",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1);
                    }
                  });
                },
                child: Container(
                  child: Align(
                    alignment: Alignment.center,
                    child: Center(
                      child: Text(
                        "RESEND",
                        style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.bold,
                            fontSize: ResponsiveFlutter.of(context).fontSize(3),
                            color: Colors.red),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
