import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/SessionManager/SessionManager.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:passwordfield/passwordfield.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as json;

Size displaySize(BuildContext context) {
  debugPrint('Size = ' + MediaQuery.of(context).size.toString());
  return MediaQuery.of(context).size;
}

double displayHeight(BuildContext context) {
  debugPrint('Height = ' + displaySize(context).height.toString());
  return displaySize(context).height;
}

double displayWidth(BuildContext context) {
  debugPrint('Width = ' + displaySize(context).width.toString());
  return displaySize(context).width;
}

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  String tokenNew;
  String username;
  String id;
  String token;
  String email;
  String fullName;
  String phoneNum;
  String points;
  String items;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final emailUsernameTextFieldController = TextEditingController();
  final passwordTextFieldController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseCloudMessaging_Listeners();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: ResponsiveFlutter.of(context).moderateScale(80)),
                  child: Image.asset(
                    'assets/images/asset113x.png',
                    height: ResponsiveFlutter.of(context).verticalScale(120),
                    color: Color(0xFF1dbcba),
                  ),

                  // Image(
                  //   height: ResponsiveFlutter.of(context).verticalScale(140),
                  //   // width: 80,
                  //   image: AssetImage('assets/images/asset113x.png'),
                  //   // fit: BoxFit.cover,
                  // ),
                ),
              ),

              // SizedBox(
              //   height: 180,
              // ),

              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: ResponsiveFlutter.of(context).moderateScale(20)),
                  child: Image.asset(
                    'assets/images/asset5@3x.png',
                    height: ResponsiveFlutter.of(context).verticalScale(120),
                    color: Color(0xFFfdda65),
                  ),

                  // Image(
                  //   height: ResponsiveFlutter.of(context).verticalScale(120),
                  //   // width: 80,
                  //   image: AssetImage('assets/images/asset5@3x.png'),
                  //   // fit: BoxFit.cover,
                  // ),
                ),
              ),

              Column(
                children: [
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(120),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(360),
                      width: ResponsiveFlutter.of(context).scale(270),

                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25.0),
                        boxShadow: kElevationToShadow[0],
                      ),
                      // height: 320,
                      // margin: EdgeInsets.symmetric(horizontal: 40.0, vertical: 0),

                      child: Column(
                        children: [
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(60),
                          ),
                          Center(
                            child: Container(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(100),
                              width: ResponsiveFlutter.of(context).scale(90),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Color(0xFF1dbcba),
                              ),
                              child: Center(
                                child: Text(
                                  "LOGO",
                                  style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      // fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(3.0)),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                          ),
                          Container(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(45),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      ResponsiveFlutter.of(context).scale(20)),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Color(0xFFF2F7F6),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(20),
                                  ),
                                  Container(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(22),
                                    width:
                                        ResponsiveFlutter.of(context).scale(22),
                                    child: Image(
                                      image: AssetImage(
                                          'assets/images/artboard16.png'),
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(20),
                                  ),
                                  Expanded(
                                    child: Container(
                                      // height: 30,
                                      // width: 150,
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: TextField(
                                          maxLines: 1,
                                          controller:
                                              emailUsernameTextFieldController,
                                          style: TextStyle(
                                              color: Color(0XFF5d5d5d),
                                              fontFamily: 'Gotham',
                                              fontWeight: FontWeight.w300,
                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(1.5)),
                                          decoration: InputDecoration(
                                              isDense: true,
                                              border: InputBorder.none,
                                              hintText:
                                                  'Email Address/ Username '),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(20),
                          ),
                          Container(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(45),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      ResponsiveFlutter.of(context).scale(20)),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Color(0xFFF2F7F6),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                  ),
                                  Container(
                                    child: Image(
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(22),
                                      width: ResponsiveFlutter.of(context)
                                          .scale(22),
                                      image: AssetImage(
                                          'assets/images/artboard17.png'),
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(20),
                                  ),
                                  Expanded(
                                    child: Container(
                                      // height: 30,
                                      // width: 150,
                                      child: TextField(
                                        style: TextStyle(
                                            color: Color(0XFF5d5d5d),
                                            fontFamily: 'Gotham',
                                            fontWeight: FontWeight.w300,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(1.5)),
                                        controller: passwordTextFieldController,
                                        decoration: InputDecoration(
                                            isDense: true,
                                            border: InputBorder.none,
                                            hintText: 'Password'),
                                      ),

                                      // PasswordField(
                                      //   controller: passwordTextFieldController,
                                      //   inputStyle: TextStyle(
                                      // fontFamily: 'Gotham',
                                      //         fontWeight: FontWeight.w300,
                                      //       fontSize: ResponsiveFlutter.of(context).fontSize(1.5)
                                      //   ),
                                      // ),
                                    ),
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(30),
                  ),
                  Container(
                    height: ResponsiveFlutter.of(context).verticalScale(45),
                    width: ResponsiveFlutter.of(context).scale(180),
                    child: RaisedButton(
                      color: Color(0xFFfdda65),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18)),
                      onPressed: () async {
                        print("tokan11 ${tokenNew}");

                        String _emailUsername =
                            emailUsernameTextFieldController.text.toString();

                        String _password =
                            passwordTextFieldController.text.toString();

                        tokenNew =
                            "MyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseToken";

                        print("signemail ${_emailUsername}");
                        print("_password ${_password}");

                        if (_emailUsername == null || _emailUsername == "") {
                          Fluttertoast.showToast(
                            msg: "please write your User name or Email",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                          );
                          return;
                        } else {
                          if (_password == null || _password == "") {
                            Fluttertoast.showToast(
                                msg: "please write password",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);
                            return;
                          } else {
                            progressShow(context);
                            Map params = {
                              'Email': _emailUsername,
                              'Password': _password,
                              'FirebaseToken': tokenNew
                            };

                            await HttpsRequest()
                                .fetchPostData(Constant.login, params)
                                .then((response) async {
                              progressHide(context);
                              print("asasas ${response.statusCode}");

                              if (response.statusCode == 200) {
                                var convertDataToJson =
                                    json.jsonDecode(response.body);
                                username = convertDataToJson['customerResponse']
                                    ['username'];
                                fullName = convertDataToJson['customerResponse']
                                    ['fullName'];
                                token = convertDataToJson['customerResponse']
                                    ['token'];
                                email = convertDataToJson['customerResponse']
                                    ['email'];
                                phoneNum = convertDataToJson['customerResponse']
                                    ['phoneNumber'];
                                id =
                                    convertDataToJson['customerResponse']['id'];
                                points = convertDataToJson['customerResponse']
                                    ['totalBalance'];
                                items = convertDataToJson['customerResponse']
                                    ['totalItem'];
                                var image =
                                    convertDataToJson['customerResponse']
                                        ['image'];

                                print("naam hai ${points}");
                                print("naam hai ${items}");
                                print("naam hai ${username}");
                                print("email hai ${email}");
                                print("token hai ${token}");
                                print("id hai ${id}");
                                print("ye hai image ${image}");
                                print("ye hai phone Number ${phoneNum}");

                                SharedPreferences prefs1 =
                                    await SharedPreferences.getInstance();
                                prefs1.setString('username', username);

                                SharedPreferences prefs2 =
                                    await SharedPreferences.getInstance();
                                prefs2.setString('fullName', fullName);

                                SharedPreferences prefs3 =
                                    await SharedPreferences.getInstance();
                                prefs3.setString('cusId', id);

                                SharedPreferences prefs4 =
                                    await SharedPreferences.getInstance();
                                prefs4.setString('token', token);

                                SharedPreferences prefs6 =
                                    await SharedPreferences.getInstance();
                                prefs6.setString('userEmail', email);

                                SharedPreferences prefs12 =
                                    await SharedPreferences.getInstance();
                                prefs12.setString('totalBalance', points);

                                print("ye hai items items${points + items}");

                                SharedPreferences prefs13 =
                                    await SharedPreferences.getInstance();
                                prefs13.setString('totalItem', items);

                                SharedPreferences prefs5 =
                                    await SharedPreferences.getInstance();
                                prefs5.setString('imageprofile', image);

                                SharedPreferences prefs7 =
                                    await SharedPreferences.getInstance();
                                prefs7.setString('phoneNum', phoneNum);

                                Fluttertoast.showToast(
                                    msg: "Successfully Sign In",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1);
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                prefs.setString('email', 'useremail@gmail.com');

                                gotoNextScreen(context);
                              } else {
                                Fluttertoast.showToast(
                                    msg: "Correct your Email or Pass",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1);
                              }
                            });
                          }
                        }
                      },
                      child: Text(
                        "Sign in",
                        style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            fontSize:
                                ResponsiveFlutter.of(context).moderateScale(18),
                            color: Colors.white),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ForgetPasswordScreen()),
                      );
                    },
                    child: new Padding(
                      padding: EdgeInsets.only(
                        right: ResponsiveFlutter.of(context).moderateScale(80),
                        top: ResponsiveFlutter.of(context).moderateScale(20),
                      ),
                      child: Text(
                        "Forget Password ?",
                        style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            color: Color(0XFF5d5d5d),
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.5)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(60),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Don't have an Account? ",
                          style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            color: Color(0XFF5d5d5d),
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.5),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignUpScreen()),
                            );
                          },
                          child: new Text("Register Now",
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                color: Color(0XFF5d5d5d),
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                                decoration: TextDecoration.underline,
                              )),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void firebaseCloudMessaging_Listeners() async {
    String _token = await SessionManager().getFirebaseToken();
    tokenNew = _token;
    tokenNew ??
        "MyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseTokenMyFirebaseToken";
    print("Inside get firebase Token $_token");
    // if (Platform.isIOS) iOS_Permission();

    // _firebaseMessaging.getToken().then((token) {
    //   print("tt ${token}");
    // });

    // _firebaseMessaging.onTokenRefresh.listen((token) async {
    //   print("refto ${token}");

    //   tokenNew = token;
    // });

    // _firebaseMessaging.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     print('on message $message');
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     print('on resume $message');
    //   },
    //   onLaunch: (Map<String, dynamic> message) async {
    //     print('on launch $message');
    //   },
    // );
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void gotoNextScreen(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }
}
