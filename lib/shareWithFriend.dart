
import 'dart:io';

import 'package:fixpay/BottomNavigation/SettingsNavBar.dart';
import 'package:fixpay/myFriends.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:responsive_flutter/responsive_flutter.dart';


class ShareWithFriend  extends StatefulWidget{
  final bool isFromItem;
  final bool isFromPoint;

  const ShareWithFriend({Key key, this.isFromItem, this.isFromPoint}) : super(key: key);

  @override
  _ShareWithFriendState createState() => _ShareWithFriendState();
}
class _ShareWithFriendState extends State<ShareWithFriend>{

  String qrData = "";



  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;


    return
      Scaffold(
        backgroundColor: Color(0xFFEBFFFE),


        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,

            child: Stack(

              children: [

                Align(
                  alignment: Alignment.topCenter,
                  child: Image(
                    // height: 106,
                    // width: 400,
                    image: AssetImage('assets/images/asset3@3x.png'),
                    // fit: BoxFit.cover,
                  ),
                ),

                Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding:  EdgeInsets.only(bottom:ResponsiveFlutter.of(context).moderateScale(40)),
                    child: Image(
                      height: ResponsiveFlutter.of(context).verticalScale(120),
                      // width: 80,
                      image: AssetImage('assets/images/asset6@2x.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),



                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding:  EdgeInsets.only(top:ResponsiveFlutter.of(context).moderateScale(40)),
                    child: Text("Share with Friend"
                      ,style: TextStyle(
                        fontSize:  ResponsiveFlutter.of(context).fontSize(3),
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),),
                  ),
                ),

                Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: [


                      GestureDetector(
                        onTap: (){


                          widget.isFromItem != true?

                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => SettingsBottomNav(isFromMerchant: true)),
                          ):

                          Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(builder: (context) => SettingsBottomNav(isFromItem: widget.isFromItem)),
                          );

                        },
                        child: Container(
                          width: ResponsiveFlutter.of(context).scale(200),
                          height: ResponsiveFlutter.of(context).verticalScale(50),

                          child:
                          Center(
                            child: Text("Profile QR",
                              style: TextStyle(
                                  color: Color(0xFFfdda65),
                                  fontWeight: FontWeight.bold
                              ),

                            ),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xFF1dbcba),
                          ),
                        ),
                      ),



                      SizedBox(
                        height: ResponsiveFlutter.of(context).verticalScale(50),
                      ),

                      GestureDetector(
                        onTap: (){


                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => MyFriends(isFromPoint: widget.isFromPoint, isFromItem: widget.isFromItem, isFromShareFriend:true)),
                      );

                        },
                        child: Container(

                          width: ResponsiveFlutter.of(context).scale(200),
                          height: ResponsiveFlutter.of(context).verticalScale(50),

                      child:
                      Center(
                        child: Text("Select Your Friend",
                          style: TextStyle(
                              color: Color(0xFF1dbcba),

                              fontWeight: FontWeight.bold
                          ),

                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xFFfdda65),
                      ),
                        ),
                      ),



                    ],
                  ),
                ),



              ],
            ),
          ),
        ),
      );

  }


}