// import 'package:device_preview/device_preview.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:fixpay/Friends.dart';
import 'package:fixpay/Wallet.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/splashScreen.dart';
import 'package:flutter/material.dart';

// void main() {
//   runApp(DevicePreview(builder: (context) => MyApp(),
//   enabled: !kReleaseMode,));
// }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // locale: DevicePreview.locale(context), // Add the locale here
      // builder: DevicePreview.appBuilder,

      title: 'Flutter Demo',

      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    );
  }
}
