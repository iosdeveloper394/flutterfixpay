


import 'dart:convert' as json;
import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:passwordfield/passwordfield.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NewPassword extends StatefulWidget {
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword>{

  final newPass = TextEditingController();
  final confirmPass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;


    return
      Scaffold(
        backgroundColor: Color(0xFFEBFFFE),


        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,

            child: Stack(

              children: [

                Align(
                  alignment: Alignment.center,
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: [

                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          height: ResponsiveFlutter.of(context).verticalScale(98),

                          child: Image(
                            image: AssetImage('assets/images/asset3@3x.png'),
                          ),
                        ),
                      ),


                      SizedBox(
                        height: ResponsiveFlutter.of(context).verticalScale(100),
                      ),

                      Container(
                        height: ResponsiveFlutter.of(context).verticalScale(160),
                        width: ResponsiveFlutter.of(context).scale(250),
                        child: Column(

                          mainAxisAlignment: MainAxisAlignment.center,//Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [


                            Container(
                              height: ResponsiveFlutter.of(context).verticalScale(35),
                              width: ResponsiveFlutter.of(context).verticalScale(200),
                              child: Center(child: Align(
                                child:
                                Container(
                                  height: ResponsiveFlutter.of(context).verticalScale(30),
                                  width: ResponsiveFlutter.of(context).verticalScale(150),
                                  child: PasswordField(
                                    hintText: "New Password",
                                    hintStyle: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                    ),
                                    controller: newPass,
                                    inputStyle: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                    ),

                                  ),
                                ),
                              ),
                              ),
                              decoration: BoxDecoration(
                                  color: Color(0xFFFFFFFF),
                                  borderRadius: BorderRadius.circular(20)
                              ),
                            ),



                            // Container(
                            //   height: 35,
                            //   width: 200,
                            //   child: Center(child: Align(
                            //     child: TextField(
                            //       obscureText: true,
                            //       controller: newPass,
                            //       decoration: InputDecoration(
                            //           isDense: true,
                            //           border: InputBorder.none,
                            //           hintText: 'New Password'
                            //       ),
                            //       textAlign: TextAlign.center,
                            //       style: TextStyle(
                            //           fontSize: 10
                            //       ),
                            //     ),
                            //   ),
                            //   ),
                            //   decoration: BoxDecoration(
                            //       color: Color(0xFFFFFFFF),
                            //       borderRadius: BorderRadius.circular(20)
                            //   ),
                            // ),

                            SizedBox(
                              height: ResponsiveFlutter.of(context).verticalScale(10),
                            ),

                            Container(
                              height: ResponsiveFlutter.of(context).verticalScale(35),
                              width: ResponsiveFlutter.of(context).verticalScale(200),
                              child: Center(child: Align(
                                child:
                                Container(
                                  height: ResponsiveFlutter.of(context).verticalScale(30),
                                  width: ResponsiveFlutter.of(context).verticalScale(150),
                                  child: PasswordField(
                                    hintText: "Confirm Password",
                                    hintStyle: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                                    ),
                                    controller: confirmPass,
                                    inputStyle: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                    ),

                                  ),
                                ),
                              ),
                              ),
                              decoration: BoxDecoration(
                                  color: Color(0xFFFFFFFF),
                                  borderRadius: BorderRadius.circular(20)
                              ),
                            ),


                            // Container(
                            //   height: 35,
                            //   width: 200,
                            //   child: Center(child: TextField(
                            //     obscureText: true,
                            //     controller: confirmPass,
                            //     textAlign: TextAlign.center,
                            //     decoration: InputDecoration(
                            //         isDense: true,
                            //         border: InputBorder.none,
                            //         hintText: 'Confirm Password'
                            //     ),
                            //     style: TextStyle(
                            //         fontSize: 10
                            //     ),)),
                            //   decoration: BoxDecoration(
                            //       color: Color(0xFFFFFFFF),
                            //       borderRadius: BorderRadius.circular(20)
                            //   ),
                            // )




                          ],
                        ),
                        decoration: BoxDecoration(
                            color: Color(0xFF1dbcba),
                            borderRadius: BorderRadius.circular(5)
                        ),
                      ),

                      SizedBox(height: ResponsiveFlutter.of(context).verticalScale(20),
                      ),


                      Container(
                        child: Align(
                          alignment: Alignment.center,
                          child: Center(
                            child: Text("Minimun 8 Characters allowed with one",
                              style: TextStyle(
                                  fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                  color: Colors.grey
                              ),),
                          ),
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 40.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: Center(
                            child: Text("uppercase, one special character",
                              style: TextStyle(
                                  fontSize: ResponsiveFlutter.of(context).fontSize(2),
                                  color: Colors.grey
                              ),),
                          ),
                        ),
                      ),


                    ],
                  ),
                ),


                Stack(children: [

                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom:50),
                      child: Image(
                        height: ResponsiveFlutter.of(context).verticalScale(140),
                        // width: 80,
                        image: AssetImage('assets/images/asset6@3x.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),



                  Padding(
                    padding: const EdgeInsets.only(top:50.0),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Text("Password",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold
                        ),),
                    ),
                  ),

                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom:100.0),
                      child: Container(
                        height: ResponsiveFlutter.of(context).verticalScale(40),
                          width: ResponsiveFlutter.of(context).verticalScale(180),
                      child: RaisedButton(

                          color: Color(0xFFfdda65),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          onPressed: () async {

                            String _newPass = newPass.text.toString();
                            String _confirmPass = confirmPass.text.toString();


                            if(_newPass == null || _newPass == "" ) {


                              Fluttertoast.showToast(
                                  msg: "Write Your New Password ",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);

                            }else if (_confirmPass == null || _confirmPass == "")
                            {
                              Fluttertoast.showToast(
                                  msg: "Write Your Confirm Password ",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);

                            }else if(_newPass != _confirmPass){

                              Fluttertoast.showToast(
                                  msg: "Passwords didn't match",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);

                            }else{

                              SharedPreferences prefs4 = await SharedPreferences.getInstance();
                              var id = prefs4.getString('cusId');

                              print(" ha ye id  ${id}");

                              print("_userName, ${_newPass}");
                              print("_fullName, ${_confirmPass}");


                              progressShow(context);
                              Map params = {
                                'Password': _newPass,
                                'CustomerId': id,
                              };


                              await HttpsRequest()
                                  .fetchPostTokenData(Constant.newPass, params)
                                  .then((response) async {
                                progressHide(context);
                                print("asasas ${response.statusCode}");

                                if (response.statusCode == 200) {

                                  var convertDataToJson = json.jsonDecode(response.body);
                                  var username = convertDataToJson['userName'];
                                  var fullName = convertDataToJson['fullName'];
                                  var token = convertDataToJson['token'];
                                  var image = convertDataToJson['image'];


                                  var points = convertDataToJson['totalBalance'];
                                  var items = convertDataToJson['totalItem'];
                                  id = convertDataToJson['id'];

                                  print("naam hai ${username}");
                                  print("naam hai full ${fullName}");
                                  print("token hai ${token}");
                                  print("id hai ${id}");
                                  print("ye hai image ${image}");



                                  SharedPreferences prefs12 =
                                  await SharedPreferences.getInstance();
                                  prefs12.setString('totalBalance',points);


                                  print("ye hai items items${points + items}");


                                  SharedPreferences prefs13 =
                                  await SharedPreferences.getInstance();
                                  prefs13 .setString('totalItem', items);



                                  SharedPreferences prefs5 =
                                  await SharedPreferences.getInstance();
                                  prefs5.setString('imageprofile', image);


                                  SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                                  prefs.setString('email', 'useremail@gmail.com');

                                  SharedPreferences prefs1 =
                                  await SharedPreferences.getInstance();
                                  prefs1.setString('username', username);

                                  SharedPreferences prefs2 =
                                  await SharedPreferences.getInstance();
                                  prefs2.setString('fullName', fullName);

                                  SharedPreferences prefs3 =
                                  await SharedPreferences.getInstance();
                                  prefs3.setString('id', id);

                                  SharedPreferences prefs4 =
                                  await SharedPreferences.getInstance();
                                  prefs4.setString('token', token);


                                  Fluttertoast.showToast(
                                      msg: "Password updated successfully",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1);


                                  gotoNextScreen(context);



                                } else if( response.statusCode == 802){
                                  Fluttertoast.showToast(
                                      msg: "Password is not valid!",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1);

                                }else {

                                  Fluttertoast.showToast(
                                      msg: "No Internet",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1);

                                }
                              });

                            }


                          },

                          child: Text("Update"
                            , style: TextStyle(
                                fontSize: ResponsiveFlutter.of(context).fontSize(3),
                                color: Colors.white
                            ),),
                        ),
                      ),
                    ),
                  ),




                ],),

              ],
            ),
          ),
        ),
      );

  }

  void gotoNextScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }

}
