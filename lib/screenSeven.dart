import 'dart:convert' as json;

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/loyalityPoints.dart';
import 'package:fixpay/signUp.dart';
import 'package:fixpay/totalItems.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SevenScreen extends StatefulWidget {
  final bool isFromQr;

  const SevenScreen({Key key, this.isFromQr}) : super(key: key);
  @override
  _SevenScreenState createState() => _SevenScreenState();
}

class _SevenScreenState extends State<SevenScreen> {
  int totalBalance = 23;
  int totalItem = 56;
  String merchantImg =
      "http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg";
  String merchantDate = "12 3 2021";
  String merchantName = "abc";
  String myDate = "2021-03-04T00:48:43.525224-08:00";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // apiMethod(context);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: ResponsiveFlutter.of(context).verticalScale(100),

                child: Image.asset(
                  'assets/images/asset3@3x.png',
                  color: Color(0xFF1dbcba),
                ),

                // child: Image(
                //   image: AssetImage('assets/images/asset3@3x.png'),
                //   // fit: BoxFit.cover,
                // ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                child: Image.asset(
                  'assets/images/asset6@2x.png',
                  height: ResponsiveFlutter.of(context).verticalScale(100),
                  color: Color(0xFFfdda65),
                ),

                // Image(
                //   height: ResponsiveFlutter.of(context).verticalScale(100),
                //   // width: 80,
                //   image: AssetImage('assets/images/asset6@2x.png'),
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: [
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(60),
                  ),
                  Container(
                    width: ResponsiveFlutter.of(context).scale(140),
                    height: ResponsiveFlutter.of(context).verticalScale(140),
                    child: merchantImg == null
                        ? Icon(
                            Icons.account_circle_outlined,
                            size: ResponsiveFlutter.of(context).fontSize(16),
                            color: Color(0xFF1dbcba),
                          )
                        : CircleAvatar(
                            radius: 25.0,
                            backgroundImage: NetworkImage(merchantImg),
                            backgroundColor: Colors.transparent,
                          ),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.orange),
                        color: Colors.white,
                        shape: BoxShape.circle),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(12),
                  ),
                  Text(
                    merchantName.toString(),
                    style: TextStyle(
                        fontSize: ResponsiveFlutter.of(context).fontSize(3.2),
                      color: Colors.blueGrey.shade800,
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w400,),
                  ),
                  Text(
                    merchantDate.toString(),
                    style: TextStyle(
                      color: Colors.blueGrey.shade800,
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                      // fontWeight: FontWeight.bold
                    ),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(10),
                  ),
                  Container(
                    width: ResponsiveFlutter.of(context).scale(170),
                    height: ResponsiveFlutter.of(context).verticalScale(170),
                    child: Stack(
                      children: [
                        new GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      TotalItems(isFromQr: widget.isFromQr)),
                            );
                          },
                          child: Center(
                            child: Container(
                              width: ResponsiveFlutter.of(context).scale(140),
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(140),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image(
                                    width:
                                        ResponsiveFlutter.of(context).scale(70),
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(70),
                                    image: AssetImage(
                                        'assets/images/artboard7.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: ResponsiveFlutter.of(context)
                                            .moderateScale(15)),
                                    child: Text(
                                      "Total Items",
                                      style: TextStyle(
                                          color: Colors.blueGrey.shade800,
                                          fontFamily: 'Gotham',
                                          fontWeight: FontWeight.w400,                                          fontSize:
                                              ResponsiveFlutter.of(context)
                                                  .fontSize(2.0),
                                          ),
                                    ),
                                  )
                                ],
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            width: ResponsiveFlutter.of(context).scale(26),
                            height:
                                ResponsiveFlutter.of(context).verticalScale(28),
                            child: Center(
                                child: Text(
                              totalItem.toString(),
                              style: TextStyle(
                                  // fontFamily: 'Gotham',
                                  fontSize:
                                      ResponsiveFlutter.of(context).fontSize(2),
                                  // fontWeight: FontWeight.bold
                                color: Colors.blueGrey.shade800,
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w400,
                              ),
                            )),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(90),
                                border: Border.all(color: Color(0xFFfdda65)),
                                color: Color(0xFFfdda65),
                                shape: BoxShape.rectangle
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(10),
                  ),
                  Container(
                    width: ResponsiveFlutter.of(context).scale(170),
                    height: ResponsiveFlutter.of(context).verticalScale(170),
                    child: Stack(
                      children: [
                        new GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoyalityPoints(
                                      isFromQr: widget.isFromQr)),
                            );
                          },
                          child: Center(
                            child: Container(
                              width: ResponsiveFlutter.of(context).scale(150),
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(150),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image(
                                    width:
                                        ResponsiveFlutter.of(context).scale(70),
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(70),
                                    image: AssetImage(
                                        'assets/images/artboard10.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: ResponsiveFlutter.of(context)
                                            .moderateScale(15)),
                                    child: Text(
                                      "Total Loyalty",
                                      style: TextStyle(

                                          fontSize:
                                              ResponsiveFlutter.of(context)
                                                  .fontSize(2.0),
                                        color: Colors.blueGrey.shade800,
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w400,),
                                    ),
                                  ),
                                  Text(
                                    "Points",
                                    style: TextStyle(
                                        // fontFamily: 'Gotham',
                                        fontSize: ResponsiveFlutter.of(context)
                                            .fontSize(2.0),
                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w400,
                                        // fontWeight: FontWeight.bold
                                    ),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            width: ResponsiveFlutter.of(context).scale(26),
                            height:
                                ResponsiveFlutter.of(context).verticalScale(28),
                            child: Center(
                                child: Text(
                              totalBalance.toString(),
                              style: TextStyle(
                                  // fontFamily: 'Gotham',
                                  fontSize:
                                      ResponsiveFlutter.of(context).fontSize(2),
                                  // fontWeight: FontWeight.bold
                                color: Colors.blueGrey.shade800,
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w400,
                              ),
                            )),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(90),
                                border: Border.all(color: Color(0xFFfdda65)),
                                color: Color(0xFFfdda65),
                                shape: BoxShape.rectangle),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    String _merchantId = merchantId.toString();

    var params = {'MerchantId': _merchantId};

    progressShow(context);

    await HttpsRequest()
        .fetchPostMerchantsForCustomer(Constant.myItemsPoints, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        totalBalance =
            convertDataToJson['merchatto']['totalBalance']['totalBalance'];
        totalItem = convertDataToJson['merchatto']['totalItem']['totalItem'];
        merchantImg = convertDataToJson['merchatto']['merchant']['image'];
        merchantDate = convertDataToJson['merchatto']['merchant']['createDate'];
        merchantName = convertDataToJson['merchatto']['merchant']['brandName'];

        String myDate = merchantDate;
        DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm").parse(myDate);
        merchantDate = "${tempDate.day} ${tempDate.month} ${tempDate.year}";
        print("ye hai date convert");
        print(merchantDate);

        setState(() {});
      } else {
        // list = new List();
        Fluttertoast.showToast(
            msg: "issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }
}
