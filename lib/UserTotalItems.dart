import 'dart:convert' as json;
import 'dart:io';

import 'package:fixpay/BottomNavigation/SettingsNavBar.dart';
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ItemPinCode.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Redeem.dart';
// import 'package:fixpay/ShareWithFriendItem.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/myFriends.dart';
import 'package:fixpay/redeemPinCode.dart';
import 'package:fixpay/shareWithFriend.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserTotalItem extends StatefulWidget {
  final bool isFromItem;
  final bool isFromPoint;

  const UserTotalItem({Key key, this.isFromItem, this.isFromPoint})
      : super(key: key);
  @override
  _UserTotalItemState createState() => _UserTotalItemState();
}

class _UserTotalItemState extends State<UserTotalItem> {
  List list = new List();
  int totalPoints = 0;
  String imgaeProfile;
  String date = "Date";
  String nameBrand = "Name";

  @override
  void initState() {
    // TODO: implement initState
    // apiMethod(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Image.asset(
                'assets/images/asset3@3x.png',
                color: Color(0xFF1dbcba),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                child: Image.asset(
                  'assets/images/asset6@2x.png',
                  height: ResponsiveFlutter.of(context).verticalScale(120),
                  color: Color(0xFFfdda65),
                ),

                // Image(
                //   height: ResponsiveFlutter.of(context).verticalScale(120),
                //   // width: 80,
                //   image: AssetImage('assets/images/asset6@2x.png'),
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(
                    top: ResponsiveFlutter.of(context).moderateScale(40)),
                child: Text(
                  "Total Items",
                  style: TextStyle(
                    fontFamily: 'Gotham',
                    fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // Container(
                  //   height: 100,
                  //   width: 100,
                  //   child:  Image(
                  //     height: 100,
                  //     width: 100,
                  //     image: AssetImage('assets/images/burger.png'),
                  //     fit: BoxFit.cover,
                  //   ),
                  //   decoration: BoxDecoration(
                  //       border: Border.all(color: Colors.orange),
                  //       color: Colors.white,
                  //       shape: BoxShape.circle
                  //   ),
                  // ),

                  // Container(
                  //   height: 120,
                  //   width: 120,
                  //   child: imgaeProfile == null  ?
                  //   Icon(Icons.account_circle_outlined,
                  //     size: 120,
                  //     color: Color(0xFF1dbcba),
                  //   ):
                  //   CircleAvatar(
                  //     radius: 25.0,
                  //     backgroundImage:
                  //     NetworkImage('http://hustledev-001-site8.itempurl.com/'+imgaeProfile),
                  //     backgroundColor: Colors.transparent,
                  //   ),
                  //   decoration: BoxDecoration(
                  //       border: Border.all(color: Colors.orange),
                  //       color: Colors.white,
                  //       shape: BoxShape.circle
                  //   ),
                  // ),
                  //
                  // SizedBox(height: 10,),
                  // Text(nameBrand.toString(),
                  //   style: TextStyle(
                  //       fontSize: 20,
                  //       fontWeight: FontWeight.bold
                  //   ),
                  // ),
                  //
                  // Text(date.toString(),
                  //   style: TextStyle(
                  //       fontSize: 8
                  //     // fontWeight: FontWeight.bold
                  //   ),
                  // ),

                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(40)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(450),
                      width: ResponsiveFlutter.of(context).scale(280),
                      color: Color(0xFFEBFFFE),
                      // color: Colors.red,
                      child: Container(
                        padding: EdgeInsets.all(8.0),
                        child: ListView.builder(
                          shrinkWrap: true,
                          padding: EdgeInsets.only(top: 10),
                          itemCount: list.length = 20,
                          // itemCount: list.length == null ? 20 : list.length,
                          itemBuilder: (BuildContext context, int index) {
                            print("test1234 ${list}");
                            // print(list.length);

                            return listViewMethod(context, index);
                          },
                        ),
                      ),
                    ),
                  ),

                  // new GestureDetector(
                  //   onTap: (){
                  //
                  //     _openPopup(context);
                  //   },
                  //   child: Container(
                  //     width: 260,
                  //
                  //     child:
                  //
                  //     Row(
                  //       children: [
                  //         SizedBox(
                  //           height: 10,
                  //         ),
                  //         Padding(
                  //           padding: const EdgeInsets.only(left:30.0),
                  //           child: Text("Whooper with fries",
                  //             style: TextStyle(
                  //                 fontSize: 12
                  //             ),),
                  //         ),
                  //
                  //
                  //         Padding(
                  //           padding: const EdgeInsets.only(left:50.0),
                  //           child: Container(
                  //             width: 60,
                  //             height: 20,
                  //             child: Center(
                  //               child: Text(
                  //                 'Reedem',
                  //                 style: TextStyle(color: Colors.black,
                  //                     fontSize: 12,
                  //                     fontWeight: FontWeight.bold),
                  //               ),
                  //             ),
                  //             decoration: BoxDecoration(
                  //               borderRadius: BorderRadius.circular(5),
                  //               color: Color(0xFF1dbcba),
                  //             ),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //
                  //     decoration: BoxDecoration(
                  //       borderRadius: BorderRadius.circular(10),
                  //       color: Colors.white,
                  //
                  //     ),
                  //     height: 40,
                  //   ),
                  // ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _openPopup(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            content: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      width: 180,
                      child: Center(
                        child: Text(
                          "Generate QR",
                          style: TextStyle(
                              color: Color(0xFFfdda65),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xFF1dbcba),
                      ),
                      height: 40,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 180,
                      child: Center(
                        child: Text(
                          "Generate QR",
                          style: TextStyle(
                              color: Color(0xFFfdda65),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xFF1dbcba),
                      ),
                      height: 40,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    String _merchantId = merchantId.toString();

    var params = {'MerchantId': _merchantId};

    progressShow(context);

    await HttpsRequest()
        .fetchPostMerchantsForCustomer(Constant.totalItems, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        list = convertDataToJson['merchatto']['cashbackRecentItemList'];

        print("ye hai list loy${list}");

        // totalPoints = convertDataToJson['merchatto']['totalBalance']['totalBalance'];
        date = convertDataToJson['merchatto']['merchant']['createDate'];
        nameBrand = convertDataToJson['merchatto']['merchant']['brandName'];
        imgaeProfile = convertDataToJson['merchatto']['merchant']['image'];

        String myDate = date;
        DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm").parse(myDate);
        date = "${tempDate.day} ${tempDate.month} ${tempDate.year}";
        print("ye hai date convert");
        print(date);

        SharedPreferences prefs13 = await SharedPreferences.getInstance();
        prefs13.setString('imgaeProfileRedeem', imgaeProfile);

        SharedPreferences prefs12 = await SharedPreferences.getInstance();
        prefs12.setString('nameBrandRedeem', nameBrand);

        SharedPreferences prefs14 = await SharedPreferences.getInstance();
        prefs14.setString('dateRedeem', date);

        setState(() {});
      } else if (response.statusCode == 500) {
        // list = new List();
        Fluttertoast.showToast(
            msg: "No Item",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      } else {
        Fluttertoast.showToast(
            msg: "No Inter Net",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }

  Widget listViewMethod(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: GestureDetector(
        onTap: () {
          // Navigator.pushReplacement(
          //   context,
          //   MaterialPageRoute(builder: (context) => ShareWithFriend(isFromItem: widget.isFromItem)),
          // );

          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  scrollable: true,
                  content: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Form(
                      child: Align(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "Share via",
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(2.8),

                                color: Colors.blueGrey.shade800,
                                // fontFamily: 'Gotham',
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(3),
                            ),
                            Text(
                              "Share your items",
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1),
                                color: Colors.blueGrey.shade800,
                                // fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                              ),
                            ),
                            SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(15),
                            ),
                            GestureDetector(
                              onTap: () {
                                widget.isFromItem != true
                                    ? Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SettingsBottomNav(
                                                    isFromMerchant: true)),
                                      )
                                    : Navigator.pushReplacement(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SettingsBottomNav(
                                                    isFromItem:
                                                        widget.isFromItem)),
                                      );
                              },
                              child: Container(
                                width: ResponsiveFlutter.of(context).scale(200),
                                height: ResponsiveFlutter.of(context)
                                    .verticalScale(50),
                                child: Center(
                                  child: Text(
                                    "Profile QR",
                                    style: TextStyle(
                                      color: Color(0xFFFFFFFF),
                                      fontWeight: FontWeight.w300,
                                      fontFamily: 'Gotham',
                                    ),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color(0xFF1dbcba),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(30),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MyFriends(
                                          isFromPoint: widget.isFromPoint,
                                          isFromItem: widget.isFromItem,
                                          isFromShareFriend: true)),
                                );
                              },
                              child: Container(
                                width: ResponsiveFlutter.of(context).scale(200),
                                height: ResponsiveFlutter.of(context)
                                    .verticalScale(50),
                                child: Center(
                                  child: Text(
                                    "Select Your Friend",
                                    style: TextStyle(
                                      color: Color(0xFFFFFFFF),
                                      fontWeight: FontWeight.w300,
                                      fontFamily: 'Gotham',
                                    ),
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Color(0xFFfdda65),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(15),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              });

          // showDialog(
          //     context: context,
          //     builder: (BuildContext context) {
          //       return AlertDialog(
          //         scrollable: true,
          //         content: Padding(
          //           padding: const EdgeInsets.all(8.0),
          //           child: Form(
          //             child: Column(
          //               children: <Widget>[
          //
          //                 SizedBox(
          //                   height: 30,
          //                 ),
          //
          //                 GestureDetector(
          //                   onTap: (){
          //
          //
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(builder: (context) => ItemPinCode()),
          // );
        },
        //                   child: Container(
        //                     width: 180,
        //                     child:
        //                     Center(
        //                       child: Text("Redeem for myself?",
        //                         style: TextStyle(
        //                             color: Color(0xFFfdda65),
        //                             fontWeight: FontWeight.bold
        //                         ),
        //
        //                       ),
        //                     ),
        //                     decoration: BoxDecoration(
        //                       borderRadius: BorderRadius.circular(5),
        //                       color: Color(0xFF1dbcba),
        //                     ),
        //                     height: 40,
        //                   ),
        //                 ),
        //
        //
        //                 SizedBox(
        //                   height: 20,
        //                 ),
        //
        //                 GestureDetector(
        //                   onTap: (){
        //
        //
        //                     Navigator.pushReplacement(
        //                       context,
        //                       MaterialPageRoute(builder: (context) => ShareWithFriendItem()),
        //                     );
        //
        //                   },
        //                   child: Container(
        //                     width: 180,
        //                     child:
        //                     Center(
        //                       child: Text("Share with a Friend?",
        //                         style: TextStyle(
        //                             color: Color(0xFF1dbcba),
        //
        //                             fontWeight: FontWeight.bold
        //                         ),
        //
        //                       ),
        //                     ),
        //                     decoration: BoxDecoration(
        //                       borderRadius: BorderRadius.circular(5),
        //                       color: Color(0xFFfdda65),
        //                     ),
        //                     height: 40,
        //                   ),
        //                 ),
        //
        //                 SizedBox(
        //                   height: 30,
        //                 ),
        //
        //
        //
        //
        //               ],
        //             ),
        //           ),
        //         ),
        //
        //       );
        //     });

        // },

        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: 5.0,
          ),
          decoration: BoxDecoration(
            color: Color(0xFFFFFFFF),
            boxShadow: [
              BoxShadow(
                blurRadius: 1.0,
                color: Colors.black26,
                offset: Offset(0.0, 1.0),
                spreadRadius: 1.0,
              )
            ],
          ),
          height: ResponsiveFlutter.of(context).verticalScale(50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Burget King".toString(),
                style: TextStyle(
                    color: Colors.blueGrey.shade800,
                    fontFamily: 'Gotham',
                    fontWeight: FontWeight.w300,
                    fontSize: ResponsiveFlutter.of(context).fontSize(2)),
              ),
              SizedBox(
                width: ResponsiveFlutter.of(context).scale(70),
              ),
              Center(
                child: Text(
                  "Share",
                  // list[index]['amount'].toString(),
                  style: TextStyle(

                      // color: Colors.blueGrey.shade800,
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF1dbcba),
                      // fontWeight: FontWeight.bold,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
