import 'dart:async';
import 'dart:convert' as json;
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/Newpassword.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/pinCod.dart';
import 'package:fixpay/signIn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VerificationEmailScreen extends StatefulWidget {
  final Color backgroundColor = Colors.white;
  final TextStyle styleTextUnderTheLoader = TextStyle(
      fontFamily: 'Gotham',
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      color: Colors.black);

  @override
  _VerificationEmailScreenState createState() =>
      _VerificationEmailScreenState();
}

class _VerificationEmailScreenState extends State<VerificationEmailScreen> {
  final pinCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: ResponsiveFlutter.of(context).verticalScale(98),
                child: Image(
                  image: AssetImage('assets/images/asset3@3x.png'),
                ),
              ),
            ),
            SizedBox(
              height: ResponsiveFlutter.of(context).verticalScale(20),
            ),
            Text(
              "Email Verification",
              style: TextStyle(
                  fontFamily: 'Gotham',
                  fontSize: ResponsiveFlutter.of(context).fontSize(3),
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 15,
            ),
            Image(
              height: 110,
              width: 110,
              image: AssetImage('assets/images/asset13@3x.png'),
              // fit: BoxFit.cover,
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: Center(
                  child: Text(
                    "Enter the verification code that",
                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        color: Colors.grey),
                  ),
                ),
              ),
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: Center(
                  child: Text(
                    "we just sent you on your",
                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        color: Colors.grey),
                  ),
                ),
              ),
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: Center(
                  child: Text(
                    "email address or phone",
                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        color: Colors.grey),
                  ),
                ),
              ),
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: Center(
                  child: Text(
                    "number",
                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        color: Colors.grey),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              // margin: EdgeInsets.symmetric(horizontal: 100),

              width: 160,

              child: PinCodeTextField(
                keyboardType: TextInputType.number,
                controller: pinCodeController,
                backgroundColor: Color(0xFFEBFFFE),
                length: 4,
                obscureText: false,
                animationType: AnimationType.fade,
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.box,
                  inactiveFillColor: Color(0xFF1dbcba),
                  inactiveColor: Color(0xFF1dbcba),
                  activeFillColor: Color(0xFF1dbcba),
                  selectedColor: Color(0xFF1dbcba),

                  selectedFillColor: Color(0xFF1dbcba),

                  activeColor: Color(0xFF1dbcba),
                  // selectedColor: Colors.blueGrey,
                  // borderRadius: BorderRadius.circular(0),
                  fieldHeight: 30,
                  fieldWidth: 30,
                  // activeFillColor: Colors.black,
                ),
                animationDuration: Duration(milliseconds: 300),
                cursorColor: Color(0xFF1dbcba),
                enableActiveFill: true,
                // errorAnimationController: errorController,
                // controller: textEditingController,
                onCompleted: (v) {
                  print("Completed");
                },
                // onChanged: (value) {
                //   print(value);
                //   setState(() {
                //     var currentText = value;
                //   });
                // },
                beforeTextPaste: (text) {
                  print("Allowing to paste $text");
                  //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                  //but you can show anything you want here, like your pop up saying wrong paste format or etc
                  return true;
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 35,
              width: 150,
              child: RaisedButton(
                color: Color(0xFFfdda65),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15)),
                onPressed: () async {
                  SharedPreferences prefs4 =
                      await SharedPreferences.getInstance();
                  var email = prefs4.getString('emailVerify');
                  print("email ha ye ${email}");

                  String _pinCode = pinCodeController.text;
                  print("aaaa${_pinCode}");

                  var myInt = int.parse(_pinCode);
                  assert(myInt is int);

                  print("myInt${myInt}"); // 12345

                  Map params = {'Email': email, 'VerificationCode': myInt};

                  print("aaaa${myInt}");

                  progressShow(context);
                  await HttpsRequest()
                      .fetchPostForgetPass(Constant.verifiCodeReg, params)
                      .then((response) async {
                    progressHide(context);

                    print("asasas ${response.statusCode}");
                    if (response.statusCode == 200) {
                      var convertDataToJson = json.jsonDecode(response.body);
                      var id = convertDataToJson['id'];
                      print(" ha ye id  ${id}");

                      SharedPreferences prefs1 =
                          await SharedPreferences.getInstance();
                      prefs1.setString('cusId', id);

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PinCodeScreen()),
                      );
                    } else {
                      Fluttertoast.showToast(
                          msg: "Enter Correct Verification Code",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1);
                    }
                  });
                },
                child: Text(
                  "Verify",
                  style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      fontSize: 18,
                      color: Colors.white),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Container(
              child: Align(
                alignment: Alignment.center,
                child: Center(
                  child: Text(
                    "If you did'nt recieve code ",
                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: 18,
                        color: Colors.grey),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            GestureDetector(
              onTap: () async {
                SharedPreferences prefs4 =
                    await SharedPreferences.getInstance();
                var email = prefs4.getString('emailVerify');
                print("email ha ye ${email}");

                SharedPreferences prefs5 =
                    await SharedPreferences.getInstance();
                var pass = prefs5.getString('passlVerify');
                print("email ha ye ${pass}");

                SharedPreferences prefs6 =
                    await SharedPreferences.getInstance();
                var phone = prefs6.getString('phoneVerify');
                print("email ha ye ${phone}");

                SharedPreferences prefs7 =
                    await SharedPreferences.getInstance();
                var token = prefs7.getString('tokenVerify');
                print("email ha ye ${token}");

                SharedPreferences prefs8 =
                    await SharedPreferences.getInstance();
                var userName = prefs8.getString('userNameVerify');
                print("email ha ye ${userName}");

                // String _email = email;

                progressShow(context);
                Map params = {
                  'UserName': userName,
                  'Password': pass,
                  'FirebaseToken': token,
                  'Email': email,
                  'PhoneNumber': phone,
                };

                await HttpsRequest()
                    .fetchPostData(Constant.signup, params)
                    .then((response) async {
                  progressHide(context);
                  print("asasas ${response.statusCode}");

                  if (response.statusCode == 200) {
                    Fluttertoast.showToast(
                        msg: "Verification Code Sent",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                  } else {
                    Fluttertoast.showToast(
                        msg: "No Internet",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.CENTER,
                        timeInSecForIosWeb: 1);
                  }
                });
              },
              child: Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "RESEND",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          color: Colors.red),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
