import 'dart:convert' as json;

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/UserTotalItems.dart';
import 'package:fixpay/UserTotalPoints.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/loyalityPoints.dart';
import 'package:fixpay/signUp.dart';
import 'package:fixpay/totalItems.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ShareScreen extends StatefulWidget {
  @override
  _ShareScreenState createState() => _ShareScreenState();
}

class _ShareScreenState extends State<ShareScreen> {
  int totalBalance = 0;
  int totalItem = 0;
  String merchantImg;
  String merchantDate = "Date";
  String merchantName = "Brand Name";
  String myDate = "2021-03-04T00:48:43.525224-08:00";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // apiMethod(context);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                child: Image(
                  image: AssetImage('assets/images/asset3@3x.png'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                child: Image.asset(
                  'assets/images/asset6@2x.png',
                  height: ResponsiveFlutter.of(context).verticalScale(120),
                  color: Color(0xFFfdda65),
                ),

                // Image(
                //   height: ResponsiveFlutter.of(context).verticalScale(120),
                //   // width: 80,
                //   image: AssetImage('assets/images/asset6@2x.png'),
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(
                    top: ResponsiveFlutter.of(context).moderateScale(50)),
                child: Text(
                  "Share",
                  style: TextStyle(
                    fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                    color: Colors.white,

                    fontFamily: 'Gotham',
                    fontWeight: FontWeight.w400,

                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // SizedBox(height: 80,),

                  // Container(
                  //   height: 120,
                  //   width: 120,
                  //   child: merchantImg == null  ?
                  //   Icon(Icons.account_circle_outlined,
                  //     size: 120,
                  //     color: Color(0xFF1dbcba),
                  //   ):
                  //   CircleAvatar(
                  //     radius: 25.0,
                  //     backgroundImage:
                  //     NetworkImage('http://hustledev-001-site8.itempurl.com/'+merchantImg),
                  //     backgroundColor: Colors.transparent,
                  //   ),
                  //   decoration: BoxDecoration(
                  //       border: Border.all(color: Colors.orange),
                  //       color: Colors.white,
                  //       shape: BoxShape.circle
                  //   ),
                  // ),
                  // SizedBox(height: 10,),
                  // Text(merchantName.toString(),
                  //   style: TextStyle(
                  //       fontSize: 20,
                  //       fontWeight: FontWeight.bold
                  //   ),
                  // ),
                  //
                  // Text(merchantDate.toString(),
                  //   style: TextStyle(
                  //       fontSize: 8
                  //     // fontWeight: FontWeight.bold
                  //   ),
                  // ),

                  SizedBox(
                    height: 10,
                  ),

                  Stack(
                    children: [
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    UserTotalItem(isFromItem: true)),
                          );
                        },
                        child: Container(
                          width: ResponsiveFlutter.of(context).scale(150),
                          height:
                              ResponsiveFlutter.of(context).verticalScale(150),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image(
                                width: ResponsiveFlutter.of(context).scale(70),
                                height: ResponsiveFlutter.of(context)
                                    .verticalScale(70),
                                image:
                                    AssetImage('assets/images/artboard7.png'),
                                fit: BoxFit.contain,
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: ResponsiveFlutter.of(context)
                                        .moderateScale(15)),
                                child: Text(
                                  "Total Items",
                                  style: TextStyle(

                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w400,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(2.5),
                                      // fontWeight: FontWeight.bold
                                  ),
                                ),
                              )
                            ],
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                      ),

                      // Padding(
                      //   padding: const EdgeInsets.only(top:100.0, left: 100),
                      //   child: Container(
                      //     height: 25,
                      //     width: 35,
                      //     child: Center(child: Text(totalItem.toString(),
                      //       style:
                      //       TextStyle(
                      //           fontWeight: FontWeight.bold
                      //       ),
                      //     )
                      //     ),
                      //     decoration: BoxDecoration(
                      //         border: Border.all(color: Colors.orange),
                      //         color: Colors.orange,
                      //         shape: BoxShape.rectangle
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(50),
                  ),
                  Stack(
                    children: [
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    UserTotalPoints(isFromPoint: true)),
                          );
                        },
                        child: Container(
                          width: ResponsiveFlutter.of(context).scale(150),
                          height:
                              ResponsiveFlutter.of(context).verticalScale(150),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image(
                                width: ResponsiveFlutter.of(context).scale(70),
                                height: ResponsiveFlutter.of(context)
                                    .verticalScale(70),
                                image:
                                    AssetImage('assets/images/artboard10.png'),
                                fit: BoxFit.contain,
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: ResponsiveFlutter.of(context)
                                        .moderateScale(15)),
                                child: Text(
                                  "Total Loyalty",
                                  style: TextStyle(

                                    color: Colors.blueGrey.shade800,
                                    fontFamily: 'Gotham',
                                    fontWeight: FontWeight.w400,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(2.5),
                                      // fontWeight: FontWeight.bold
                                  ),
                                ),
                              ),
                              Text(
                                "Points",
                                style: TextStyle(
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.5),

                                  color: Colors.blueGrey.shade800,
                                  fontFamily: 'Gotham',
                                  fontWeight: FontWeight.w400,),
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                      ),

                      // Padding(
                      //   padding: const EdgeInsets.only(top:100.0, left: 100),
                      //   child: Container(
                      //     height: 25,
                      //     width: 35,
                      //     child: Center(child: Text(totalBalance.toString(),
                      //       style:
                      //       TextStyle(
                      //           fontWeight: FontWeight.bold
                      //       ),
                      //     )
                      //     ),
                      //     decoration: BoxDecoration(
                      //         border: Border.all(color: Colors.orange),
                      //         color: Colors.orange,
                      //         shape: BoxShape.rectangle
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    String _merchantId = merchantId.toString();

    var params = {'MerchantId': "646683a1-1ab3-4d0a-92dd-119feb0d004e"};

    progressShow(context);

    await HttpsRequest()
        .fetchPostMerchantsForCustomer(Constant.myItemsPoints, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        totalBalance =
            convertDataToJson['merchatto']['totalBalance']['totalBalance'];
        totalItem = convertDataToJson['merchatto']['totalItem']['totalItem'];
        merchantImg = convertDataToJson['merchatto']['merchant']['image'];
        merchantDate = convertDataToJson['merchatto']['merchant']['createDate'];
        merchantName = convertDataToJson['merchatto']['merchant']['brandName'];

        String myDate = merchantDate;
        DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm").parse(myDate);
        merchantDate = "${tempDate.day} ${tempDate.month} ${tempDate.year}";
        print("ye hai date convert");
        print(merchantDate);

        setState(() {});
      } else {
        // list = new List();
        Fluttertoast.showToast(
            msg: "issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }
}
