import 'dart:io';

import 'package:fixpay/Redeem.dart';
import 'package:fixpay/pinCod.dart';
import 'package:fixpay/redeemPinCode.dart';
import 'package:fixpay/screenSeven.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsBottomNav extends StatefulWidget {
  final bool isFromMerchant;
  final bool isFromItem;

  const SettingsBottomNav({Key key, this.isFromMerchant, this.isFromItem})
      : super(key: key);

  @override
  _SettingsBottomNavState createState() => _SettingsBottomNavState();
}

class _SettingsBottomNavState extends State<SettingsBottomNav> {
  String qrData = "";

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Image.asset(
                'assets/images/asset3@3x.png',
                color: Color(0xFF1dbcba),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: Text(
                    "Scan Your QR",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'Gotham',

                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Container(
                      height: 40,
                      width: 180,
                      child: RaisedButton(
                        color: Color(0xFF1dbcba),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        onPressed: () async {
                          String barcodeScanRes =
                              await FlutterBarcodeScanner.scanBarcode(
                                  "#000000", "close", true, ScanMode.QR);

                          qrData = barcodeScanRes;

                          print("ye hai 12 ${qrData}");
                          print("ye hai 123 ${barcodeScanRes}");

                          setState(() {});
                        },
                        child: Text(
                          "Open Qr Scanner",
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: 'Gotham',
                              fontWeight: FontWeight.w300,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(20),
                  ),
                  Text(
                    "OR",
                    style: TextStyle(
                        fontSize: ResponsiveFlutter.of(context).fontSize(2),
                      fontFamily: 'Gotham',
                      color: Colors.blueGrey.shade800,
                      // fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(20),
                  ),
                  Container(
                    height: 40,
                    width: 180,
                    child: RaisedButton(
                      color: Color(0xFF1dbcba),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      onPressed: () async {
                        //
                        // SharedPreferences prefs13 =
                        // await SharedPreferences.getInstance();
                        // prefs13 .setString('imgaeProfileRedeem', "fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg");
                        //
                        //
                        // SharedPreferences prefs12 =
                        // await SharedPreferences.getInstance();
                        // prefs12.setString('nameBrandRedeem', "Ali");
                        //
                        //
                        // SharedPreferences prefs14 =
                        // await SharedPreferences.getInstance();
                        // prefs14.setString('dateRedeem', "20 3 2021");

                        SharedPreferences prefs1 =
                            await SharedPreferences.getInstance();
                        prefs1.setString(
                            'merchantId', "646683a1-1ab3-4d0a-92dd-119feb0d004e");

                        widget.isFromItem != true
                            ?

                            // print("ara hai ${widget.isFromMerchant}");
                            widget.isFromMerchant != true
                                ? Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            SevenScreen(isFromQr: true)),
                                  )
                                : Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Redeem()),
                                  )
                            : Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => RedeemPinCode()),
                              );
                      },
                      child: Text(
                        "Redeem",
                        style: TextStyle(
                          fontSize: 14,
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 5.0),
                  child: Image(
                    height: 120,
                    // width: 80,
                    image: AssetImage('assets/images/asset6@3x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
