import 'package:fixpay/Friends.dart';
import 'package:fixpay/FriendsAllLists.dart';
import 'package:fixpay/ShareScreen.dart';
import 'package:fixpay/UserTrans.dart';
import 'package:fixpay/Wallet.dart';
import 'package:fixpay/screenSeven.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image/network.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeBottomNav extends StatefulWidget {
  @override
  _HomeBottomNavState createState() => _HomeBottomNavState();
}

class _HomeBottomNavState extends State<HomeBottomNav> {
  String imageProfile;
  String fullName;
  String totalItem = "0";
  String totalBalance = "0";
  String userName = "User Name";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Data();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        return showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext dialogContext) {
              final screenSize = MediaQuery.of(context).size;
              return AlertDialog(
                insetPadding: EdgeInsets.all(30.0),
                backgroundColor: Colors.transparent,
                contentPadding: EdgeInsets.all(0.0),
                content: Container(
                  height: 160.0,
                  width: screenSize.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 20.0,
                        ),
                        Text(
                          "Are you sure you want to exit the app?",
                          style: TextStyle(
                              color: Colors.grey.withOpacity(0.8),
                              fontSize: 20.0),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 23.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            FlatButton(
                              padding: EdgeInsets.all(0.0),
                              onPressed: () {
                                SystemNavigator.pop();
                              },
                              child: Container(
                                height: 45.0,
                                width: 100,
                                decoration: BoxDecoration(
                                  color: Color(0xFF1dbcba),
                                  // hexToColor(primaryColor), hexToColor(secondaryColor)
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3)),
                                  borderRadius: BorderRadius.circular(50.0),
                                ),
                                child: Center(
                                  child: Text(
                                    "Yes",
                                    style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.8,
                                      color: Colors.white,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            FlatButton(
                              padding: EdgeInsets.all(0.0),
                              onPressed: () {
                                Navigator.pop(dialogContext);
                              },
                              child: Container(
                                height: 45.0,
                                width: 100,
                                decoration: BoxDecoration(
                                  color: Color(0xFF1dbcba),
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3)),
                                  borderRadius: BorderRadius.circular(50.0),
                                ),
                                child: Center(
                                  child: Text(
                                    "No",
                                    style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.8,
                                      color: Colors.white,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              );
            });
      },
      child: Scaffold(
        backgroundColor: Color(0xFFEBFFFE),
        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Container(
                    // height: 100,
                    // width: 45,
                    color: Colors.transparent,
                    child:
                        //
                        // Image(
                        //   height: MediaQuery.of(context).size.height * 0.20,
                        //
                        // // width: 80,
                        //   image: AssetImage('assets/images/asset6@2x.png'),
                        //   fit: BoxFit.cover,
                        // ),

                        Image.asset(
                      'assets/images/asset6@2x.png',
                      height: MediaQuery.of(context).size.height * 0.20,
                      color: Color(0xFFfdda65),
                    ),
                  ),
                ),
                Container(
                  // margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.10,),

//                   width: MediaQuery.of(context).size.width * 0.95,

                  child: Stack(children: [
                    //  Padding(
                    //   padding: const EdgeInsets.only(top:20.0),
                    //   child: Align(
                    //     alignment: Alignment.topRight,
                    //     child: Container(
                    //       child:
                    //
                    //
                    //       Image(
                    //         height: MediaQuery.of(context).size.width * 0.40,
                    //
                    //       width: MediaQuery.of(context).size.width * 0.40,
                    //         image: AssetImage('assets/images/12.png'),
                    //         fit: BoxFit.cover,
                    //       ),
                    //     ),
                    //   ),
                    // ),

                    Column(
                      // crossAxisAlignment: CrossAxisAlignment.start, // for left side
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.10,
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              right: ResponsiveFlutter.of(context)
                                  .moderateScale(70)),
                          child: Container(
                            child: Column(
                              crossAxisAlignment:
                                  CrossAxisAlignment.start, // for left side

                              children: [
                                Container(
                                  width: ResponsiveFlutter.of(context).scale(60),
                                  height: ResponsiveFlutter.of(context)
                                      .verticalScale(60),

                                  // width: MediaQuery.of(context).size.width * 0.30,
                                  child: imageProfile == null
                                      ? Icon(
                                          Icons.account_circle_outlined,
                                          size: ResponsiveFlutter.of(context)
                                              .fontSize(220),
                                          color: Color(0xFF1dbcba),
                                        )
                                      : CircleAvatar(
                                          radius: 25.0,
                                          backgroundImage: NetworkImageWithRetry(
                                              'http://hustledev-001-site8.itempurl.com/' +
                                                  imageProfile),
                                          backgroundColor: Colors.transparent,
                                        ),
                                ),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.01,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 10.0),
                                  child: Text(
                                    "Hello!",
                                    style: TextStyle(
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(3),
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFF1dbcba),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.01,
                                ),
                                Container(
                                  height: ResponsiveFlutter.of(context)
                                      .verticalScale(35),
                                  width: ResponsiveFlutter.of(context).scale(150),
                                  child: Center(
                                      child: Text(
                                    fullName == null
                                        ? userName.toString()
                                        : fullName.toString(),
                                    style: TextStyle(
                                      fontSize: 17,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w300,
                                    ),
                                  )),
                                  decoration: BoxDecoration(
                                    color: Color(0xFFfdda65),
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: ResponsiveFlutter.of(context).verticalScale(25),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: ResponsiveFlutter.of(context).scale(110),
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(110),
                              child: Stack(
                                children: [
                                  Image(
                                    width:
                                        ResponsiveFlutter.of(context).scale(110),
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(110),
                                    image:
                                        AssetImage('assets/images/asset20.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: ResponsiveFlutter.of(context)
                                                .moderateScale(40)),
                                        child: Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Text(
                                            totalBalance.toString(),
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(4),
                                              fontFamily: 'Gotham',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text(
                                        "Total Points",
                                        style: TextStyle(
                                            color: Color(0XFF5d5d5d),
                                            fontWeight: FontWeight.w300,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(1.4)
                                            // color: Colors.blueGrey,
                                            ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                // color: Color(0xFFfad668),
                                borderRadius: BorderRadius.circular(60),
                              ),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(10),
                            ),
                            Container(
                              //
                              width: ResponsiveFlutter.of(context).scale(110),
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(110),
                              child: Stack(
                                children: [
                                  Image(
                                    width:
                                        ResponsiveFlutter.of(context).scale(110),
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(110),
                                    image:
                                        AssetImage('assets/images/asset19.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: ResponsiveFlutter.of(context)
                                                .moderateScale(40)),
                                        child: Align(
                                          alignment: Alignment.bottomCenter,
                                          child: Text(
                                            totalItem.toString(),
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(4),
                                              fontFamily: 'Gotham',
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Text(
                                        "Total Items",
                                        style: TextStyle(
                                            color: Color(0XFF5d5d5d),
                                            fontFamily: 'Gotham',
                                            fontWeight: FontWeight.w300,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(1.3)
                                            // color: Colors.blueGrey,
                                            ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                // color: Color(0xFFfad668),
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: ResponsiveFlutter.of(context).verticalScale(20),
                        ),
                        Container(
                          width: ResponsiveFlutter.of(context).scale(220),
                          height:
                              ResponsiveFlutter.of(context).verticalScale(220),
                          child: Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: ResponsiveFlutter.of(context).scale(30),
                              vertical: ResponsiveFlutter.of(context).scale(10),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  children: [
                                    new GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  FriendAllList()),
                                        );
                                      },

                                      child: Column(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                width:
                                                    ResponsiveFlutter.of(context)
                                                        .scale(60),
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(65),
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      //  padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(18)),
                                                      child: Image(
                                                        width:
                                                            ResponsiveFlutter.of(
                                                                    context)
                                                                .scale(100),
                                                        height: ResponsiveFlutter
                                                                .of(context)
                                                            .verticalScale(100),
                                                        image: AssetImage(
                                                            'assets/images/oval.png'),
                                                        fit: BoxFit.contain,
                                                      ),
                                                    ),
                                                    Container(
                                                      // padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(8)),
                                                      child: Center(
                                                        child: Image(
                                                          width: ResponsiveFlutter
                                                                  .of(context)
                                                              .scale(36),
                                                          height:
                                                              ResponsiveFlutter
                                                                      .of(context)
                                                                  .verticalScale(
                                                                      36),
                                                          image: AssetImage(
                                                              'assets/images/artboard121.png'),
                                                          // fit: BoxFit.contain,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(3),
                                              ),
                                              Text(
                                                "Friends",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w300,
                                                  fontSize: ResponsiveFlutter.of(
                                                          context)
                                                      .fontSize(1.5),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),

                                      // child: Column(
                                      //   children: [
                                      //     Container(
                                      //
                                      //       width: ResponsiveFlutter.of(context).scale(60),
                                      //       height: ResponsiveFlutter.of(context).verticalScale(65),
                                      //       decoration: BoxDecoration(
                                      //           border: Border.all(
                                      //             color: Color(0xFF1dbcba),
                                      //           ),
                                      //           borderRadius: BorderRadius.all(Radius.circular(90))
                                      //       ),
                                      //
                                      //       child:
                                      //
                                      //       Container(
                                      //         height: 20,
                                      //         width: 20,
                                      //         padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(18)),
                                      //         child: Image(
                                      //           height: 10,
                                      //           width: 10,
                                      //           image: AssetImage('assets/images/asset17.png'),
                                      //           fit: BoxFit.contain,
                                      //         ),
                                      //       ),
                                      //
                                      //     ),
                                      //     Text("Friends",
                                      //     style: TextStyle(
                                      //       fontSize: 10
                                      //     ),
                                      //     ),
                                      //   ],
                                      // ),
                                    ),
                                    SizedBox(
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(10),
                                    ),
                                    new GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  ShareScreen()),
                                        );
                                      },
                                      child: Column(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                width:
                                                    ResponsiveFlutter.of(context)
                                                        .scale(60),
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(65),
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      //  padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(18)),
                                                      child: Image(
                                                        width:
                                                            ResponsiveFlutter.of(
                                                                    context)
                                                                .scale(100),
                                                        height: ResponsiveFlutter
                                                                .of(context)
                                                            .verticalScale(100),
                                                        image: AssetImage(
                                                            'assets/images/oval.png'),
                                                        fit: BoxFit.contain,
                                                      ),
                                                    ),
                                                    Container(
                                                      // padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(8)),
                                                      child: Center(
                                                        child: Image(
                                                          width: ResponsiveFlutter
                                                                  .of(context)
                                                              .scale(36),
                                                          height:
                                                              ResponsiveFlutter
                                                                      .of(context)
                                                                  .verticalScale(
                                                                      36),
                                                          image: AssetImage(
                                                              'assets/images/artboard141.png'),
                                                          fit: BoxFit.contain,
                                                        ),

                                                        // Image(
                                                        //
                                                        //   width: ResponsiveFlutter.of(context).scale(27),
                                                        //   height: ResponsiveFlutter.of(context).verticalScale(27),
                                                        //   image: AssetImage('assets/images/asset14.png'),
                                                        //   fit: BoxFit.contain,
                                                        // ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(3),
                                              ),
                                              Text(
                                                "Share",
                                                style: TextStyle(
                                                  fontWeight: FontWeight.w300,
                                                  fontSize: ResponsiveFlutter.of(
                                                          context)
                                                      .fontSize(1.5),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                // SizedBox(
                                //   width: ResponsiveFlutter.of(context).scale(30),
                                // ),
                                Column(
                                  children: [
                                    new GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => Wallet(),
                                          ),
                                        );
                                        print("asd1111");
                                      },
                                      child: Column(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                width:
                                                    ResponsiveFlutter.of(context)
                                                        .scale(60),
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(65),
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      //  padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(18)),
                                                      child: Image(
                                                        width:
                                                            ResponsiveFlutter.of(
                                                                    context)
                                                                .scale(100),
                                                        height: ResponsiveFlutter
                                                                .of(context)
                                                            .verticalScale(100),
                                                        image: AssetImage(
                                                            'assets/images/oval.png'),
                                                        fit: BoxFit.contain,
                                                      ),
                                                    ),
                                                    Container(
                                                      // padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(8)),
                                                      child: Center(
                                                        child: Image(
                                                          width: ResponsiveFlutter
                                                                  .of(context)
                                                              .scale(40),
                                                          height:
                                                              ResponsiveFlutter
                                                                      .of(context)
                                                                  .verticalScale(
                                                                      40),
                                                          image: AssetImage(
                                                              'assets/images/artboard131.png'),
                                                          fit: BoxFit.contain,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(3),
                                              ),
                                              Text(
                                                "Wallet",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    fontSize:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .fontSize(1.5)),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: ResponsiveFlutter.of(context)
                                          .verticalScale(10),
                                    ),
                                    new GestureDetector(
                                      onTap: () async {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => UserTrans()),
                                        );
                                      },
                                      child: Column(
                                        children: [
                                          Column(
                                            children: [
                                              Container(
                                                width:
                                                    ResponsiveFlutter.of(context)
                                                        .scale(60),
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(65),
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      //  padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(18)),
                                                      child: Image(
                                                        width:
                                                            ResponsiveFlutter.of(
                                                                    context)
                                                                .scale(100),
                                                        height: ResponsiveFlutter
                                                                .of(context)
                                                            .verticalScale(100),
                                                        image: AssetImage(
                                                            'assets/images/oval.png'),
                                                        fit: BoxFit.contain,
                                                      ),
                                                    ),
                                                    Container(
                                                      // padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(8)),
                                                      child: Center(
                                                        child: Image(
                                                          width: ResponsiveFlutter
                                                                  .of(context)
                                                              .scale(40),
                                                          height:
                                                              ResponsiveFlutter
                                                                      .of(context)
                                                                  .verticalScale(
                                                                      40),
                                                          image: AssetImage(
                                                              'assets/images/artboard151.png'),
                                                          // fit: BoxFit.contain,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height:
                                                    ResponsiveFlutter.of(context)
                                                        .verticalScale(3),
                                              ),
                                              Text(
                                                "Transaction",
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    fontSize:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .fontSize(1.5)),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          decoration: BoxDecoration(







                           color: Color(
                             0xFFFFFFFF,
                           ),
                           borderRadius: BorderRadius.circular(15),
                          ),
                        ),
                      ],
                    )
                  ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> Data() async {
    SharedPreferences prefs2 = await SharedPreferences.getInstance();
    imageProfile = prefs2.getString('imageprofile');
    print("ye hai image proile ${imageProfile}");

    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    fullName = prefs4.getString('fullName');
    print("fullName${fullName}");

    SharedPreferences prefs5 = await SharedPreferences.getInstance();
    userName = prefs5.getString('username');
    print("user Name home mai${userName}");

    SharedPreferences prefs12 = await SharedPreferences.getInstance();
    totalBalance = prefs12.getString('totalBalance');
    print("user Name home mai points ${totalBalance}");

    SharedPreferences prefs13 = await SharedPreferences.getInstance();
    totalItem = prefs13.getString('totalItem');
    print("user Name home mai${totalItem}");

    setState(() {});
  }
}
