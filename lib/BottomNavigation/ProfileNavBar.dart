import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:fixpay/Email.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/password.dart';
import 'package:fixpay/phone.dart';
import 'package:fixpay/profile.dart';
import 'package:fixpay/signIn.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as json;

import 'package:slider_button/slider_button.dart';

class ProfileBottomNav extends StatefulWidget {
  @override
  _ProfileBottomNavState createState() => _ProfileBottomNavState();
}

class _ProfileBottomNavState extends State<ProfileBottomNav> {
  String imageProfile;
  String fullName;
  String userName;
  PlatformFile file;
  String cusId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sharedPrefencess();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return WillPopScope(
      onWillPop: () async {
        return showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext dialogContext) {
              final screenSize = MediaQuery.of(context).size;
              return AlertDialog(
                insetPadding: EdgeInsets.all(30.0),
                backgroundColor: Colors.transparent,
                contentPadding: EdgeInsets.all(0.0),
                content: Container(
                  height: 160.0,
                  width: screenSize.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 20.0,
                        ),
                        Text(
                          "Are you sure you want to exit the app?",
                          style: TextStyle(
                              color: Colors.grey.withOpacity(0.8),
                              fontSize: 20.0),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 23.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            FlatButton(
                              padding: EdgeInsets.all(0.0),
                              onPressed: () {
                                SystemNavigator.pop();
                              },
                              child: Container(
                                height: 45.0,
                                width: 100,
                                decoration: BoxDecoration(
                                  color: Color(0xFF1dbcba),
                                  // hexToColor(primaryColor), hexToColor(secondaryColor)
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3)),
                                  borderRadius: BorderRadius.circular(50.0),
                                ),
                                child: Center(
                                  child: Text(
                                    "Yes",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 0.8,
                                      color: Colors.white,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 20.0,
                            ),
                            FlatButton(
                              padding: EdgeInsets.all(0.0),
                              onPressed: () {
                                Navigator.pop(dialogContext);
                              },
                              child: Container(
                                height: 45.0,
                                width: 100,
                                decoration: BoxDecoration(
                                  color: Color(0xFF1dbcba),
                                  border: Border.all(
                                      color: Colors.grey.withOpacity(0.3)),
                                  borderRadius: BorderRadius.circular(50.0),
                                ),
                                child: Center(
                                  child: Text(
                                    "No",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 0.8,
                                      color: Colors.white,
                                      fontSize: 18.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              );
            });
      },
      child: Scaffold(
        backgroundColor: Color(0xFFEBFFFE),
        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,
            child: Stack(
              children: [
                // SizedBox(height: 0.5,),

                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: ResponsiveFlutter.of(context).moderateScale(150)),
                    child: Image.asset(
                      'assets/images/asset4@3x.png',
                      height: ResponsiveFlutter.of(context).verticalScale(150),
                      color: Color(0xFFfdda65),
                    ),

                    // Image(
                    //   height: ResponsiveFlutter.of(context).verticalScale(150),
                    //   // width: 80,
                    //   image: AssetImage('assets/images/asset4@3x.png'),
                    //   fit: BoxFit.cover,
                    // ),
                  ),
                ),

                Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                        bottom: ResponsiveFlutter.of(context).moderateScale(10)),
                    child:

                        // Image(
                        //   height: ResponsiveFlutter.of(context).verticalScale(150),
                        //     // width: 80,
                        //   image: AssetImage('assets/images/asset6@3x.png'),
                        //   fit: BoxFit.cover,
                        // ),

                        Image.asset(
                      'assets/images/asset6@3x.png',
                      height: ResponsiveFlutter.of(context).verticalScale(150),
                      color: Color(0xFFfdda65),
                    ),
                  ),
                ),

                Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: ResponsiveFlutter.of(context).verticalScale(98),
                        child:
                        Image.asset('assets/images/asset3@3x.png',
                        color: Color(0xFF1dbcba),
                      ),
                        // Image(
                        //   image: AssetImage('assets/images/asset3@3x.png'),
                        // ),
                      ),
                    ),

                    GestureDetector(
                      onTap: () async {
                        FilePickerResult result =
                            await FilePicker.platform.pickFiles(
                          type: FileType.custom,
                          allowedExtensions: ['jpg', 'png'],
                        );

                        file = result.files.single;

                        print(file.name);
                        print(file.bytes);
                        print(file.size);
                        print(file.extension);
                        print(file.path);

                        print("ye hai upload result  ${result}");
                        print("ye hai upload file  ${file}");

                        if (result != null) {
                          // progressShow(context);

                          SharedPreferences prefs4 =
                              await SharedPreferences.getInstance();
                          var token;
                          token = prefs4.getString('token');
                          print("token123${token}");

                          progressShow(context);

                          var postUri = Uri.parse(
                              "http://hustledev-001-site8.itempurl.com/api/Customer/changeProfilePicture");
                          var request = new http.MultipartRequest(
                            "POST",
                            postUri,
                          );

                          http.MultipartFile multipartFile =
                              await http.MultipartFile.fromPath(
                                  'file', file.path);
                          request.files.add(multipartFile);

                          print("ye hai file${multipartFile}");

                          request.headers.clear();
                          request.headers.addAll({
                            "content-type": "multipart/form-data",
                            'Authorization': 'Bearer $token'
                          });

                          // http.StreamedResponse response = await request.send();

                          var streamedResponse = await request.send();
                          var response =
                              await http.Response.fromStream(streamedResponse);

                          progressHide(context);

                          print("ye hai response code ${response.statusCode}");

                          if (response.statusCode == 200) {
                            var convertDataToJson =
                                json.jsonDecode(response.body);
                            print("ye hai body ${convertDataToJson}");

                            var image =
                                convertDataToJson['responseto']['imagePath'];

                            print("ye hai image api mai ${image}");

                            SharedPreferences prefs5 =
                                await SharedPreferences.getInstance();
                            prefs5.setString('imageprofile', image);

                            sharedPrefencess();

                            Fluttertoast.showToast(
                                msg: " Profile Picture Uploaded",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);

                            file = null;
                          }

                          // Fluttertoast.showToast(
                          //         msg: "woring.......!",
                          //         toastLength: Toast.LENGTH_SHORT,
                          //         gravity: ToastGravity.CENTER,);
                        }
                      },
                      child: Container(
                        width: ResponsiveFlutter.of(context).scale(140),
                        height: ResponsiveFlutter.of(context).verticalScale(140),
                        child: imageProfile == null
                            ? Icon(
                                Icons.account_circle_outlined,
                                size: 120,
                                color: Color(0xFF1dbcba),
                              )
                            : CircleAvatar(
                                radius: 25.0,
                                backgroundImage: NetworkImage(
                                    'http://hustledev-001-site8.itempurl.com/' +
                                        imageProfile),
                                backgroundColor: Colors.transparent,
                              ),
                      ),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(15),
                    ),

                    Container(
                      width: ResponsiveFlutter.of(context).scale(50),
                      height: ResponsiveFlutter.of(context).verticalScale(25),
                      child: Text(
                        fullName == null ? 'Full Name' : fullName,
                        style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(2.6)),
                      ),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(15),
                    ),

                    new GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Profile()),
                        );
                      },
                      child: Container(
                        width: ResponsiveFlutter.of(context).scale(420),
                        height: ResponsiveFlutter.of(context).verticalScale(35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .center, //Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment
                              .center, //Center Column contents horizontally,

                          children: [
                            Image(
                              width: ResponsiveFlutter.of(context).scale(25),
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(25),
                              image: AssetImage('assets/images/artboard6.png'),
                              // fit: BoxFit.contain,
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(10),
                            ),
                            Text(
                              "Profile",
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  fontWeight: FontWeight.w300,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(2.0)),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(135),
                            ),
                            Icon(
                              Icons.arrow_right,
                              size: ResponsiveFlutter.of(context).fontSize(4.5),
                              color: Color(0xFF1dbcba),
                            ),
                          ],
                        ),
                      ),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(5),
                    ),

                    Container(
                      width: ResponsiveFlutter.of(context).scale(420),
                      height: ResponsiveFlutter.of(context).verticalScale(35),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .center, //Center Column contents vertically,
                        crossAxisAlignment: CrossAxisAlignment
                            .center, //Center Column contents horizontally,

                        children: [
                          Image(
                            width: ResponsiveFlutter.of(context).scale(25),
                            height:
                                ResponsiveFlutter.of(context).verticalScale(25),
                            image: AssetImage('assets/images/artboard11.png'),
                            // fit: BoxFit.contain,
                          ),
                          SizedBox(
                            width: ResponsiveFlutter.of(context).scale(10),
                          ),
                          Text(
                            "QR code",
                            style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(2.0)),
                          ),
                          SizedBox(
                            width: ResponsiveFlutter.of(context).scale(120),
                          ),
                          Icon(
                            Icons.arrow_right,
                            size: ResponsiveFlutter.of(context).fontSize(4.5),
                            color: Color(0xFF1dbcba),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(5),
                    ),

                    new GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Password()),
                        );
                      },
                      child: Container(
                        width: ResponsiveFlutter.of(context).scale(420),
                        height: ResponsiveFlutter.of(context).verticalScale(35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .center, //Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment
                              .center, //Center Column contents horizontally,

                          children: [
                            Image(
                              width: ResponsiveFlutter.of(context).scale(25),
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(25),
                              image: AssetImage('assets/images/artboard17.png'),
                              // fit: BoxFit.contain,
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(10),
                            ),
                            Text(
                              "Password",
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  fontWeight: FontWeight.w300,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(2.0)),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(110),
                            ),
                            Icon(
                              Icons.arrow_right,
                              size: ResponsiveFlutter.of(context).fontSize(4.5),
                              color: Color(0xFF1dbcba),
                            ),
                          ],
                        ),
                      ),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(5),
                    ),

                    new GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Email()),
                        );
                      },
                      child: Container(
                        width: ResponsiveFlutter.of(context).scale(420),
                        height: ResponsiveFlutter.of(context).verticalScale(35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .center, //Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment
                              .center, //Center Column contents horizontally,

                          children: [
                            Image(
                              width: ResponsiveFlutter.of(context).scale(25),
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(25),
                              image: AssetImage('assets/images/artboard16.png'),
                              // fit: BoxFit.contain,
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(10),
                            ),
                            Text(
                              "Email",
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  fontWeight: FontWeight.w300,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(2.0)),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(145),
                            ),
                            Icon(
                              Icons.arrow_right,
                              size: ResponsiveFlutter.of(context).fontSize(4.5),
                              color: Color(0xFF1dbcba),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(5),
                    ),

                    new GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Phone()),
                        );
                      },
                      child: Container(
                        width: ResponsiveFlutter.of(context).scale(420),
                        height: ResponsiveFlutter.of(context).verticalScale(35),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .center, //Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment
                              .center, //Center Column contents horizontally,

                          children: [
                            Image(
                              width: ResponsiveFlutter.of(context).scale(25),
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(25),
                              image: AssetImage('assets/images/artboard18.png'),
                              // fit: BoxFit.contain,
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(10),
                            ),
                            Text(
                              "Phone number",
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  fontWeight: FontWeight.w300,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(2.0)),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(75),
                            ),
                            Icon(
                              Icons.arrow_right,
                              size: ResponsiveFlutter.of(context).fontSize(4.5),
                              color: Color(0xFF1dbcba),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(40),
                    ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.center,//Center Column contents vertically,
                    //   crossAxisAlignment: CrossAxisAlignment.center, //Center Column contents horizontally,
                    //
                    //   children: [
                    //     Icon(Icons.account_balance_wallet,
                    //       size: 22,
                    //       color: Color(0xFF1dbcba),
                    //     ),
                    //     SizedBox(width: 2,),
                    //
                    //     Text(
                    //       "Wallet",
                    //       style: TextStyle(
                    //           fontSize: 16
                    //       ),
                    //     ),
                    //
                    //
                    //     SizedBox(width: 140,),
                    //
                    //     Icon(Icons.arrow_right,
                    //       size: 28,
                    //       color: Color(0xFF1dbcba),
                    //     ),
                    //   ],
                    // ),

                    Container(
                      width: ResponsiveFlutter.of(context).scale(240),
                      height: ResponsiveFlutter.of(context).verticalScale(37),
                      child: SliderButton(
                        action: () async {
                          SharedPreferences prefs1 =
                              await SharedPreferences.getInstance();
                          prefs1.remove('username');
                          SharedPreferences prefs2 =
                              await SharedPreferences.getInstance();
                          prefs2.remove('fullName');
                          SharedPreferences prefs3 =
                              await SharedPreferences.getInstance();
                          prefs3.remove('id');
                          SharedPreferences prefs4 =
                              await SharedPreferences.getInstance();
                          prefs4.remove('token');
                          SharedPreferences prefs5 =
                              await SharedPreferences.getInstance();
                          prefs5.remove('imageprofile');
                          SharedPreferences prefs6 =
                              await SharedPreferences.getInstance();
                          prefs6.remove('userEmail');
                          SharedPreferences prefs7 =
                              await SharedPreferences.getInstance();
                          prefs7.remove('phoneNum');
                          SharedPreferences prefs8 =
                              await SharedPreferences.getInstance();
                          prefs8.remove('email');
                          SharedPreferences prefs9 =
                              await SharedPreferences.getInstance();
                          prefs9.remove('totalBalance');
                          SharedPreferences prefs10 =
                              await SharedPreferences.getInstance();
                          prefs10.remove('totalItem');

                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext ctx) => SignInScreen()));
                        },

                        ///Put label over here
                        label: Text(
                          "Sign Out",
                          style: TextStyle(
                            color: Color(0xff4a4a4a),
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                          ),
                        ),
                        icon: Icon(
                          Icons.logout,
                          size: ResponsiveFlutter.of(context).fontSize(4.5),
                          color: Color(0xFF1dbcba),
                        ),

                        ///Change All the color and size from here.
                        width: ResponsiveFlutter.of(context).scale(400),
                        radius: 5,
                        buttonColor: Colors.white,
                        backgroundColor: Colors.white,
                        highlightedColor: Colors.white,
                        // baseColor: Colors.red,
                      ),
                    ),
                  ],
                ),

                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(40)),
                    child: Text(
                      "Profile",
                      style: TextStyle(
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                        color: Colors.white,
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w400,

                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> sharedPrefencess() async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    imageProfile = prefs4.getString('imageprofile');
    print("ye hai image profile ${imageProfile}");

    SharedPreferences prefs5 = await SharedPreferences.getInstance();
    fullName = prefs5.getString('fullName');
    print("ye hai full name ${fullName}");

    SharedPreferences prefs6 = await SharedPreferences.getInstance();
    userName = prefs6.getString('username');
    print("ye hai full name ${userName}");

    SharedPreferences prefs7 = await SharedPreferences.getInstance();
    cusId = prefs7.getString('cusId');
    print("ye hai user ID ${cusId}");

    setState(() {});
  }
}
