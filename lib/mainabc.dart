import 'package:fixpay/BottomNavigation/HomeNavBar.dart';
import 'package:flutter/material.dart';



class XyzScreen extends StatefulWidget {
  final bool isAMerchant;

  XyzScreen({Key key, this.isAMerchant}) : super(key: key);

  @override
  _XyzScreenState createState() => _XyzScreenState();
}

class _XyzScreenState extends State<XyzScreen> {
  @override
  Widget build(BuildContext context) {
    return

      DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(

            bottom: TabBar(
              labelColor: Colors.black,
              tabs: [
                Tab(text: "asd"),
                Tab(icon: Icon(Icons.directions_transit)),
                Tab(icon: Icon(Icons.directions_car)),
              ],
            ),
          ),
          body:
          TabBarView(
            children: [
              // Icon(Icons.flight, size: 350),
              HomeBottomNav(),

              Icon(Icons.directions_transit, size: 350),
              Icon(Icons.directions_car, size: 350),
            ],
          ),
        ),
      );

  }

  Widget menu() {
    return Container(
      height: 50.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(0), topLeft: Radius.circular(0)),
        boxShadow: [
          BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 0),
        ],
      ),
      child: Container(
        color: Color(0xFFfdda65),
        child: TabBar(
          // labelColor: hexToColor(primaryColor),
          unselectedLabelColor: Colors.black26,
          indicatorSize: TabBarIndicatorSize.label,
          indicatorColor: Color(0xFFfdda65),
          labelStyle: TextStyle(
            fontSize: 11.0,
            // color: hexToColor(primaryColor),
          ),
          unselectedLabelStyle: TextStyle(
            fontSize: 11.0,
            color: Colors.black26,
          ),
          tabs: [

            Tab(
              icon: Image(
                height: 25,
                width: 25,
                image: AssetImage('assets/images/asset1.png'),
                // fit: BoxFit.contain,
              ),
            ),

            Tab(
                icon:
                Icon(Icons.qr_code_scanner,
                  // Icon(Icons.account_circle_outlined,
                  size: 30,
                  color: Colors.black,
                )


              // Image(
              //   height: 25,
              //   width: 25,
              //   image: AssetImage('assets/images/asset2.png'),
              //   // fit: BoxFit.contain,
              // ),

              // icon: _currentIndex == 0 ? Image.asset(
              //   "assets/images/1.png",
              // ) : Image.asset(
              //   "assets/images/2.png",
              // ),
            ),


            Tab(
              icon:
              Image(
                height: 25,
                width: 25,
                image: AssetImage('assets/images/asset3.png'),
                // fit: BoxFit.contain,
              ),
            ),

          ],
          // onTap: (index) {
          //   setState(() {
          //     _currentIndex = index;
          //   });
          // },
        ),
      ),
    );
  }
}
