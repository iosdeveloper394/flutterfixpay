import 'dart:io';
import 'package:fixpay/Emailverification.dart';
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/SessionManager/SessionManager.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/signIn.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:passwordfield/passwordfield.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as json;

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String tokenNew;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final userNameTextFieldController = TextEditingController();
  final emailTextFieldController = TextEditingController();
  final phoneNumberTextFieldController = TextEditingController();
  final passwordTextFieldController = TextEditingController();
  String fullName;
  String username;
  String token;
  String id;
  String email;
  String phoneNum;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseCloudMessaging_Listeners();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: ResponsiveFlutter.of(context).moderateScale(120)),
                  child: Image.asset(
                    'assets/images/asset5@3x.png',
                    height: ResponsiveFlutter.of(context).verticalScale(110),
                    color: Color(0xFFfdda65),
                  ),

                  // Image(
                  //   height: ResponsiveFlutter.of(context).verticalScale(110),
                  //   // width: 80,
                  //   image: AssetImage('assets/images/asset5@3x.png'),
                  //   // fit: BoxFit.cover,
                  // ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: ResponsiveFlutter.of(context).moderateScale(70)),
                  child:
                      //
                      // Image(
                      //   height: ResponsiveFlutter.of(context).verticalScale(120),
                      //   // width: 80,
                      //   image: AssetImage('assets/images/asset113x.png'),
                      //   // fit: BoxFit.cover,
                      // ),

                      Image.asset(
                    'assets/images/asset113x.png',
                    height: ResponsiveFlutter.of(context).verticalScale(120),
                    color: Color(0xFF1dbcba),
                  ),
                ),
              ),
              Column(
                children: [
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(25),
                  ),
                  Center(
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(100),
                      width: ResponsiveFlutter.of(context).scale(90),
                      decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: Color(0xFF1dbcba),
                      ),
                      child: Center(
                        child: Text(
                          "LOGO",
                          style: TextStyle(
                              fontFamily: 'Gotham',
                              fontWeight: FontWeight.w300,
                              color: Colors.white,
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(3.0)),
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(10),
                  ),

                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(300),
                      margin: EdgeInsets.symmetric(
                          horizontal: ResponsiveFlutter.of(context).scale(40)),

                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(25.0),
                        boxShadow: kElevationToShadow[0],
                      ),
                      // height: 230,

                      child: Column(
                        children: [
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(20),
                          ),
                          Container(
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(45),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      ResponsiveFlutter.of(context).scale(20)),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Color(0xFFF2F7F6),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                  ),
                                  Image(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(25),
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                    image:
                                        AssetImage('assets/images/artboard6.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(20),
                                  ),
                                  Expanded(
                                    child: Container(
                                      // height: 30,
                                      // width: 150,
                                      child: TextField(
                                        controller: userNameTextFieldController,
                                        style: TextStyle(
                                            fontFamily: 'Gotham',
                                            fontWeight: FontWeight.w300,
                                            color: Color(0XFF5d5d5d),

                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(1.5)),
                                        decoration: InputDecoration(
                                            isDense: true,
                                            border: InputBorder.none,
                                            hintText: 'Username '),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10),
                          ),
                          Container(
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(45),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      ResponsiveFlutter.of(context).scale(20)),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Color(0xFFF2F7F6),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                  ),
                                  Image(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(25),
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                    image: AssetImage(
                                        'assets/images/artboard16.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(20),
                                  ),
                                  Expanded(
                                    child: Container(
                                      // height: 30,
                                      // width: 150,
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: TextField(
                                          controller: emailTextFieldController,
                                          style: TextStyle(
                                              fontFamily: 'Gotham',
                                              fontWeight: FontWeight.w300,
                                              color: Color(0XFF5d5d5d),

                                              fontSize:
                                                  ResponsiveFlutter.of(context)
                                                      .fontSize(1.5)),
                                          decoration: InputDecoration(
                                              isDense: true,
                                              border: InputBorder.none,
                                              hintText:
                                                  'Email Address/ Username'),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10),
                          ),
                          Container(
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(45),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      ResponsiveFlutter.of(context).scale(20)),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Color(0xFFF2F7F6),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                  ),
                                  Image(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(25),
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                    image: AssetImage(
                                        'assets/images/artboard18.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(20),
                                  ),
                                  Expanded(
                                    child: Container(
                                      // height: 30,
                                      // width: 150,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller:
                                            phoneNumberTextFieldController,
                                        style: TextStyle(
                                            fontFamily: 'Gotham',
                                            fontWeight: FontWeight.w300,
                                            color: Color(0XFF5d5d5d),

                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(1.5)),
                                        decoration: InputDecoration(
                                            isDense: true,
                                            border: InputBorder.none,
                                            hintText: 'Phone Number '),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10),
                          ),
                          Container(
                              height:
                                  ResponsiveFlutter.of(context).verticalScale(45),
                              margin: EdgeInsets.symmetric(
                                  horizontal:
                                      ResponsiveFlutter.of(context).scale(20)),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                color: Color(0xFFF2F7F6),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                  ),
                                  Image(
                                    height: ResponsiveFlutter.of(context)
                                        .verticalScale(25),
                                    width:
                                        ResponsiveFlutter.of(context).scale(25),
                                    image: AssetImage(
                                        'assets/images/artboard17.png'),
                                    fit: BoxFit.contain,
                                  ),
                                  SizedBox(
                                    width:
                                        ResponsiveFlutter.of(context).scale(20),
                                  ),
                                  Expanded(
                                    child: Container(
                                      child: TextField(
                                        controller: passwordTextFieldController,
                                        style: TextStyle(
                                            fontFamily: 'Gotham',
                                            fontWeight: FontWeight.w300,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(1.5)),
                                        decoration: InputDecoration(
                                            isDense: true,
                                            border: InputBorder.none,
                                            hintText: 'Password'),

                                        // PasswordField(
                                        //   controller: passwordTextFieldController,
                                        //   inputStyle: TextStyle(
                                        // fontFamily: 'Gotham',
                                        //         fontWeight: FontWeight.w300,
                                        //
                                        //       fontSize: ResponsiveFlutter.of(context).fontSize(1.5)
                                        //   ),
                                      ),
                                    ),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(15),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 25.0),
                            child: Align(
                              alignment: Alignment.center,
                              child: Center(
                                child: Text(
                                  "Minimun 8 Characters allowed with one",
                                  style: TextStyle(
                                    fontFamily: 'Gotham',
                                    fontWeight: FontWeight.w300,
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(1.5),
                                    color: Color(0XFF5d5d5d),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 25.0),
                            child: Align(
                              alignment: Alignment.center,
                              child: Center(
                                child: Text(
                                  "uppercase, one special character",
                                  style: TextStyle(
                                    fontFamily: 'Gotham',
                                    fontWeight: FontWeight.w300,
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(1.5),
                                    color: Color(0XFF5d5d5d),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  // SizedBox(
                  //   height: 10,
                  // ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(15),
                  ),

                  Text(
                    "or",
                    style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      color: Color(0XFF5d5d5d),

                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                    ),
                  ),

                  // SizedBox(
                  //   height: 10,
                  // ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "SIGN UP VIA",
                        style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                          color: Color(0XFF5d5d5d),

                        ),
                      ),
                      SizedBox(
                        width: ResponsiveFlutter.of(context).verticalScale(10),
                      ),

                      // Image(
                      //   height: ResponsiveFlutter.of(context).verticalScale(40),
                      //   width: ResponsiveFlutter.of(context).scale(30),
                      //   image: AssetImage('assets/images/artboard19.png'),
                      //   // fit: BoxFit.cover,
                      // ),

                      Image.asset(
                        'assets/images/artboard19.png',
                        height: ResponsiveFlutter.of(context).verticalScale(40),
                        width: ResponsiveFlutter.of(context).scale(30),
                        color: Color(0xFF1dbcba),
                      ),

                      SizedBox(
                        width: ResponsiveFlutter.of(context).verticalScale(5),
                      ),

                      // Image(
                      //   height: ResponsiveFlutter.of(context).verticalScale(42),
                      //   width: ResponsiveFlutter.of(context).scale(32),
                      //   image: AssetImage('assets/images/artboard20.png'),
                      //   // fit: BoxFit.cover,
                      // ),

                      Image.asset(
                        'assets/images/artboard20.png',
                        height: ResponsiveFlutter.of(context).verticalScale(42),
                        width: ResponsiveFlutter.of(context).scale(32),
                        color: Color(0xFF1dbcba),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(70),
                  ),

                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(45),
                      width: ResponsiveFlutter.of(context).scale(180),
                      child: RaisedButton(
                        color: Color(0xFFfdda65),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)),
                        onPressed: () async {
                          print("tokan ${tokenNew}");

                          String _userName =
                              userNameTextFieldController.text.toString();

                          String _phoneNumber =
                              phoneNumberTextFieldController.text.toString();

                          String _email =
                              emailTextFieldController.text.toString();

                          String _password =
                              passwordTextFieldController.text.toString();

                          if (_userName == null || _userName == "") {
                            Fluttertoast.showToast(
                                msg: "Please Enter Username",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);
                            return;
                          } else {
                            if (_email == null || _email == "") {
                              Fluttertoast.showToast(
                                  msg: "Please Enter Email Address",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                              return;
                            } else {
                              if (_phoneNumber == null || _phoneNumber == "") {
                                Fluttertoast.showToast(
                                    msg: "Please Enter Phone Number",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1);
                                return;
                              } else {
                                if (_password == null || _password == "") {
                                  Fluttertoast.showToast(
                                      msg: "Please Enter password",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.CENTER,
                                      timeInSecForIosWeb: 1);
                                  return;
                                } else {
                                  if (_password.length < 8) {
                                    Fluttertoast.showToast(
                                        msg:
                                            "pasword should be more then 8 corrector",
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.CENTER,
                                        timeInSecForIosWeb: 1);
                                    return;
                                  } else {
                                    if (_phoneNumber == null ||
                                        _phoneNumber == "") {
                                      Fluttertoast.showToast(
                                          msg: "Please write your Phone Number",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.CENTER,
                                          timeInSecForIosWeb: 1);
                                      return;
                                    } else {
                                      if (_phoneNumber.length < 7) {
                                        Fluttertoast.showToast(
                                            msg: "Phone Number is Invalid",
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.CENTER,
                                            timeInSecForIosWeb: 1);
                                        return;
                                      } else {
                                        if (_phoneNumber.length > 12) {
                                          Fluttertoast.showToast(
                                              msg: "Phone Number is Invalid",
                                              toastLength: Toast.LENGTH_SHORT,
                                              gravity: ToastGravity.CENTER,
                                              timeInSecForIosWeb: 1);
                                          return;
                                        }
                                      }

                                      progressShow(context);
                                      Map params = {
                                        'UserName': _userName,
                                        'Password': _password,
                                        'FirebaseToken': tokenNew,
                                        'Email': _email,
                                        'PhoneNumber': _phoneNumber,
                                      };

                                      await HttpsRequest()
                                          .fetchPostData(Constant.signup, params)
                                          .then((response) async {
                                        progressHide(context);
                                        print("asasas ${response.statusCode}");

                                        if (response.statusCode == 200) {
                                          SharedPreferences prefs17 =
                                              await SharedPreferences
                                                  .getInstance();
                                          prefs17.setString(
                                              'emailVerify', _email);
                                          print("User Name signup mai ${_email}");

                                          SharedPreferences prefs18 =
                                              await SharedPreferences
                                                  .getInstance();
                                          prefs18.setString(
                                              'passlVerify', _password);
                                          print(
                                              "User Name signup mai ${_password}");

                                          SharedPreferences prefs19 =
                                              await SharedPreferences
                                                  .getInstance();
                                          prefs19.setString(
                                              'tokenVerify', tokenNew);
                                          print(
                                              "User Name signup mai ${tokenNew}");

                                          SharedPreferences prefs20 =
                                              await SharedPreferences
                                                  .getInstance();
                                          prefs20.setString(
                                              'phoneVerify', _phoneNumber);
                                          print(
                                              "User Name signup mai ${_phoneNumber}");

                                          SharedPreferences prefs21 =
                                              await SharedPreferences
                                                  .getInstance();
                                          prefs21.setString(
                                              'userNameVerify', _userName);
                                          print(
                                              "User Name signup mai ${_userName}");

                                          //
                                          // print("test123 ${response.body}");
                                          // var convertDataToJson = json.jsonDecode(response.body);
                                          // username = convertDataToJson['userName'];
                                          // fullName = convertDataToJson['fullName'];
                                          // email = convertDataToJson['email'];
                                          // token = convertDataToJson['token'];
                                          // id = convertDataToJson['id'];
                                          // var image = convertDataToJson['image'];
                                          // phoneNum = convertDataToJson['phoneNumber'];
                                          //
                                          // print("ye hai phone num signup mai$phoneNum");
                                          //
                                          // SharedPreferences prefs1 =
                                          // await SharedPreferences.getInstance();
                                          // prefs1.setString('fullName', fullName);
                                          // print("full name signup mai ${fullName}");
                                          //
                                          // SharedPreferences prefs5 =
                                          // await SharedPreferences.getInstance();
                                          // prefs5.setString('imageprofile', image);
                                          //
                                          //
                                          // SharedPreferences prefs6 =
                                          // await SharedPreferences.getInstance();
                                          // prefs6.setString('userEmail', email);
                                          // print("email signup mai ${email}");
                                          //
                                          //
                                          //
                                          // SharedPreferences prefs7 =
                                          // await SharedPreferences.getInstance();
                                          // prefs7.setString('username', username);
                                          // print("User Name signup mai ${username}");
                                          //
                                          //
                                          //
                                          // SharedPreferences prefs8 =
                                          // await SharedPreferences.getInstance();
                                          // prefs8.setString('cusId', id);
                                          // print("User Name signup mai ${id}");
                                          //
                                          // SharedPreferences prefs9 =
                                          // await SharedPreferences.getInstance();
                                          // prefs9.setString('token', token);
                                          // print("User Name signup mai ${token}");
                                          //
                                          //
                                          // SharedPreferences prefs10 =
                                          // await SharedPreferences.getInstance();
                                          // prefs10.setString('phoneNum', phoneNum);

                                          // Fluttertoast.showToast(
                                          //     msg: "Successfully Sign Up",
                                          //     toastLength: Toast.LENGTH_SHORT,
                                          //     gravity: ToastGravity.CENTER,
                                          //     timeInSecForIosWeb: 1);

                                          // SharedPreferences prefs =
                                          // await SharedPreferences.getInstance();
                                          // prefs.setString(
                                          //     'email', 'useremail@gmail.com');

                                          gotoNextScreen(context);
                                        } else {
                                          if (response.statusCode == 812) {
                                            Fluttertoast.showToast(
                                                msg: "Email already exists",
                                                toastLength: Toast.LENGTH_SHORT,
                                                gravity: ToastGravity.CENTER,
                                                timeInSecForIosWeb: 1);
                                          } else {
                                            if (response.statusCode == 811) {
                                              Fluttertoast.showToast(
                                                  msg:
                                                      "Password requirement is not fulfilled.",
                                                  toastLength: Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.CENTER,
                                                  timeInSecForIosWeb: 1);
                                            } else {
                                              if (response.statusCode == 809) {
                                                Fluttertoast.showToast(
                                                    msg:
                                                        "Please Enter Valid Email Address",
                                                    toastLength:
                                                        Toast.LENGTH_SHORT,
                                                    gravity: ToastGravity.CENTER,
                                                    timeInSecForIosWeb: 1);
                                              } else {
                                                if (response.statusCode == 810) {
                                                  Fluttertoast.showToast(
                                                      msg:
                                                          "Phone number is not valid!",
                                                      toastLength:
                                                          Toast.LENGTH_SHORT,
                                                      gravity:
                                                          ToastGravity.CENTER,
                                                      timeInSecForIosWeb: 1);
                                                } else {
                                                  if (response.statusCode ==
                                                      813) {
                                                    Fluttertoast.showToast(
                                                        msg:
                                                            "Phone number is already taken!",
                                                        toastLength:
                                                            Toast.LENGTH_SHORT,
                                                        gravity:
                                                            ToastGravity.CENTER,
                                                        timeInSecForIosWeb: 1);
                                                  } else {
                                                    if (response.statusCode ==
                                                        814) {
                                                      Fluttertoast.showToast(
                                                          msg:
                                                              "User Name is already taken!",
                                                          toastLength:
                                                              Toast.LENGTH_SHORT,
                                                          gravity:
                                                              ToastGravity.CENTER,
                                                          timeInSecForIosWeb: 1);
                                                    }
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      });
                                    }
                                  }
                                }
                              }
                            }
                          }
                        },
                        child: Text(
                          "Create Account",
                          style: TextStyle(
                              fontFamily: 'Gotham',
                              fontWeight: FontWeight.w300,
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(2.0),
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),

                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(10),
                  ),

                  Text(
                    "or",
                    style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      color: Color(0XFF5d5d5d),
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                    ),
                  ),

                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(0),
                  ),
                  GestureDetector(
                    onTap: () {
                      gotoNextScreenSignin(context);
                    },
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Text("Login via an existing account",
                          style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            color: Color(0XFF5d5d5d),
                            fontSize: ResponsiveFlutter.of(context).fontSize(1.8),
                            decoration: TextDecoration.underline,
                          )),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void firebaseCloudMessaging_Listeners() async {
    String _token = await SessionManager().getFirebaseToken();
    tokenNew = _token;
    // if (Platform.isIOS) iOS_Permission();

    // _firebaseMessaging.getToken().then((token) {
    //   print("tt ${token}");
    // });

    // _firebaseMessaging.onTokenRefresh.listen((token) async {
    //   print("refto ${token}");

    //   tokenNew = token;
    // });

    // _firebaseMessaging.configure(
    //   onMessage: (Map<String, dynamic> message) async {
    //     print('on message $message');
    //   },
    //   onResume: (Map<String, dynamic> message) async {
    //     print('on resume $message');
    //   },
    //   onLaunch: (Map<String, dynamic> message) async {
    //     print('on launch $message');
    //   },
    // );
  }

  // void iOS_Permission() {
  //   _firebaseMessaging.requestNotificationPermissions(
  //       IosNotificationSettings(sound: true, badge: true, alert: true));
  //   _firebaseMessaging.onIosSettingsRegistered
  //       .listen((IosNotificationSettings settings) {
  //     print("Settings registered: $settings");
  //   });
  // }

  gotoNextScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => VerificationEmailScreen(),
      ),
    );
  }

  gotoNextScreenSignin(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (_) => SignInScreen(),
      ),
    );
  }
}
