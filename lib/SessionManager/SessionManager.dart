import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
class SessionManager{

  Logout() async{
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  setFirebaseToken(token) async
  {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('FCMToken', token);
  }

  Future<String> getFirebaseToken () async {
    final prefs = await SharedPreferences.getInstance();
    var _token = prefs.getString('FCMToken');
    print(_token);
    return _token;
  }

}