import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:fixpay/Email.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/password.dart';
import 'package:fixpay/phone.dart';
import 'package:fixpay/profile.dart';
import 'package:fixpay/signIn.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as json;

import 'package:slider_button/slider_button.dart';

class FriendProfile extends StatefulWidget {
  @override
  _FriendProfileState createState() => _FriendProfileState();
}

class _FriendProfileState extends State<FriendProfile> {
  String imageProfile;
  String fullName;
  String userName;
  PlatformFile file;
  String cusId;
  String totalItem = "0";
  String totalBalance = "0";
  List list = new List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sharedPrefencess();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                  child: Image(
                    height: ResponsiveFlutter.of(context).verticalScale(120),
                    // width: 80,
                    image: AssetImage('assets/images/asset6@2x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Image.asset(
                'assets/images/asset3@3x.png',
                color: Color(0xFF1dbcba),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: ResponsiveFlutter.of(context).moderateScale(40)),
                  child: Text(
                    "Profile",
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.6),
                      color: Colors.white,
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(70),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height:
                              ResponsiveFlutter.of(context).verticalScale(80),
                          width: ResponsiveFlutter.of(context).scale(80),
                          child: imageProfile == null
                              ? Icon(
                                  Icons.account_circle_outlined,
                                  size: ResponsiveFlutter.of(context)
                                      .moderateScale(120),
                                  color: Color(0xFF1dbcba),
                                )
                              : CircleAvatar(
                                  radius: 25.0,
                                  backgroundImage: NetworkImage(
                                      'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                  // NetworkImage('http://hustledev-001-site8.itempurl.com/'+imageProfile),
                                  backgroundColor: Colors.transparent,
                                ),
                        ),
                        SizedBox(
                          width:
                              ResponsiveFlutter.of(context).verticalScale(20),
                        ),

                        // Padding(
                        //   padding: EdgeInsets.only(top: ResponsiveFlutter.of(context).verticalScale(100)),
                        //   child: Text("Ali Khan",
                        //     // Text(fullName == null ? 'Full Name' : fullName,
                        //     style: TextStyle(fontSize: ResponsiveFlutter.of(context).fontSize(2.5)
                        //     ),
                        //   ),
                        // ),
                        Text(
                          "Ali Khan",
                          // Text(fullName == null ? 'Full Name' : fullName,
                          style: TextStyle(
                              fontFamily: 'Gotham',
                              color: Color(0XFF5d5d5d),
                              fontWeight: FontWeight.w300,
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(2.2)),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(10),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: ResponsiveFlutter.of(context).scale(110),
                          height:
                              ResponsiveFlutter.of(context).verticalScale(110),
                          child: Stack(
                            children: [
                              Image(
                                width: ResponsiveFlutter.of(context).scale(110),
                                height: ResponsiveFlutter.of(context)
                                    .verticalScale(110),
                                image: AssetImage('assets/images/asset20.png'),
                                fit: BoxFit.contain,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: ResponsiveFlutter.of(context)
                                            .moderateScale(40)),
                                    child: Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Text(
                                        totalBalance.toString(),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(4),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "Total Points",
                                    style: TextStyle(
                                        fontFamily: 'Gotham',
                                        color: Color(0XFF5d5d5d),
                                        fontWeight: FontWeight.w300,
                                        fontSize: ResponsiveFlutter.of(context)
                                            .fontSize(1.5)
                                        // color: Colors.blueGrey,
                                        ),
                                  )
                                ],
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                            // color: Color(0xFFfad668),
                            borderRadius: BorderRadius.circular(60),
                          ),
                        ),
                        SizedBox(
                          width: ResponsiveFlutter.of(context).scale(10),
                        ),
                        Container(
                          //
                          width: ResponsiveFlutter.of(context).scale(110),
                          height:
                              ResponsiveFlutter.of(context).verticalScale(110),
                          child: Stack(
                            children: [
                              Image(
                                width: ResponsiveFlutter.of(context).scale(110),
                                height: ResponsiveFlutter.of(context)
                                    .verticalScale(110),
                                image: AssetImage('assets/images/asset19.png'),
                                fit: BoxFit.contain,
                              ),

                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        top: ResponsiveFlutter.of(context)
                                            .moderateScale(40)),
                                    child: Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Text(
                                        totalItem.toString(),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize:
                                                ResponsiveFlutter.of(context)
                                                    .fontSize(4),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Text(
                                    "Total Items",
                                    style: TextStyle(
                                        fontFamily: 'Gotham',
                                        color: Color(0XFF5d5d5d),
                                        fontWeight: FontWeight.w300,
                                        // color: Colors.blueGrey,
                                        fontSize: ResponsiveFlutter.of(context)
                                            .fontSize(1.5)
                                        // color: Colors.blueGrey,
                                        ),
                                  )
                                ],
                              ),

                              // Padding(
                              //   padding: const EdgeInsets.only(left:10),
                              //   child: Padding(
                              //     padding: const EdgeInsets.only(top:45.0),
                              //     child: Column(
                              //       crossAxisAlignment: CrossAxisAlignment.start, // for left side
                              //       children: [
                              //
                              //         Text(totalItem.toString(),
                              //           style: TextStyle(
                              //               color: Colors.white,
                              //               fontSize: 30,
                              //               fontWeight: FontWeight.bold
                              //           ),
                              //         ),
                              //
                              //         Padding(
                              //           padding: const EdgeInsets.only(left:5.0),
                              //           child: Text("Total Items",
                              //             style: TextStyle(
                              //               color: Colors.blueGrey,
                              //               fontSize: 12,
                              //               // color: Colors.blueGrey,
                              //             ),),
                              //         )
                              //       ],
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                          decoration: BoxDecoration(
                            // color: Color(0xFFfad668),
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(10),
                    ),
                    Container(
                      width: ResponsiveFlutter.of(context).scale(260),
                      height: ResponsiveFlutter.of(context).verticalScale(300),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: ResponsiveFlutter.of(context)
                                    .moderateScale(10)),
                            child: Text(
                              "Friend Items",
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  color: Color(0XFF5d5d5d),
                                  fontWeight: FontWeight.w400,
                                  // fontWeight: FontWeight.bold,
                                  // fontSize: 18,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(3)),
                            ),
                          ),
                          SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(5)),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.all(
                                  ResponsiveFlutter.of(context)
                                      .moderateScale(8)),
                              child: ListView.builder(
                                shrinkWrap: true,
                                padding: EdgeInsets.only(
                                    top: ResponsiveFlutter.of(context)
                                        .moderateScale(10)),
                                // itemCount: list.length == null ? 0 : list.length,
                                itemCount: list.length = 10,
                                itemBuilder: (BuildContext context, int index) {
                                  // print("test1234 ${list}");
                                  // print(list.length);

                                  return listViewMethod(context, index);
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> sharedPrefencess() async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    imageProfile = prefs4.getString('imageprofile');
    print("ye hai image profile ${imageProfile}");

    SharedPreferences prefs5 = await SharedPreferences.getInstance();
    fullName = prefs5.getString('fullName');
    print("ye hai full name ${fullName}");

    SharedPreferences prefs6 = await SharedPreferences.getInstance();
    userName = prefs6.getString('username');
    print("ye hai full name ${userName}");

    SharedPreferences prefs7 = await SharedPreferences.getInstance();
    cusId = prefs7.getString('cusId');
    print("ye hai user ID ${cusId}");

    setState(() {});
  }

  Widget listViewMethod(BuildContext context, int index) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(
            horizontal: 5.0,
          ),
          decoration: BoxDecoration(
            color: Color(0xFFEBFFFE),
            boxShadow: [
              BoxShadow(
                blurRadius: 1.0,
                color: Colors.black26,
                offset: Offset(0.0, 1.0),
                spreadRadius: 1.0,
              )
            ],
          ),
          height: ResponsiveFlutter.of(context).verticalScale(50),
          child: Row(
            children: [
              Text(
                "Pizza".toString(),
                style: TextStyle(
                    fontFamily: 'Gotham',
                    fontWeight: FontWeight.w300,
                    fontSize: ResponsiveFlutter.of(context).fontSize(2.5)),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              top: ResponsiveFlutter.of(context).moderateScale(10)),
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5.0,
            ),
            decoration: BoxDecoration(
              color: Color(0xFFEBFFFE),
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 1.0,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(50),
            child: Row(
              children: [
                Text(
                  "Burger".toString(),
                  style: TextStyle(
                      fontFamily: 'Gotham',
                      color: Color(0XFF5d5d5d),
                      fontWeight: FontWeight.w300,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.5)),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: ResponsiveFlutter.of(context).moderateScale(10),
          ),
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5.0,
            ),
            decoration: BoxDecoration(
              color: Color(0xFFEBFFFE),
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 1.0,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(50),
            child: Row(
              children: [
                Text(
                  "Ice Cream".toString(),
                  style: TextStyle(
                      fontFamily: 'Gotham',
                      color: Color(0XFF5d5d5d),
                      fontWeight: FontWeight.w300,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.5)),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
