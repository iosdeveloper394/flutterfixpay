import 'dart:convert' as json;
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:passwordfield/passwordfield.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Password extends StatefulWidget {
  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<Password> {
  final currentPass = TextEditingController();
  final newPass = TextEditingController();
  final confirmNewPass = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 80),
                  child: Image(
                    height: ResponsiveFlutter.of(context).verticalScale(140),
                    // width: 80,
                    image: AssetImage('assets/images/asset6@3x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Password",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Image.asset(
                        'assets/images/asset3@3x.png',
                        color: Color(0xFF1dbcba),
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(100),
                    ),
                    Container(
                      height: ResponsiveFlutter.of(context).verticalScale(160),
                      width: ResponsiveFlutter.of(context).scale(250),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment
                            .center, //Center Column contents vertically,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          // Container(
                          //   height: 35,
                          //   width: 200,
                          //   child: Center(child: TextField(
                          //     controller: currentPass,
                          //     textAlign: TextAlign.center,
                          //     decoration: InputDecoration.collapsed(hintText: "Current Password"),
                          //     style: TextStyle(
                          //         fontSize: 10
                          //     ),)),
                          //   decoration: BoxDecoration(
                          //       color: Color(0xFFFFFFFF),
                          //       borderRadius: BorderRadius.circular(20)
                          //   ),
                          // ),

                          Container(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                            width:
                                ResponsiveFlutter.of(context).verticalScale(200),
                            child: Center(
                              child: Align(
                                child: Container(
                                  height: ResponsiveFlutter.of(context)
                                      .verticalScale(30),
                                  width: ResponsiveFlutter.of(context)
                                      .verticalScale(160),
                                  child: TextField(
                                    style: TextStyle(
                                        color: Color(0XFF5d5d5d),
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w300,
                                        fontSize:
                                        ResponsiveFlutter.of(context)
                                            .fontSize(1.5)
                                    ),
                                    controller: currentPass,
                                    decoration: InputDecoration(
                                        isDense: true,
                                        border: InputBorder.none,
                                        hintText: 'Current Password'),
                                  ),
                                ),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFFFFF),
                                borderRadius: BorderRadius.circular(20)),
                          ),

                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10),
                          ),
                          Container(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                            width:
                                ResponsiveFlutter.of(context).verticalScale(200),
                            child: Center(
                              child: Align(
                                child: Container(
                                  height: ResponsiveFlutter.of(context)
                                      .verticalScale(30),
                                  width: ResponsiveFlutter.of(context)
                                      .verticalScale(160),
                                  child: TextField(
                                    style: TextStyle(
                                        color: Color(0XFF5d5d5d),
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w300,
                                        fontSize:
                                        ResponsiveFlutter.of(context)
                                            .fontSize(1.5)
                                    ),
                                    controller: newPass,
                                    decoration: InputDecoration(
                                        isDense: true,
                                        border: InputBorder.none,
                                        hintText: 'New Password'),
                                  ),
                                ),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFFFFF),
                                borderRadius: BorderRadius.circular(20)),
                          ),

                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10),
                          ),
                          Container(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                            width:
                                ResponsiveFlutter.of(context).verticalScale(200),
                            child: Center(
                              child: Align(
                                child: Container(
                                  height: ResponsiveFlutter.of(context)
                                      .verticalScale(30),
                                  width: ResponsiveFlutter.of(context)
                                      .verticalScale(160),
                                  child: TextField(
                                    style: TextStyle(
                                        color: Color(0XFF5d5d5d),
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w300,
                                        fontSize:
                                        ResponsiveFlutter.of(context)
                                            .fontSize(1.5)
                                    ),
                                    controller: confirmNewPass,
                                    decoration: InputDecoration(
                                        isDense: true,
                                        border: InputBorder.none,
                                        hintText: 'Confirm New Password'),
                                  ),
                                ),
                              ),
                            ),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFFFFF),
                                borderRadius: BorderRadius.circular(20)),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Color(0xFF1dbcba),
                          borderRadius: BorderRadius.circular(5)),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(10),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 25.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Center(
                          child: Text(
                            "Minimun 8 Characters allowed with",
                            style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                                color: Colors.blueGrey.shade800),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 25.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Center(
                          child: Text(
                            "one uppercase, one special character.",
                            style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.5),
                                color: Colors.blueGrey.shade800),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(120),
                    ),
                    GestureDetector(
                      onTap: () async {
                        String _currentPass = currentPass.text;
                        String _newPass = newPass.text;
                        String _confirmNewPass = confirmNewPass.text;

                        if (_confirmNewPass == null || _currentPass == "") {
                          Fluttertoast.showToast(
                              msg: "Write Your current Password ",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else if (_newPass == null || _newPass == "") {
                          Fluttertoast.showToast(
                              msg: "Enter Your New Password ",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else if (_confirmNewPass == null ||
                            _confirmNewPass == "") {
                          Fluttertoast.showToast(
                              msg: "Confirm Your New Password.",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else if (_newPass != _confirmNewPass) {
                          Fluttertoast.showToast(
                              msg: "Password Not Same",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else {
                          SharedPreferences prefs4 =
                              await SharedPreferences.getInstance();
                          var id = prefs4.getString('cusId');

                          print(" ha ye id  ${id}");

                          print("ye hai new pass  ${_newPass}");
                          print("ye hai conf pass ${_confirmNewPass}");
                          print("ye hai current pass , ${_currentPass}");

                          progressShow(context);
                          Map params = {
                            // 'CustomerId': id,
                            'OldPassword': _currentPass,
                            'NewPassword': _newPass,
                          };

                          await HttpsRequest()
                              .fetchPostTokenData(Constant.changePass, params)
                              .then((response) async {
                            progressHide(context);
                            print("asasas ${response.statusCode}");

                            if (response.statusCode == 200) {
                              Fluttertoast.showToast(
                                  msg: "Password updated successfully",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);

                              // gotoNextScreen(context);

                            } else if (response.statusCode == 805) {
                              Fluttertoast.showToast(
                                  msg:
                                      "New Password must be different to old password!",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                            } else if (response.statusCode == 811) {
                              Fluttertoast.showToast(
                                  msg: "New password is not valid!",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                            } else if (response.statusCode == 803) {
                              Fluttertoast.showToast(
                                  msg: "Old password verification failed!",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                            } else {
                              Fluttertoast.showToast(
                                  msg: "No Internet",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                            }
                          });
                        }
                      },
                      child: Container(
                        height: ResponsiveFlutter.of(context).verticalScale(50),
                        width: ResponsiveFlutter.of(context).scale(220),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .center, //Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.save_alt,
                              size: ResponsiveFlutter.of(context).fontSize(4),
                              color: Color(0xFF1dbcba),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(70),
                            ),
                            Text(
                              ("Save Changes"),
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Color(0xFFFFFFFF),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Password",
                    style: TextStyle(
                        // color: Colors.white,

                      color: Colors.white,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
