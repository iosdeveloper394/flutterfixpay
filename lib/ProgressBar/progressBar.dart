import 'package:flutter/material.dart';
//import 'CustomColors.dart';

void progressShow(context) {
  // flutter defined function
  showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      // return object of type Dialog
      return Align(
          alignment: Alignment.center,
          child:CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Color(0xFFfdda65)

          ) ,));
    },
  );
}
void progressHide(context) {
  Navigator.pop(context);
}