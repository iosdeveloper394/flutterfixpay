import 'package:flutter/material.dart';

Container lightTextTitle(String textToShow, BuildContext context) {
  return Container(
    child: Center(
        child: DefaultTextStyle(
      style: TextStyle(),
      child: Text(textToShow,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.subtitle1),
    )),
  );
}

Container boldTextTitle(String textToShow, BuildContext context) {
  return Container(
    child: Center(
      child: DefaultTextStyle(
        style: TextStyle(),
        child: Text(textToShow,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.title),
      ),
    ),
  );
}

Container buildTextLineWithColor(
    String textToShow, double fontSize, Color textColor) {
  return Container(
    child: Center(
        child: DefaultTextStyle(
      style: TextStyle(),
      child: Text(
        textToShow,
        textAlign: TextAlign.center,
        // style: Theme.of(context).textTheme.title
        style: TextStyle(
            fontFamily: 'Aleo',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.bold,
            fontSize: fontSize,
            color: textColor),
      ),
    )),
  );
}

Container buildTextLineWithColorLeft(
    String textToShow, double fontSize, Color textColor) {
  return Container(
    alignment: Alignment.centerLeft,
    padding: EdgeInsets.all(5.0),
    child: DefaultTextStyle(
      style: TextStyle(),
      child: Text(
        textToShow,
        textAlign: TextAlign.start,
        style: TextStyle(fontFamily: '', fontSize: fontSize, color: textColor),
      ),
    ),
  );
}

Container buildTextLineWithColorCenter(
    String textToShow, double fontSize, Color textColor) {
  return Container(
    alignment: Alignment.center,
    padding: EdgeInsets.all(5.0),
    child: DefaultTextStyle(
      style: TextStyle(),
      child: Text(
        textToShow,
        textAlign: TextAlign.start,
        style: TextStyle(
            fontFamily: '',
            fontStyle: FontStyle.normal,
            fontWeight: FontWeight.bold,
            fontSize: fontSize,
            color: textColor),
      ),
    ),
  );
}
