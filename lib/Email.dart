import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Redeem.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Email extends StatefulWidget {
  @override
  _EmailState createState() => _EmailState();
}

class _EmailState extends State<Email> {
  String userEmail;
  String useNewEmail;

  // TextEditingController userNameController;
  final userNewEmailController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // userNameController = TextEditingController();
    Data();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 80),
                  child: Image(
                    height: ResponsiveFlutter.of(context).verticalScale(140),
                    // width: 80,
                    image: AssetImage('assets/images/asset6@3x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        height: ResponsiveFlutter.of(context).verticalScale(98),
                        child:
                        Image.asset('assets/images/asset3@3x.png',
                        color: Color(0xFF1dbcba),
                      ),
                        // Image(
                        //   image: AssetImage('assets/images/asset3@3x.png'),
                        // ),
                      ),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(100),
                    ),

                    Container(
                      height: ResponsiveFlutter.of(context).verticalScale(160),
                      width: ResponsiveFlutter.of(context).scale(250),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment
                            .center, //Center Column contents vertically,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                            width:
                                ResponsiveFlutter.of(context).verticalScale(200),
                            child: Center(
                                child: Text(
                              userEmail.toString(),
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                color: Colors.blueGrey.shade800,
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                              ),
                            )),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFFFFF),
                                borderRadius: BorderRadius.circular(20)),
                          ),
                          SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(10)),
                          Container(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(35),
                            width:
                                ResponsiveFlutter.of(context).verticalScale(200),
                            child: Center(
                                child: TextField(
                              controller: userNewEmailController,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration.collapsed(
                                  hintText: "New Email",hintStyle: TextStyle(

                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                color: Colors.blueGrey.shade800,
                                fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.5),

                              ) ),
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                color: Colors.blueGrey.shade800,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                              ),
                            )),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFFFFF),
                                borderRadius: BorderRadius.circular(20)),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                          color: Color(0xFF1dbcba),
                          borderRadius: BorderRadius.circular(5)),
                    ),

                    SizedBox(
                        height: ResponsiveFlutter.of(context).verticalScale(160)),
                    // Container(
                    //   height: 40,
                    //   width: 180,
                    //   child: RaisedButton(
                    //
                    //     color: Color(0xFFfad668),
                    //     shape: RoundedRectangleBorder(
                    //         borderRadius: BorderRadius.circular(15)),
                    //     onPressed: () async {
                    //
                    //       String _currentEmail = userEmail;
                    //       String _newEmail = userNewEmailController.text.toString();
                    //       print("aabbcc$_newEmail");
                    //
                    //       if(_currentEmail == null || _currentEmail == "" ) {
                    //
                    //
                    //         Fluttertoast.showToast(
                    //             msg: "Write Your current Email",
                    //             toastLength: Toast.LENGTH_SHORT,
                    //             gravity: ToastGravity.CENTER,
                    //             timeInSecForIosWeb: 1);
                    //
                    //       }else if (_newEmail == null || _newEmail == "")
                    //       {
                    //         Fluttertoast.showToast(
                    //             msg: "Write Your New Email ",
                    //             toastLength: Toast.LENGTH_SHORT,
                    //             gravity: ToastGravity.CENTER,
                    //             timeInSecForIosWeb: 1);
                    //
                    //       }else{
                    //
                    //         SharedPreferences prefs4 = await SharedPreferences.getInstance();
                    //         var id = prefs4.getString('cusId');
                    //         print(" ha ye id  ${id}");
                    //         print("ye hai new Email  ${_newEmail}");
                    //         print("ye hai Current Email ${_currentEmail}");
                    //
                    //         progressShow(context);
                    //
                    //         Map params = {
                    //           'CustomerId': id,
                    //           'OldEmail': _currentEmail,
                    //           'NewEmail': _newEmail,
                    //         };
                    //
                    //         await HttpsRequest()
                    //             .fetchPostTokenData(Constant.changeEmail, params)
                    //             .then((response) async {
                    //           progressHide(context);
                    //           print("asasas ${response.statusCode}");
                    //
                    //           if (response.statusCode == 200) {
                    //
                    //             userNewEmailController.clear();
                    //
                    //             Fluttertoast.showToast(
                    //                 msg: "Email updated successfully",
                    //                 toastLength: Toast.LENGTH_SHORT,
                    //                 gravity: ToastGravity.CENTER,
                    //                 timeInSecForIosWeb: 1);
                    //
                    //             SharedPreferences prefs6 =
                    //             await SharedPreferences.getInstance();
                    //             prefs6.setString('userEmail',_newEmail);
                    //             print("email signup mai ${_newEmail}");
                    //
                    //             // setState(() {
                    //             // });
                    //
                    //             Data();
                    //
                    //             // gotoNextScreen(context);
                    //
                    //           }  else if(response.statusCode == 812){
                    //
                    //         Fluttertoast.showToast(
                    //         msg: "Email is already taken",
                    //         toastLength: Toast
                    //             .LENGTH_SHORT,
                    //         gravity: ToastGravity.CENTER,
                    //         timeInSecForIosWeb: 1);
                    //
                    //           }});
                    //       }
                    //       },
                    //
                    //     child: Text("Update"
                    //       , style: TextStyle(
                    //           fontSize: 18,
                    //           color: Colors.white
                    //       ),),
                    //   ),
                    // ),

                    GestureDetector(
                      onTap: () async {
                        String _currentEmail = userEmail;
                        String _newEmail = userNewEmailController.text.toString();
                        print("aabbcc$_newEmail");

                        if (_currentEmail == null || _currentEmail == "") {
                          Fluttertoast.showToast(
                              msg: "Write Your current Email",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else if (_newEmail == null || _newEmail == "") {
                          Fluttertoast.showToast(
                              msg: "Write Your New Email ",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else {
                          SharedPreferences prefs4 =
                              await SharedPreferences.getInstance();
                          var id = prefs4.getString('cusId');
                          print(" ha ye id  ${id}");
                          print("ye hai new Email  ${_newEmail}");
                          print("ye hai Current Email ${_currentEmail}");

                          progressShow(context);

                          Map params = {
                            'CustomerId': id,
                            'OldEmail': _currentEmail,
                            'NewEmail': _newEmail,
                          };

                          await HttpsRequest()
                              .fetchPostTokenData(Constant.changeEmail, params)
                              .then((response) async {
                            progressHide(context);
                            print("asasas ${response.statusCode}");

                            if (response.statusCode == 200) {
                              userNewEmailController.clear();

                              Fluttertoast.showToast(
                                  msg: "Email updated successfully",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);

                              SharedPreferences prefs6 =
                                  await SharedPreferences.getInstance();
                              prefs6.setString('userEmail', _newEmail);
                              print("email signup mai ${_newEmail}");

                              // setState(() {
                              // });

                              Data();

                              // gotoNextScreen(context);

                            } else if (response.statusCode == 812) {
                              Fluttertoast.showToast(
                                  msg: "Email is already taken",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                            } else if (response.statusCode == 809) {
                              Fluttertoast.showToast(
                                  msg: "Email is not valid",
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.CENTER,
                                  timeInSecForIosWeb: 1);
                            }
                          });
                        }
                      },
                      child: Container(
                        height: ResponsiveFlutter.of(context).verticalScale(50),
                        width: ResponsiveFlutter.of(context).scale(220),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment
                              .center, //Center Column contents vertically,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.save_alt,
                              size: ResponsiveFlutter.of(context).fontSize(4),
                              color: Color(0xFF1dbcba),
                            ),
                            SizedBox(
                              width: ResponsiveFlutter.of(context).scale(70),
                            ),
                            Text(
                              ("Save Changes"),
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                              ),
                            ),
                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Color(0xFFFFFFFF),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Email",
                    style: TextStyle(

                      color: Colors.white,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> Data() async {
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    userEmail = prefs1.getString('userEmail');
    print("email change mai 123${userEmail}");

    setState(() {});
    // userNameController.text = userEmail;
  }
}
