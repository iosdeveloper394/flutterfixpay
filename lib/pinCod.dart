


import 'dart:convert' as json;
import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Wallet.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/loyalityPoints.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinCodeScreen extends StatefulWidget {
  @override
  _PinCodeScreenState createState() => _PinCodeScreenState();
}

class _PinCodeScreenState extends State<PinCodeScreen>{

  final pinCodeController = TextEditingController();


  String merchantId;
  String profileImage;
  String nameBrandPinCodeScreen;
  String datePinCodeScreen;
  String pointsSend;
  int pointSendInt;

  @override
  void initState() {
    // TODO: implement initState
    apiMethod(context);
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;


    return
      Scaffold(
        backgroundColor: Color(0xFFEBFFFE),


        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,

            child: Stack(

              children: [



                Image.asset('assets/images/asset3@3x.png',
                  color: Color(0xFF1dbcba),
                ),



                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      child: Image(
                        height: 80,
                        width: 80,
                        image: AssetImage('assets/images/12.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),

                Align(
                  alignment: Alignment.center,
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,

                    children: [

                      SizedBox(height: 70,),

                      Text("Pin Code "
                        ,style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),),


                      SizedBox(
                        height: 100,
                      ),


                      Text("Pin Code",
                        style: TextStyle(

                            fontSize: 18,
                            fontWeight: FontWeight.bold
                        ),
                      ),

                      SizedBox(height: 10,),

                      Text("Please set a 4 digit long Wallet Pin."
                        ,style: TextStyle(
                          fontSize: 12,

                        ),),

                      Text("Memorize it and keep it confidential."
                        ,style: TextStyle(
                          fontSize: 12,

                        ),),


                      SizedBox(
                        height: 30,
                      ),

                      VerificationCode(
                        textStyle: TextStyle(fontSize: 40.0, color: Color(0xFF1dbcba),
                            fontWeight: FontWeight.bold
                        ),
                        underlineColor: Colors.black,
                        keyboardType: TextInputType.number,
                        // controller: pinCodeController,

                        length: 4,
                        // clearAll is NOT required, you can delete it
                        // takes any widget, so you can implement your design

                        onCompleted: (String value) {
                          setState(() {
                            var _code = value;
                            pointsSend = _code;

                            print("ye hai String${pointsSend}"); //


                            pointSendInt = int.parse(pointsSend);
                            assert(pointSendInt is int);
                            print("ye hai myInt${pointSendInt}"); //
                          });
                        },

                        onEditing: (bool value) {
                          setState(() {
                            var _onEditing = value;
                            print("ye hai Edit ${_onEditing}");

                          });
                        },
                      ),



                      SizedBox(height: 50,
                      ),

                      GestureDetector(
                        onTap: () async {

                          print("ye hai code ${pointSendInt}");


                          SharedPreferences prefs4 = await SharedPreferences.getInstance();
                          var email = prefs4.getString('emailVerify');
                          print("email ha ye ${email}");


                          SharedPreferences prefs5 = await SharedPreferences.getInstance();
                          var pass = prefs5.getString('passlVerify');
                          print("email ha ye ${pass}");

                          SharedPreferences prefs6 = await SharedPreferences.getInstance();
                          var phone = prefs6.getString('phoneVerify');
                          print("email ha ye ${phone}");

                          SharedPreferences prefs7 = await SharedPreferences.getInstance();
                          var token = prefs7.getString('tokenVerify');
                          print("email ha ye ${token}");

                          SharedPreferences prefs8 = await SharedPreferences.getInstance();
                          var userName = prefs8.getString('userNameVerify');
                          print("email ha ye ${userName}");



                          Map params = {
                              'UserName': userName,
                              'Password': pass,
                              'FirebaseToken': token,
                              'Email': email,
                              'PhoneNumber': phone,
                              'PinCode': pointSendInt,

                            };

                            progressShow(context);
                            await HttpsRequest()
                                .fetchPostMyAllFriendsData(
                                Constant.PinCodeSend, params)
                                .then((response) async {
                              progressHide(context);

                              print("asasas ${response.statusCode}");
                              if (response.statusCode == 200) {

                                print("test123 ${response.body}");
                                var convertDataToJson = json.jsonDecode(response.body);
                                var username = convertDataToJson['userName'];
                                var fullName = convertDataToJson['fullName'];
                                email = convertDataToJson['email'];
                                token = convertDataToJson['token'];
                                var id = convertDataToJson['id'];
                                var image = convertDataToJson['image'];
                                var phoneNum = convertDataToJson['phoneNumber'];
                                var totalBalance = convertDataToJson['totalBalance'];
                                var totalItem = convertDataToJson['totalItem'];

                                print("ye hai phone num pinCode mai$totalItem");
                                print("ye hai phone num pinCode mai$totalBalance");


                                SharedPreferences prefs12 =
                                await SharedPreferences.getInstance();
                                prefs12.setString('totalBalance',totalBalance);



                                SharedPreferences prefs13 =
                                await SharedPreferences.getInstance();
                                prefs13 .setString('totalItem', totalItem);



                                SharedPreferences prefs1 =
                                await SharedPreferences.getInstance();
                                prefs1.setString('fullName', fullName);
                                print("full name signup mai ${fullName}");

                                SharedPreferences prefs5 =
                                await SharedPreferences.getInstance();
                                prefs5.setString('imageprofile', image);


                                SharedPreferences prefs6 =
                                await SharedPreferences.getInstance();
                                prefs6.setString('userEmail', email);
                                print("email signup mai ${email}");



                                SharedPreferences prefs7 =
                                await SharedPreferences.getInstance();
                                prefs7.setString('username', username);
                                print("User Name signup mai ${username}");



                                SharedPreferences prefs8 =
                                await SharedPreferences.getInstance();
                                prefs8.setString('cusId', id);
                                print("User Name signup mai ${id}");

                                SharedPreferences prefs9 =
                                await SharedPreferences.getInstance();
                                prefs9.setString('token', token);
                                print("User Name signup mai ${token}");


                                SharedPreferences prefs10 =
                                await SharedPreferences.getInstance();
                                prefs10.setString('phoneNum', phoneNum);


                                SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                                prefs.setString(
                                    'email', 'useremail@gmail.com');

                                Fluttertoast.showToast(
                                    msg: "Successfully Sign Up",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1);


                                print("ye hai code 200 ${pointSendInt}");
                                Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => HomeScreen()));



                                Fluttertoast.showToast(
                                    msg: "Points Successfully PinCodeScreened",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1);


                              } else {
                                Fluttertoast.showToast(
                                    msg: "InterNet Failed",
                                    toastLength: Toast.LENGTH_SHORT,
                                    gravity: ToastGravity.CENTER,
                                    timeInSecForIosWeb: 1);
                              }
                            });


                        },
                        child: Container(
                          width: 180,
                          child:
                          Center(
                            child: Text("Continue",
                              style: TextStyle(
                                  color: Color(0xFFfdda65),
                                  fontWeight: FontWeight.bold
                              ),

                            ),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xFF1dbcba),
                          ),
                          height: 40,
                        ),
                      ),



                    ],
                  ),
                ),



              ],
            ),
          ),
        ),
      );




  }

  Future<void> apiMethod(BuildContext context) async {


    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");



    SharedPreferences prefs5 = await SharedPreferences.getInstance();
    profileImage = prefs5.getString('imgaeProfilePinCodeScreen');

    print("profilePinCodeScreen${profileImage}");


    SharedPreferences prefs6 = await SharedPreferences.getInstance();
    nameBrandPinCodeScreen = prefs6.getString('nameBrandPinCodeScreen');

    print("nameBrandPinCodeScreen${nameBrandPinCodeScreen}");


    SharedPreferences prefs7 = await SharedPreferences.getInstance();
    datePinCodeScreen = prefs7.getString('datePinCodeScreen');

    print("datePinCodeScreen${datePinCodeScreen}");



    setState(() {});

  }
}
