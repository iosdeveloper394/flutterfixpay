import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Wallet.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/loyalityPoints.dart';
import 'package:fixpay/redeemPinCode.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Redeem extends StatefulWidget {
  @override
  _RedeemState createState() => _RedeemState();
}

class _RedeemState extends State<Redeem> {
  final pinCodeController = TextEditingController();

  String merchantId;
  String profileImage;
  String nameBrandRedeem;
  String dateRedeem;
  String pointsSend;
  String pinCodeSend;
  int pointSendInt;
  int pinCodeSendInt;

  @override
  void initState() {
    // TODO: implement initState
    apiMethod(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: ResponsiveFlutter.of(context).verticalScale(98),
                  child: Image.asset(
                    'assets/images/asset3@3x.png',
                    color: Color(0xFF1dbcba),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                  child: Image.asset(
                    'assets/images/asset6@2x.png',
                    height: ResponsiveFlutter.of(context).verticalScale(100),
                    color: Color(0xFFfdda65),
                  ),

                  // Image(
                  //   height: ResponsiveFlutter.of(context).verticalScale(100),
                  //   // width: 80,
                  //   image: AssetImage('assets/images/asset6@2x.png'),
                  //   fit: BoxFit.cover,
                  // ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: ResponsiveFlutter.of(context).moderateScale(40)),
                  child: Text(
                    "Redeem",
                    style: TextStyle(
                      fontFamily: 'Gotham',
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.6),
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(120),
                    ),
                    Container(
                      width: ResponsiveFlutter.of(context).scale(140),
                      height: ResponsiveFlutter.of(context).verticalScale(140),
                      child:
                          // profileImage == null  ?
                          // Icon(Icons.account_circle_outlined,
                          //   size: ResponsiveFlutter.of(context).fontSize(16),
                          //   color: Color(0xFF1dbcba),
                          // ):
                          CircleAvatar(
                        radius: 25.0,
                        backgroundImage: NetworkImage(
                            'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                        backgroundColor: Colors.transparent,
                      ),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.orange),
                          color: Colors.white,
                          shape: BoxShape.circle),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(12),
                    ),
                    Text(
                      "abc".toString(),
                      style: TextStyle(
                        fontFamily: 'Gotham',
                        fontSize: ResponsiveFlutter.of(context)
                            .fontSize(3),

                        color: Colors.blueGrey.shade800,
                        // fontFamily: 'Gotham',
                        fontWeight: FontWeight.w400,),
                    ),
                    Text(
                      "24 03 2021".toString(),
                      style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                    ),
                    Text(
                      "How many points ?",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          color: Colors.blueGrey.shade800,
                          fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                          fontWeight: FontWeight.w400),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(10),
                    ),
                    VerificationCode(
                      textStyle: TextStyle(
                          fontFamily: 'Gotham',
                          fontSize: ResponsiveFlutter.of(context).fontSize(4),
                          color: Color(0xFF1dbcba),
                          fontWeight: FontWeight.w400),
                      underlineColor: Colors.black,
                      keyboardType: TextInputType.number,
                      // controller: pinCodeController,

                      length: 3,
                      // clearAll is NOT required, you can delete it
                      // takes any widget, so you can implement your design

                      onCompleted: (String value) {
                        setState(() {
                          var _code = value;
                          pointsSend = _code;

                          print("ye hai String${pointsSend}"); //

                          pointSendInt = int.parse(pointsSend);
                          assert(pointSendInt is int);
                          print("ye hai myInt${pointSendInt}"); //
                        });
                      },

                      onEditing: (bool value) {
                        setState(() {
                          var _onEditing = value;
                          print("ye hai Edit ${_onEditing}");
                        });
                      },
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(40),
                    ),
                    GestureDetector(
                      onTap: () async {
                        print("ye hai code ${pointSendInt}");

                        if (pointSendInt == null || pointSendInt == "") {
                          Fluttertoast.showToast(
                              msg: "Add Points",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else {
                          SharedPreferences prefs17 =
                              await SharedPreferences.getInstance();
                          prefs17.setString(
                              'pointSendInt', pointSendInt.toString());
                          print("User Name signup mai ${pointSendInt}");

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RedeemPinCode()),
                          );

                          // Map params = {
                          //   'MerchantId': merchantId,
                          //   'ShareRewardPoints': pointSendInt
                          // };

                          // progressShow(context);
                          // await HttpsRequest()
                          //     .fetchPostMyAllFriendsData(
                          //     Constant.redeemPointSend, params)
                          //     .then((response) async {
                          //   progressHide(context);
                          //
                          //   print("asasas ${response.statusCode}");
                          //   if (response.statusCode == 200) {
                          //
                          //
                          //     pointSendInt = null;
                          //
                          //     print("ye hai code 200 ${pointSendInt}");
                          //     Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => Wallet()));
                          //
                          //
                          //
                          //     Fluttertoast.showToast(
                          //         msg: "Points Successfully Redeemed",
                          //         toastLength: Toast.LENGTH_SHORT,
                          //         gravity: ToastGravity.CENTER,
                          //         timeInSecForIosWeb: 1);
                          //
                          //
                          //   } else {
                          //     Fluttertoast.showToast(
                          //         msg: "InterNet Failed",
                          //         toastLength: Toast.LENGTH_SHORT,
                          //         gravity: ToastGravity.CENTER,
                          //         timeInSecForIosWeb: 1);
                          //   }
                          // });
                        }
                      },
                      child: Container(
                        width: ResponsiveFlutter.of(context).scale(180),
                        child: Center(
                          child: Text(
                            "Continue",
                            style: TextStyle(
                                fontFamily: 'Gotham',
                                color: Color(0xFFfdda65),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xFF1dbcba),
                        ),
                        height: ResponsiveFlutter.of(context).verticalScale(50),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    SharedPreferences prefs5 = await SharedPreferences.getInstance();
    profileImage = prefs5.getString('imgaeProfileRedeem');

    print("profileRedeem${profileImage}");

    SharedPreferences prefs6 = await SharedPreferences.getInstance();
    nameBrandRedeem = prefs6.getString('nameBrandRedeem');

    print("nameBrandRedeem${nameBrandRedeem}");

    SharedPreferences prefs7 = await SharedPreferences.getInstance();
    dateRedeem = prefs7.getString('dateRedeem');

    print("dateRedeem${dateRedeem}");

    setState(() {});
  }
}
