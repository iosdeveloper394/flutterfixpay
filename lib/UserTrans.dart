import 'dart:convert' as json;
import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ItemPinCode.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Redeem.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/shareWithFriend.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserTrans extends StatefulWidget {
  @override
  _UserTransState createState() => _UserTransState();
}

class _UserTransState extends State<UserTrans> {
  List list = new List();
  int totalPoints = 0;
  String imgaeProfile;
  String date = "Date";
  String nameBrand = "Name";

  @override
  void initState() {
    // TODO: implement initState
    // apiMethod(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                child: Image.asset(
                  'assets/images/asset6@2x.png',
                  height: ResponsiveFlutter.of(context).verticalScale(120),
                  color: Color(0xFFfdda65),
                ),

                // Image(
                //   height: ResponsiveFlutter.of(context).verticalScale(120),
                //   // width: 80,
                //   image: AssetImage('assets/images/asset6@2x.png'),
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            Column(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Image.asset(
                    'assets/images/asset3@3x.png',
                    color: Color(0xFF1dbcba),
                  ),
                ),

                // Expanded(
                //   child:
                Container(
                  width: ResponsiveFlutter.of(context).scale(310),
                  height: ResponsiveFlutter.of(context).verticalScale(550),
                  child: Column(
                    children: [
                      SizedBox(
                        height: ResponsiveFlutter.of(context).verticalScale(10),
                      ),
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.only(
                              bottom: ResponsiveFlutter.of(context)
                                  .moderateScale(30),
                              top: ResponsiveFlutter.of(context)
                                  .moderateScale(20)),
                          child: ListView.builder(
                            shrinkWrap: true,
                            padding: EdgeInsets.only(top: 10),
                            itemCount: list.length = 20,
                            // itemCount: list.length == null ? 20 : list.length,
                            itemBuilder: (BuildContext context, int index) {
                              print("test1234 ${list}");
                              // print(list.length);

                              return listViewMethod(context, index);
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    // color: Color(0xFFEBFFFE),
                  ),
                ),
                // ),

                // new GestureDetector(
                //   onTap: (){
                //
                //     _openPopup(context);
                //   },
                //   child: Container(
                //     width: 260,
                //
                //     child:
                //
                //     Row(
                //       children: [
                //         SizedBox(
                //           height: 10,
                //         ),
                //         Padding(
                //           padding: const EdgeInsets.only(left:30.0),
                //           child: Text("Whooper with fries",
                //             style: TextStyle(
                //                 fontSize: 12
                //             ),),
                //         ),
                //
                //
                //         Padding(
                //           padding: const EdgeInsets.only(left:50.0),
                //           child: Container(
                //             width: 60,
                //             height: 20,
                //             child: Center(
                //               child: Text(
                //                 'Reedem',
                //                 style: TextStyle(color: Colors.black,
                //                     fontSize: 12,
                //                     fontWeight: FontWeight.bold),
                //               ),
                //             ),
                //             decoration: BoxDecoration(
                //               borderRadius: BorderRadius.circular(5),
                //               color: Color(0xFF1dbcba),
                //             ),
                //           ),
                //         ),
                //       ],
                //     ),
                //
                //     decoration: BoxDecoration(
                //       borderRadius: BorderRadius.circular(10),
                //       color: Colors.white,
                //
                //     ),
                //     height: 40,
                //   ),
                // ),
              ],
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: EdgeInsets.only(
                    top: ResponsiveFlutter.of(context).moderateScale(40)),
                child: Text(
                  "Transactions",
                  style: TextStyle(
                    fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                    color: Colors.white,
                    fontFamily: 'Gotham',
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _openPopup(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            content: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 30,
                    ),

                    Container(
                      width: 180,
                      child: Center(
                        child: Text(
                          "Generate QR",
                          style: TextStyle(
                              color: Color(0xFFfdda65),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xFF1dbcba),
                      ),
                      height: 40,
                    ),

                    SizedBox(
                      height: 20,
                    ),

                    Container(
                      width: 180,
                      child: Center(
                        child: Text(
                          "Generate QR",
                          style: TextStyle(
                              color: Color(0xFFfdda65),
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xFF1dbcba),
                      ),
                      height: 40,
                    ),

                    // SizedBox(
                    //   height: 30,
                    // ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    String _merchantId = merchantId.toString();

    var params = {'MerchantId': _merchantId};

    progressShow(context);

    await HttpsRequest()
        .fetchPostMerchantsForCustomer(Constant.totalItems, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        list = convertDataToJson['merchatto']['cashbackRecentItemList'];

        print("ye hai list loy${list}");

        // totalPoints = convertDataToJson['merchatto']['totalBalance']['totalBalance'];
        date = convertDataToJson['merchatto']['merchant']['createDate'];
        nameBrand = convertDataToJson['merchatto']['merchant']['brandName'];
        imgaeProfile = convertDataToJson['merchatto']['merchant']['image'];

        String myDate = date;
        DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm").parse(myDate);
        date = "${tempDate.day} ${tempDate.month} ${tempDate.year}";
        print("ye hai date convert");
        print(date);

        SharedPreferences prefs13 = await SharedPreferences.getInstance();
        prefs13.setString('imgaeProfileRedeem', imgaeProfile);

        SharedPreferences prefs12 = await SharedPreferences.getInstance();
        prefs12.setString('nameBrandRedeem', nameBrand);

        SharedPreferences prefs14 = await SharedPreferences.getInstance();
        prefs14.setString('dateRedeem', date);

        setState(() {});
      } else if (response.statusCode == 500) {
        // list = new List();
        Fluttertoast.showToast(
            msg: "No Item",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      } else {
        Fluttertoast.showToast(
            msg: "No Inter Net",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }

  Widget listViewMethod(BuildContext context, int index) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10.0,
            ),
            decoration: BoxDecoration(
              color: Color(0xFFFFFFFF),
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 0.5,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    child: Text(
                  "Burget King".toString(),
                  style: TextStyle(
                      color: Colors.blueGrey.shade800,
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                )
                    // child: Text(list[index]['brandName'].toString())
                    ),
                SizedBox(
                  width: ResponsiveFlutter.of(context).scale(70),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                        "Shared",
                        // list[index]['amount'].toString(),
                        style: TextStyle(
                            color: Color(0xFF1dbcba),

                            // color: Colors.blueGrey.shade800,
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w400,
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.5)),
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(5),
                    ),
                    Text(
                      "20/3/2021",
                      style: TextStyle(
                          color: Colors.blueGrey.shade800,
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          fontSize:
                              ResponsiveFlutter.of(context).fontSize(1.5)),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 10.0,
            ),
            decoration: BoxDecoration(
              color: Color(0xFFFFFFFF),
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 0.5,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    child: Text(
                  "50".toString(),
                  style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      fontSize: ResponsiveFlutter.of(context).fontSize(1.5)),
                )
                    // child: Text(list[index]['brandName'].toString())
                    ),
                SizedBox(
                  width: ResponsiveFlutter.of(context).scale(145),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                        "Redeemed",
                        // list[index]['amount'].toString(),
                        style: TextStyle(
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF1dbcba),
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.5)),
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(5),
                    ),
                    Text("5/4/2021",
                        style: TextStyle(
                            color: Colors.blueGrey.shade800,
                            fontFamily: 'Gotham',
                            fontWeight: FontWeight.w300,
                            fontSize:
                                ResponsiveFlutter.of(context).fontSize(1.5))),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
