


import 'dart:convert' as json;
import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FriendRequest extends StatefulWidget {
  bool checkResponse = false;

  @override
  _FriendRequestState createState() => _FriendRequestState();
}

class _FriendRequestState extends State<FriendRequest>{

  String id;
  List list = new List();


  void initState() {
    // TODO: implement initState
    super.initState();
    requestFriends(context);

  }
  
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return
      Scaffold(
        backgroundColor: Color(0xFFEBFFFE),


        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,
            child: Stack(

              children: [


                // Stack(children: [
                //   Padding(
                //     padding: const EdgeInsets.only(top:60.0),
                //     child: Text("Friends Request"
                //       ,style: TextStyle(
                //         fontSize: 18,
                //         color: Colors.white,
                //         fontWeight: FontWeight.bold,
                //       ),),
                //   ),
                // ],),



                Padding(
                  padding: const EdgeInsets.only(top:20.0),
                  child: Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      child: Image(
                        height: 80,
                        width: 80,
                        image: AssetImage('assets/images/12.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),


                Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    color: Colors.transparent,
                    child:


                    Image(
                      height: 100,
                      // width: 80,
                      image: AssetImage('assets/images/asset6@3x.png'),
                      fit: BoxFit.cover,
                    ),


                  ),
                ),


                Padding(
                  padding: const EdgeInsets.only(bottom:40.0),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      color: Colors.transparent,
                      child:

                      Image(
                        height: 120,
                        // width: 20,
                        image: AssetImage('assets/images/asset9@3x.png'),
                        // fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),




                Padding(
                  padding: const EdgeInsets.only(left:0.0),
                  child: Column(

                    crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.center,

                    children: [


                      Align(
                        alignment: Alignment.topCenter,
                        child:
                Image.asset('assets/images/asset3@3x.png',
                  color: Color(0xFF1dbcba),
                ),
                      ),



                      SizedBox(height: 40,),


                      Padding(
                        padding: const EdgeInsets.only(left:20.0),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Text("Requests",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20
                          ),),
                        ),
                      ),

                      SizedBox(
                        height: 20,
                      ),

                        // widget.checkResponse == false ? _donthave() : _buildPageView(),

                        widget.checkResponse == true ? notYet() :

                      Expanded(
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            width: 400,
                            margin: EdgeInsets.symmetric(horizontal: 5),


                            child: ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: 10, bottom: 40),
                              itemCount: list.length == null ? 0 : list.length,
                              itemBuilder: (BuildContext context, int index) {
                                print("test1234 ${list.length}");
                                // print(list.length);

                                return listViewMethod(context, index);
                              },
                            ),
                          ),
                        ),
                      ),

                      SizedBox(
                        height: 20,
                      ),

                    ],

                  ),
                ),


                Stack(children: [
                  Padding(
                    padding: const EdgeInsets.only(top:60.0),
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Text("Friends Request"
                        ,style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),),
                    ),
                  ),
                ],)



              ],
            ),
          ),
        ),
      );

  }

  Future<void> requestFriends(BuildContext context) async {


    SharedPreferences prefs3 = await SharedPreferences.getInstance();
    id = prefs3.getString('cusId');
    print("id123${id}");

    String _id = id;

    progressShow(context);

    Map params = {
      'Id':_id
    };


    await HttpsRequest()
        .fetchPostMyAllFriendsData(Constant.requestFriends,params)
        .then((response) async {
          progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {

        var convertDataToJson = json.jsonDecode(response.body);
        list = convertDataToJson['requestedfriendto'];

        print("test123 ${list.length}");

        // if(list.length != 0) {
        //
        //   widget.checkResponse = true;
        //
        // }

        if(list.length == 0){

            widget.checkResponse = true;

        }

        setState(() {});


      }else{
        list = new List();
        Fluttertoast.showToast(
            msg: "No Friend in List",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });


  }

   listViewMethod(BuildContext context, int index) {

   // String requestedId = list[index]["friendRequestId"];
   //  print("testreq${requestedId}");

    return   Container(

     child:  Stack(

        children: [

          GestureDetector(
            onTap: (){

              requestAccpect(context, index);


            },
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top:20.0),
                child: Container(
                  width: 60,
                  height: 18,
                  child: Center(
                    child: Text(
                      'Accept',
                      style: TextStyle(color: Colors.black,
                        fontSize: 12,
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xFF1dbcba),

                  ),
                ),
              ),
            ),
          ),



          SizedBox(
            width: 1,
          ),


          GestureDetector(
            onTap: (){
              rejectRequest(context, index);
            },
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(right:62.0,top:20),
                child: Container(
                  width: 60,
                  height: 18,
                  child: Center(
                    child: Text(
                      'Reject',
                      style: TextStyle(color: Colors.white,
                        fontSize: 12,
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xFFD50000),

                  ),
                ),
              ),
            ),
          ),



          Row(
            children: [

              list[index]["image"] == null ?
              Icon(Icons.account_circle_outlined,
                size: 50,
                color: Color(0xFFfdda65),
              ) :
              CircleAvatar(
                radius: 25.0,
                backgroundImage:
                NetworkImage('http://hustledev-001-site8.itempurl.com/'+list[index]["image"]),
                backgroundColor: Colors.transparent,
              ),

              Padding(
                padding: const EdgeInsets.only(left:0.0,top:0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start, // for left side
                  children: [
                    Text(list[index]["userName"].toString(),
                      style: TextStyle(
                      ),
                    ),

                    Text("Lorem ipsum is simple dummy text",
                      style: TextStyle(
                          fontSize: 8
                      ),
                    ),

                    SizedBox(
                      height: 2,
                    ),

                    Text("of the printing and typesetting industry",
                      style: TextStyle(
                          fontSize: 8
                      ),
                    ),

                  ],
                ),
              ),

              SizedBox(
                width: 10,
              ),


            ],
          ),

        ],
      ),


    );


   }

  Future<void> requestAccpect(BuildContext context, int index) async {

    String requestedId = list[index]["friendRequestId"];

    Map params ={
      'FriendRequestId': requestedId,
      'FriendRequestFlagNumber': 2
    };

    progressShow(context);
    await HttpsRequest()
        .fetchPostMyAllFriendsData(Constant.accOrRejRequest,params)
        .then((response) async {
          progressHide(context);
      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {

        requestFriends(context);
        setState(() {});


      }else{
        list = new List();
        Fluttertoast.showToast(
            msg: "No Friend in List",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });

  }

  Future<void> rejectRequest(BuildContext context, int index) async {

    String requestedId = list[index]["friendRequestId"];

    Map params ={
      'FriendRequestId': requestedId,
      'FriendRequestFlagNumber': 3
    };
    progressShow(context);

    await HttpsRequest()
        .fetchPostMyAllFriendsData(Constant.accOrRejRequest,params)
        .then((response) async {

          progressHide(context);
      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {

        requestFriends(context);
        setState(() {});

      }else{
        list = new List();
        Fluttertoast.showToast(
            msg: "No Friend in List",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });


  }

  notYet() {

    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Container(

        child: Column(
          children: [

            SizedBox(height: 60,),
            Image(
              height: 106,
              width: 400,
              image: AssetImage('assets/images/asset12@3x.png'),
              // fit: BoxFit.cover,
            ),
            SizedBox(
              height: 30,
            ),

            Text("You Don't Have Friend Request",
              style: TextStyle(
                  fontSize: 15,
                fontWeight: FontWeight.bold

              ),
            ),
          ],
        ),

      ),
    );


  }
}
