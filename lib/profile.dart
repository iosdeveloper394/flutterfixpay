import 'dart:convert' as json;
import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Redeem.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/signIn.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String username;
  String fullName;
  String id;
  String token;

  TextEditingController usernameController;
  TextEditingController fullNameController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    usernameController = new TextEditingController();
    fullNameController = new TextEditingController();

    Data();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 80),
                  child: Image(
                    height: ResponsiveFlutter.of(context).verticalScale(140),
                    // width: 80,
                    image: AssetImage('assets/images/asset6@3x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Column(
                children: [
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(98),
                      child:
                      Image.asset('assets/images/asset3@3x.png',
                      color: Color(0xFF1dbcba),
                    ),
                      // Image(
                      //   image: AssetImage('assets/images/asset3@3x.png'),
                      // ),
                    ),
                  ),
                  SizedBox(
                    height: ResponsiveFlutter.of(context).verticalScale(100),
                  ),
                  Container(
                    height: ResponsiveFlutter.of(context).verticalScale(160),
                    width: ResponsiveFlutter.of(context).scale(250),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment
                          .center, //Center Column contents vertically,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: ResponsiveFlutter.of(context).verticalScale(35),
                          width: ResponsiveFlutter.of(context).verticalScale(200),
                          child: Center(
                            child: TextField(
                              controller: fullNameController,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration.collapsed(
                                hintStyle: new TextStyle(color: Colors.blueGrey),
                                hintText: "Full Name",
                                // border: InputBorder.none,
                              ),
                              style: TextStyle(
                                color: Colors.blueGrey,
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(2),
                              ),
                            ),
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFFFFFFFF),
                              borderRadius: BorderRadius.circular(20)),
                        ),
                        SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10)),
                        Container(
                          height: ResponsiveFlutter.of(context).verticalScale(35),
                          width: ResponsiveFlutter.of(context).verticalScale(200),
                          child: Center(
                            child: TextField(
                              controller: usernameController,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration.collapsed(
                                hintStyle: new TextStyle(color: Colors.blueGrey),
                                hintText: "User Name",
                                // border: InputBorder.none,
                              ),
                              style: TextStyle(
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                color: Colors.blueGrey,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(2),
                              ),
                            ),
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFFFFFFFF),
                              borderRadius: BorderRadius.circular(20)),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                        color: Color(0xFF1dbcba),
                        borderRadius: BorderRadius.circular(5)),
                  ),
                  SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(160)),
                  GestureDetector(
                    onTap: () async {
                      String _userName = usernameController.text.toString();

                      String _fullName = fullNameController.text.toString();

                      String _id = id;

                      print("_userName, ${_userName}");
                      print("_fullName, ${_fullName}");

                      if (_userName == null || _userName == "") {
                        Fluttertoast.showToast(
                            msg: "Write User Name",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1);
                      } else if (_fullName == null || _fullName == "") {
                        Fluttertoast.showToast(
                            msg: "Write Full Name",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1);
                      } else {
                        progressShow(context);
                        Map params = {
                          'Username': _userName,
                          'FullName': _fullName,
                          'CustomerId': _id,
                        };

                        await HttpsRequest()
                            .fetchPostTokenData(Constant.profile, params)
                            .then((response) async {
                          progressHide(context);
                          print("asasas ${response.statusCode}");

                          if (response.statusCode == 200) {
                            fullNameController.clear();

                            // var convertDataToJson = json.jsonDecode(response.body);

                            Fluttertoast.showToast(
                                msg: "Profile updated successfully",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);

                            SharedPreferences prefs1 =
                                await SharedPreferences.getInstance();
                            prefs1.setString('username', _userName);

                            SharedPreferences prefs2 =
                                await SharedPreferences.getInstance();
                            prefs2.setString('fullName', _fullName);

                            gotoNextScreen(context);
                          } else if (response.statusCode == 500) {
                            Fluttertoast.showToast(
                                msg: "User Name is already taken ",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);
                          }
                        });
                      }
                    },
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(50),
                      width: ResponsiveFlutter.of(context).scale(220),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .center, //Center Column contents vertically,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.save_alt,
                            size: ResponsiveFlutter.of(context).fontSize(4),
                            color: Color(0xFF1dbcba),
                          ),
                          SizedBox(
                            width: ResponsiveFlutter.of(context).scale(70),
                          ),
                          Text(
                            ("Save Changes"),
                            style: TextStyle(
                              fontFamily: 'Gotham',
                              fontWeight: FontWeight.w300,
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(1.5),
                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Profile",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w400,

                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> Data() async {
    SharedPreferences prefs1 = await SharedPreferences.getInstance();
    username = prefs1.getString('username');
    print("username123${username}");

    SharedPreferences prefs2 = await SharedPreferences.getInstance();
    fullName = prefs2.getString('fullName');
    print("fullname123${fullName}");

    SharedPreferences prefs3 = await SharedPreferences.getInstance();
    id = prefs3.getString('cusId');
    print("id123${id}");

    usernameController.text = username;
    fullNameController.text = fullName;

    print("fullControler ${fullNameController.text}");
    print("userControler ${usernameController.text}");

    setState(() {});
  }

  void gotoNextScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }
}
