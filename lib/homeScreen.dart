import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:fixpay/BottomNavigation/HomeNavBar.dart';
import 'package:fixpay/BottomNavigation/ProfileNavBar.dart';
import 'package:fixpay/BottomNavigation/SettingsNavBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // int _currentIndex = 1;

  int _currentIndex = 0;
  final List<Widget> _children = [
    HomeBottomNav(),
    SettingsBottomNav(),
    ProfileBottomNav(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: menu(),
    );

    // DefaultTabController(
    //   length: 3,
    //   child:
    //   Scaffold(
    //     body:_children[_currentIndex],
    //     bottomNavigationBar: menu(),

    //   ),
    // );
  }

  Widget menu() {
    return ConvexAppBar(
      style: TabStyle.fixedCircle,
      height: 50.0,
      cornerRadius: 20.0,
      backgroundColor: Color(0xFFfdda65),
      color: Color.fromRGBO(31, 188, 186, 1.0),
      items: [
        TabItem(
          icon: Image(
            width: 40.0,
            height: 40.0,
            image: AssetImage('assets/images/asset1.png'),
            color: Colors.black87,
          ),
          title: "",
        ),
        TabItem(
          icon: Icon(
            CupertinoIcons.qrcode_viewfinder,
            // Icon(Icons.account_circle_outlined,
            size: ResponsiveFlutter.of(context).fontSize(7),
            color: Colors.white,
          ),
          title: "",
        ),
        TabItem(
          icon: Image(
            width: 30.0,
            height: 30.0,
            image: AssetImage('assets/images/asset3.png'),
            color: Colors.black87,
          ),
          title: "",
        ),
      ],
      initialActiveIndex: _currentIndex,
      onTap: (index) {
        setState(() {
          _currentIndex = index;
        });
      },
    );
  }

  // Widget menu() {
  //   return Container(
  //     height: ResponsiveFlutter.of(context).verticalScale(50),
  //     decoration: BoxDecoration(
  //       color: Colors.white,
  //       borderRadius: BorderRadius.only(
  //           topRight: Radius.circular(0), topLeft: Radius.circular(0)),
  //       boxShadow: [
  //         BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 0),
  //       ],
  //     ),
  //     child: Container(
  //       color: Color(0xFFfdda65),
  //       child: TabBar(
  //         // labelColor: hexToColor(primaryColor),
  //         unselectedLabelColor: Colors.black26,
  //         indicatorSize: TabBarIndicatorSize.label,
  //         indicatorColor: Color(0xFFfdda65),
  //         labelStyle: TextStyle(
  //           fontSize: 11.0,
  //           // color: hexToColor(primaryColor),
  //         ),
  //         unselectedLabelStyle: TextStyle(
  //           fontSize: 11.0,
  //           color: Colors.black26,
  //         ),
  //         tabs: [

  //           Tab(
  //             icon: Image(
  //               width: ResponsiveFlutter.of(context).scale(25),
  //               height: ResponsiveFlutter.of(context).verticalScale(25),
  //               image: AssetImage('assets/images/asset1.png'),
  //               fit: BoxFit.contain,
  //             ),
  //           ),

  //           Tab(
  //             icon:
  // Icon(Icons.qr_code_scanner,
  //   // Icon(Icons.account_circle_outlined,
  //   size: ResponsiveFlutter.of(context).fontSize(4),
  //   color: Colors.black,
  // )

  //           ),

  //           Tab(
  //             icon:
  //             Image(
  //               width: ResponsiveFlutter.of(context).scale(25),
  //               height: ResponsiveFlutter.of(context).verticalScale(25),
  //               image: AssetImage('assets/images/asset3.png'),
  //               fit: BoxFit.contain,
  //             ),
  //           ),

  //         ],
  // onTap: (index) {
  //   setState(() {
  //     _currentIndex = index;
  //   });
  // },
  //       ),
  //     ),
  //   );
  // }

}
