import 'dart:io';

import 'package:fixpay/BottomNavigation/HomeNavBar.dart';
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Wallet.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/homeScreen.dart';
import 'package:fixpay/loyalityPoints.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RedeemPinCode extends StatefulWidget {
  @override
  _RedeemState createState() => _RedeemState();
}

class _RedeemState extends State<RedeemPinCode> {
  final pinCodeController = TextEditingController();

  String merchantId;
  String profileImage;
  String nameBrandRedeem = "abc";
  String dateRedeem;
  String pointsSend = "Burger";
  int pointSendInt;

  String pinCodeSend;
  int pinCodeSendInt;

  @override
  void initState() {
    // TODO: implement initState
    apiMethod(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: ResponsiveFlutter.of(context).verticalScale(98),
                  child: Image.asset(
                    'assets/images/asset3@3x.png',
                    color: Color(0xFF1dbcba),
                  ),
                ),
              ),

              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                  child: Image.asset(
                    'assets/images/asset6@2x.png',
                    height: ResponsiveFlutter.of(context).verticalScale(100),
                    color: Color(0xFFfdda65),
                  ),

                  // Image(
                  //   height: ResponsiveFlutter.of(context).verticalScale(100),
                  //   // width: 80,
                  //   image: AssetImage('assets/images/asset6@2x.png'),
                  //   fit: BoxFit.cover,
                  // ),
                ),
              ),

              // Padding(
              //   padding: const EdgeInsets.only(top:20.0),
              //   child: Align(
              //     alignment: Alignment.topRight,
              //     child: Container(
              //       child: Image(
              //         height: 80,
              //         width: 80,
              //         image: AssetImage('assets/images/12.png'),
              //         fit: BoxFit.cover,
              //       ),
              //     ),
              //   ),
              // ),

              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(120),
                    ),
                    Container(
                      width: ResponsiveFlutter.of(context).scale(140),
                      height: ResponsiveFlutter.of(context).verticalScale(140),
                      child:
                          // profileImage == null  ?
                          // Icon(Icons.account_circle_outlined,
                          //   size: ResponsiveFlutter.of(context).fontSize(16),
                          //   color: Color(0xFF1dbcba),
                          // ):
                          CircleAvatar(
                        radius: 25.0,
                        backgroundImage: NetworkImage(
                            'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                        backgroundColor: Colors.transparent,
                      ),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.orange),
                          color: Colors.white,
                          shape: BoxShape.circle),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(12),
                    ),
                    Text(
                      "abc".toString(),
                      style: TextStyle(

                          color: Colors.blueGrey.shade800,
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w400,
                          fontSize: ResponsiveFlutter.of(context).fontSize(3),
                          // fontWeight: FontWeight.bold
                      ),
                    ),
                    Text(
                      "24 04 2021".toString(),
                      style: TextStyle(

                        color: Colors.blueGrey.shade800,
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                    ),
                    Text(
                      "Your Pin Code",
                      style: TextStyle(

                        color: Colors.blueGrey.shade800,
                        fontWeight: FontWeight.w400,
                          fontFamily: 'Gotham',
                          fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                          // fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(30),
                    ),
                    Container(
                      width: ResponsiveFlutter.of(context).verticalScale(160),
                      // margin: EdgeInsets.symmetric(horizontal: 80),

                      child: PinCodeTextField(
                        keyboardType: TextInputType.number,
                        controller: pinCodeController,
                        backgroundColor: Color(0xFFEBFFFE),
                        length: 4,
                        obscureText: false,
                        animationType: AnimationType.fade,
                        pinTheme: PinTheme(
                          shape: PinCodeFieldShape.box,
                          inactiveFillColor: Color(0xFFfdda65),
                          inactiveColor: Color(0xFFfdda65),
                          activeFillColor: Color(0xFFfdda65),
                          selectedColor: Color(0xFFfdda65),

                          selectedFillColor: Color(0xFFfdda65),

                          activeColor: Color(0xFFfdda65),
                          // selectedColor: Colors.blueGrey,
                          // borderRadius: BorderRadius.circular(0),
                          fieldHeight:
                              ResponsiveFlutter.of(context).verticalScale(30),
                          fieldWidth: ResponsiveFlutter.of(context).scale(30),
                          // activeFillColor: Colors.black,
                        ),
                        animationDuration: Duration(milliseconds: 300),
                        cursorColor: Color(0xFFfdda65),
                        enableActiveFill: true,
                        // errorAnimationController: errorController,
                        // controller: textEditingController,
                        onCompleted: (v) {
                          print("Completed");
                        },
                        // onChanged: (value) {
                        //   print(value);
                        //   setState(() {
                        //     var currentText = value;
                        //   });
                        // },
                        beforeTextPaste: (text) {
                          print("Allowing to paste $text");
                          //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                          //but you can show anything you want here, like your pop up saying wrong paste format or etc
                          return true;
                        },
                      ),
                    ),
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(20),
                    ),
                    GestureDetector(
                      onTap: () async {
                        print("ye hai code ${pinCodeController.text.toString()}");

                        if (pinCodeController.text.toString() == null ||
                            pinCodeController.text.toString() == "" ||
                            pinCodeController.text.toString().length < 4) {
                          Fluttertoast.showToast(
                              msg: "Add Pin Code",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        } else {
                          showDialog(
                              context: context,
                              builder: (BuildContext contextdialog) {
                                return AlertDialog(
                                  scrollable: true,
                                  title: Text('Confirm', style: TextStyle(
                                    color: Colors.blueGrey.shade800,
                                    fontFamily: 'Gotham',
                                    fontWeight: FontWeight.w400,

                                  ),),
                                  content: Form(
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          "Are you sure you want to Redeem ?",
                                          // Text("Are you sure you want to Redeem"" " +pointsSend+" point/s ?",

                                          style: TextStyle(
                                            color: Colors.blueGrey.shade800,
                                            fontFamily: 'Gotham',
                                            fontWeight: FontWeight.w300,

                                          ),
                                        ),
                                        SizedBox(
                                          height: 30,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              height: 30,
                                              width: 80,
                                              child: RaisedButton(
                                                color: Color(0xFFfdda65),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15)),
                                                onPressed: () async {
                                                  // Navigator.pushReplacement(context, MaterialPageRoute(builder:
                                                  //     (BuildContext context) => HomeScreen()));
                                                  Navigator.of(context).pop();
                                                },
                                                child: Text(
                                                  "Cancel",
                                                  style: TextStyle(
                                                      fontFamily: 'Gotham',
                                                      fontWeight: FontWeight.w300,
                                                      fontSize: 12,
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 20,
                                            ),
                                            Container(
                                              height: 30,
                                              width: 80,
                                              child: RaisedButton(
                                                color: Color(0xFF1dbcba),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15)),
                                                onPressed: () async {
                                                  // pointSendInt = int.parse(pointsSend);
                                                  // assert(pointSendInt is int);
                                                  // print("ye hai myInt${pointSendInt}");
                                                  //
                                                  //
                                                  // print("ye hai myInt${pinCodeController.text.toString()}");
                                                  //
                                                  // var pinFinal = pinCodeController.text.toString();
                                                  // pinCodeSendInt = int.parse(pinFinal);
                                                  // assert(pinCodeSendInt is int);
                                                  // print("ye hai myInt${pinCodeSendInt}");
                                                  //
                                                  //
                                                  //
                                                  // Map params = {
                                                  //   'MerchantId': merchantId,
                                                  //   'ShareRewardPoints': pointSendInt,
                                                  //   'PinCode': pinCodeSendInt
                                                  // };
                                                  //
                                                  // progressShow(context);
                                                  // await HttpsRequest()
                                                  //     .fetchPostMyAllFriendsData(
                                                  //     Constant.redeemPointSend, params)
                                                  //     .then((response) async {
                                                  //   progressHide(context);
                                                  //
                                                  //   print("asasas ${response.statusCode}");
                                                  //   if (response.statusCode == 200) {
                                                  //
                                                  //
                                                  //     // pointSendInt = null;
                                                  //
                                                  //     print("ye hai code 200 ${pointSendInt}");
                                                  //
                                                  //
                                                  //     // Navigator.of(context).popUntil(ModalRoute.withName('/wallet'));
                                                  //
                                                  //     // Navigator.pushNamedAndRemoveUntil(context, "/wallet", (Route<dynamic> route) => false);
                                                  //
                                                  //     // Navigator.pushNamed(context, '/second');
                                                  //
                                                  //     // Navigator.of(context).popUntil((route) => route.isFirst);
                                                  //

                                                  SharedPreferences prefs10 =
                                                      await SharedPreferences
                                                          .getInstance();
                                                  prefs10.remove('pointSendInt');

                                                  // Navigator.pushReplacement(context, MaterialPageRoute(builder:
                                                  // (BuildContext context) => HomeScreen()));

                                                  Navigator.pushAndRemoveUntil(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              HomeScreen()),
                                                      ModalRoute.withName(
                                                          "/Home"));

                                                  //
                                                  //
                                                  //     Navigator.pushAndRemoveUntil(context,
                                                  //         MaterialPageRoute(builder: (BuildContext context) => Wallet()),
                                                  //             (Route<dynamic> route) => route is HomeBottomNav(),
                                                  //     );
                                                  //
                                                  //
                                                  Fluttertoast.showToast(
                                                      msg:
                                                          "Points Successfully Redeemed",
                                                      toastLength:
                                                          Toast.LENGTH_SHORT,
                                                      gravity:
                                                          ToastGravity.CENTER,
                                                      timeInSecForIosWeb: 1);

                                                  //
                                                  //
                                                  //
                                                  //   } else if (response.statusCode == 803) {
                                                  //     Fluttertoast.showToast(
                                                  //         msg: "Set Your Correct Pin Code",
                                                  //         toastLength: Toast.LENGTH_SHORT,
                                                  //         gravity: ToastGravity.CENTER,
                                                  //         timeInSecForIosWeb: 1);
                                                  //
                                                  //   }else{
                                                  //
                                                  //     Fluttertoast.showToast(
                                                  //         msg: "No Internet",
                                                  //         toastLength: Toast.LENGTH_SHORT,
                                                  //         gravity: ToastGravity.CENTER,
                                                  //         timeInSecForIosWeb: 1);
                                                  //
                                                  //
                                                  //   }
                                                  //
                                                  //
                                                  // });
                                                },
                                                child: Text(
                                                  "Ok",
                                                  style: TextStyle(
                                                      fontFamily: 'Gotham',
                                                      fontWeight: FontWeight.w300,
                                                      fontSize: 12,
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              });
                        }
                      },
                      child: Container(
                        width: ResponsiveFlutter.of(context).scale(160),
                        child: Center(
                          child: Text(
                            "Send",
                            style: TextStyle(
                                fontFamily: 'Gotham',
                                color: Color(0xFFfdda65),
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xFF1dbcba),
                        ),
                        height: ResponsiveFlutter.of(context).verticalScale(45),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    SharedPreferences prefs5 = await SharedPreferences.getInstance();
    profileImage = prefs5.getString('imgaeProfileRedeem');

    print("profileRedeem${profileImage}");

    SharedPreferences prefs6 = await SharedPreferences.getInstance();
    nameBrandRedeem = prefs6.getString('nameBrandRedeem');

    print("nameBrandRedeem${nameBrandRedeem}");

    SharedPreferences prefs7 = await SharedPreferences.getInstance();
    dateRedeem = prefs7.getString('dateRedeem');

    print("dateRedeem${dateRedeem}");

    SharedPreferences prefs8 = await SharedPreferences.getInstance();
    pointsSend = prefs8.getString('pointSendInt');

    print("dateRedeem${pointsSend}");

    setState(() {});
  }
}
