import 'dart:convert' as json;
import 'package:fixpay/BottomNavigation/SettingsNavBar.dart';
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/myFriends.dart';
import 'package:fixpay/shareWithFriend.dart';
import 'package:fixpay/Redeem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoyalityPoints extends StatefulWidget {
  final bool isFromQr;
  final bool isFromItem;
  // final bool isFromPoint;

  const LoyalityPoints({
    Key key,
    this.isFromQr,
    this.isFromItem,
  }) : super(key: key);
  @override
  _LoyalityPointsState createState() => _LoyalityPointsState();
}

class _LoyalityPointsState extends State<LoyalityPoints> {
  List list = new List();
  int totalPoints = 533;
  String imgaeProfile =
      "http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg";
  String date = "12 3 2021";
  String nameBrand = "abc";

  @override
  void initState() {
    // TODO: implement initState
    // apiMethod(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: ResponsiveFlutter.of(context).verticalScale(98),

                  child: Image.asset(
                    'assets/images/asset3@3x.png',
                    color: Color(0xFF1dbcba),
                  ),

                  // Image(
                  //   image: AssetImage('assets/images/asset3@3x.png'),
                  //   // fit: BoxFit.cover,
                  // ),
                ),
              ),
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      bottom: ResponsiveFlutter.of(context).moderateScale(40)),
                  child: Image(
                    height: ResponsiveFlutter.of(context).verticalScale(100),
                    // width: 80,
                    image: AssetImage('assets/images/asset6@2x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: ResponsiveFlutter.of(context).moderateScale(25)),
                  child: Text(
                    "Loyalty Points",
                    style: TextStyle(
                      fontFamily: 'Gotham',
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.2),
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [
                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                    ),

                    // Container(
                    //   height: 100,
                    //   width: 100,
                    //   child:  Image(
                    //     height: 100,
                    //     width: 100,
                    //     image: AssetImage('assets/images/burger.png'),
                    //     fit: BoxFit.cover,
                    //   ),
                    //   decoration: BoxDecoration(
                    //       border: Border.all(color: Colors.orange),
                    //       color: Colors.white,
                    //       shape: BoxShape.circle
                    //   ),
                    // ),

                    Container(
                      width: ResponsiveFlutter.of(context).scale(140),
                      height: ResponsiveFlutter.of(context).verticalScale(140),
                      child: imgaeProfile == null
                          ? Icon(
                              Icons.account_circle_outlined,
                              size: ResponsiveFlutter.of(context).fontSize(16),
                              color: Color(0xFF1dbcba),
                            )
                          : CircleAvatar(
                              radius: 25.0,
                              backgroundImage: NetworkImage(imgaeProfile),
                              backgroundColor: Colors.transparent,
                            ),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.orange),
                          color: Colors.white,
                          shape: BoxShape.circle),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(12),
                    ),

                    Text(
                      nameBrand.toString(),
                      style: TextStyle(
                        color: Colors.blueGrey.shade800,
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w400,
                        fontSize: ResponsiveFlutter.of(context).fontSize(3),
                        // fontWeight: FontWeight.bold
                      ),
                    ),

                    Text(
                      date.toString(),
                      style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(1.5),
                        // fontWeight: FontWeight.bold
                      ),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(10),
                    ),

                    Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: 5.0,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        // color: Color(0xFFEBFFFE),
                        // borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 1.0,
                            color: Colors.black26,
                            offset: Offset(0.0, 1.0),
                            spreadRadius: 1.0,
                          )
                        ],
                      ),
                      width: ResponsiveFlutter.of(context).scale(200),
                      child: Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: ResponsiveFlutter.of(context)
                                    .moderateScale(12)),
                            child: Text(
                              "Total Points",
                              style: TextStyle(
                                color: Colors.blueGrey.shade800,
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w400,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(2.5),
                                // fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: ResponsiveFlutter.of(context)
                                    .moderateScale(40)),
                            child: Text(
                              totalPoints.toString(),
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  color: Color(0xFF1dbcba),
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(2.5),
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                      // decoration: BoxDecoration(
                      //   borderRadius: BorderRadius.circular(10),
                      //   color: Colors.white,
                      // ),
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                    ),
                    SizedBox(
                      height: 10,
                    ),

                    Container(
                      width: ResponsiveFlutter.of(context).scale(260),
                      height: ResponsiveFlutter.of(context).verticalScale(300),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                top: ResponsiveFlutter.of(context)
                                    .moderateScale(10)),
                            child: Text(
                              "Recent Points",
                              style: TextStyle(
                                  fontFamily: 'Gotham',
                                  color: Colors.blueGrey.shade800,
                                  // fontFamily: 'Gotham',
                                  // fontWeight: FontWeight.w300,
                                  fontWeight: FontWeight.w400,
                                  // fontSize: 18,
                                  fontSize: ResponsiveFlutter.of(context)
                                      .fontSize(2.5)),
                            ),
                          ),
                          SizedBox(
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(5)),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.all(
                                  ResponsiveFlutter.of(context)
                                      .moderateScale(8)),
                              child: ListView.builder(
                                shrinkWrap: true,
                                padding: EdgeInsets.only(
                                    top: ResponsiveFlutter.of(context)
                                        .moderateScale(10)),
                                // itemCount: list.length == null ? 0 : list.length,
                                itemCount: list.length = 10,
                                itemBuilder: (BuildContext context, int index) {
                                  // print("test1234 ${list}");
                                  // print(list.length);

                                  return listViewMethod(context, index);
                                },
                              ),
                            ),
                          ),
                          SizedBox(
                            height:
                                ResponsiveFlutter.of(context).verticalScale(10),
                          ),
                          GestureDetector(
                            onTap: () {
                              widget.isFromQr == true
                                  ? Navigator.pushReplacement(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => Redeem()),
                                    )
                                  : showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          scrollable: true,
                                          content: Padding(
                                            padding: EdgeInsets.all(
                                                ResponsiveFlutter.of(context)
                                                    .moderateScale(8)),
                                            child: Form(
                                              child: Column(
                                                children: <Widget>[
                                                  Text(
                                                    "Redeem your Points",
                                                    style: TextStyle(
                                                      fontFamily: 'Gotham',
                                                      fontSize:
                                                          ResponsiveFlutter.of(
                                                                  context)
                                                              .fontSize(2.8),

                                                      color: Colors
                                                          .blueGrey.shade800,
                                                      // fontFamily: 'Gotham',
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .verticalScale(3),
                                                  ),
                                                  SizedBox(
                                                      height: ResponsiveFlutter
                                                              .of(context)
                                                          .verticalScale(30)),
                                                  GestureDetector(
                                                    onTap: () {
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder:
                                                                (context) =>
                                                                    Redeem()),
                                                      );
                                                    },
                                                    child: Container(
                                                      width:
                                                          ResponsiveFlutter.of(
                                                                  context)
                                                              .scale(200),
                                                      height: ResponsiveFlutter
                                                              .of(context)
                                                          .verticalScale(50),
                                                      child: Center(
                                                        child: Text(
                                                          "Redeem for Yourself",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              color: Color(
                                                                  0xFFfdda65),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300),
                                                        ),
                                                      ),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color:
                                                            Color(0xFF1dbcba),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .verticalScale(10),
                                                  ),
                                                  Text(
                                                    "OR",
                                                    style: TextStyle(
                                                        fontFamily: 'Gotham',
                                                        fontWeight:
                                                            FontWeight.w300,
                                                        fontSize:
                                                            ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(2)),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .verticalScale(10),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () {
                                                      // Navigator.pushReplacement(
                                                      //   context,
                                                      //   MaterialPageRoute(builder: (context) => ShareWithFriend(isFromPoint: true)),
                                                      // );

                                                      showDialog(
                                                          context: context,
                                                          builder: (BuildContext
                                                              context) {
                                                            return AlertDialog(
                                                              scrollable: true,
                                                              content: Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .all(
                                                                        8.0),
                                                                child: Form(
                                                                  child: Align(
                                                                    alignment:
                                                                        Alignment
                                                                            .center,
                                                                    child:
                                                                        Column(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .center,
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .center,
                                                                      children: [
                                                                        Text(
                                                                          "Share your points",
                                                                          style:
                                                                              TextStyle(
                                                                            fontFamily:
                                                                                'Gotham',
                                                                            fontSize:
                                                                                ResponsiveFlutter.of(context).fontSize(2.8),

                                                                            color:
                                                                                Colors.blueGrey.shade800,
                                                                            // fontFamily: 'Gotham',
                                                                            fontWeight:
                                                                                FontWeight.w400,
                                                                          ),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              ResponsiveFlutter.of(context).verticalScale(3),
                                                                        ),
                                                                        Text(
                                                                          "Share Via",
                                                                          style: TextStyle(
                                                                              fontFamily: 'Gotham',
                                                                              fontWeight: FontWeight.w300,
                                                                              fontSize: 12),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              ResponsiveFlutter.of(context).verticalScale(15),
                                                                        ),
                                                                        GestureDetector(
                                                                          onTap:
                                                                              () {
                                                                            widget.isFromItem != true
                                                                                ? Navigator.pushReplacement(
                                                                                    context,
                                                                                    MaterialPageRoute(builder: (context) => SettingsBottomNav(isFromMerchant: true)),
                                                                                  )
                                                                                : Navigator.pushReplacement(
                                                                                    context,
                                                                                    MaterialPageRoute(builder: (context) => SettingsBottomNav(isFromItem: true)),
                                                                                  );
                                                                          },
                                                                          child:
                                                                              Container(
                                                                            width:
                                                                                ResponsiveFlutter.of(context).scale(200),
                                                                            height:
                                                                                ResponsiveFlutter.of(context).verticalScale(50),
                                                                            child:
                                                                                Center(
                                                                              child: Text(
                                                                                "Profile QR",
                                                                                style: TextStyle(fontFamily: 'Gotham', color: Color(0xFFfdda65), fontWeight: FontWeight.w300),
                                                                              ),
                                                                            ),
                                                                            decoration:
                                                                                BoxDecoration(
                                                                              borderRadius: BorderRadius.circular(5),
                                                                              color: Color(0xFF1dbcba),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              ResponsiveFlutter.of(context).verticalScale(10),
                                                                        ),
                                                                        Text(
                                                                          "OR",
                                                                          style: TextStyle(
                                                                              fontFamily: 'Gotham',
                                                                              fontWeight: FontWeight.w300,
                                                                              fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              ResponsiveFlutter.of(context).verticalScale(10),
                                                                        ),
                                                                        GestureDetector(
                                                                          onTap:
                                                                              () {
                                                                            Navigator.pushReplacement(
                                                                              context,
                                                                              MaterialPageRoute(builder: (context) => MyFriends(isFromPoint: true, isFromItem: widget.isFromItem, isFromShareFriend: true)),
                                                                            );
                                                                          },
                                                                          child:
                                                                              Container(
                                                                            width:
                                                                                ResponsiveFlutter.of(context).scale(200),
                                                                            height:
                                                                                ResponsiveFlutter.of(context).verticalScale(50),
                                                                            child:
                                                                                Center(
                                                                              child: Text(
                                                                                "Select Your Friend",
                                                                                style: TextStyle(fontFamily: 'Gotham', color: Color(0xFF1dbcba), fontWeight: FontWeight.w300),
                                                                              ),
                                                                            ),
                                                                            decoration:
                                                                                BoxDecoration(
                                                                              borderRadius: BorderRadius.circular(5),
                                                                              color: Color(0xFFfdda65),
                                                                            ),
                                                                          ),
                                                                        ),
                                                                        SizedBox(
                                                                          height:
                                                                              ResponsiveFlutter.of(context).verticalScale(15),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            );
                                                          });
                                                    },
                                                    child: Container(
                                                      width:
                                                          ResponsiveFlutter.of(
                                                                  context)
                                                              .scale(200),
                                                      height: ResponsiveFlutter
                                                              .of(context)
                                                          .verticalScale(50),
                                                      child: Center(
                                                        child: Text(
                                                          "Share with a Friend?",
                                                          style: TextStyle(
                                                              fontFamily:
                                                                  'Gotham',
                                                              color: Color(
                                                                  0xFF1dbcba),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300),
                                                        ),
                                                      ),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                        color:
                                                            Color(0xFFfdda65),
                                                      ),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height:
                                                        ResponsiveFlutter.of(
                                                                context)
                                                            .verticalScale(15),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      });
                            },
                            child: Container(
                              // width: 90,
                              // height: 25,
                              width: ResponsiveFlutter.of(context).scale(105),
                              height: ResponsiveFlutter.of(context)
                                  .verticalScale(28),
                              child: Center(
                                child: Text(
                                  'Reedem',
                                  style: TextStyle(
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      color: Colors.white,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(2.5)),
                                ),
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(3),
                                color: Color(0xFF1dbcba),
                              ),
                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var merchantId = prefs4.getString('merchantId');

    print("merchantId${merchantId}");

    String _merchantId = merchantId.toString();

    var params = {'MerchantId': _merchantId};

    progressShow(context);

    await HttpsRequest()
        .fetchPostMerchantsForCustomer(Constant.loyaltyPoints, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        list = convertDataToJson['merchatto']['cashbackRecentList'];

        print("ye hai list loy${list}");

        totalPoints =
            convertDataToJson['merchatto']['totalBalance']['totalBalance'];
        date = convertDataToJson['merchatto']['merchant']['createDate'];
        nameBrand = convertDataToJson['merchatto']['merchant']['brandName'];
        imgaeProfile = convertDataToJson['merchatto']['merchant']['image'];

        String myDate = date;
        DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm").parse(myDate);
        date = "${tempDate.day} ${tempDate.month} ${tempDate.year}";
        print("ye hai date convert");
        print(date);

        SharedPreferences prefs13 = await SharedPreferences.getInstance();
        prefs13.setString('imgaeProfileRedeem', imgaeProfile);

        SharedPreferences prefs12 = await SharedPreferences.getInstance();
        prefs12.setString('nameBrandRedeem', nameBrand);

        SharedPreferences prefs14 = await SharedPreferences.getInstance();
        prefs14.setString('dateRedeem', date);

        setState(() {});
      } else {
        // list = new List();
        Fluttertoast.showToast(
            msg: "issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }

  Widget listViewMethod(BuildContext context, int index) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(
              top: ResponsiveFlutter.of(context).moderateScale(10)),
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5.0,
            ),
            decoration: BoxDecoration(
              color: Color(0xFFEBFFFE),
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 1.0,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(50),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Asad".toString(),
                  style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      color: Colors.blueGrey.shade800,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                ),
                SizedBox(
                  width: ResponsiveFlutter.of(context).scale(95),
                ),
                Container(
                  width: ResponsiveFlutter.of(context).scale(70),
                  height: ResponsiveFlutter.of(context).verticalScale(25),
                  child: Center(
                    child: Text(
                      "100".toString(),
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          color: Colors.white,
                          fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xFF1dbcba),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              top: ResponsiveFlutter.of(context).moderateScale(10)),
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Color(0xFFEBFFFE),
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 1.0,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(60),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Aysha".toString(),
                  style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      color: Colors.blueGrey.shade800,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                ),
                SizedBox(
                  width: ResponsiveFlutter.of(context).scale(85),
                ),
                Container(
                  width: ResponsiveFlutter.of(context).scale(70),
                  height: ResponsiveFlutter.of(context).verticalScale(25),
                  child: Center(
                    child: Text(
                      "230".toString(),
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          color: Colors.white,
                          fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xFF1dbcba),
                  ),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              top: ResponsiveFlutter.of(context).moderateScale(10)),
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Color(0xFFEBFFFE),
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 1.0,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(60),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Umair".toString(),
                  style: TextStyle(
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                      color: Colors.blueGrey.shade800,
                      fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                ),
                SizedBox(
                  width: ResponsiveFlutter.of(context).scale(90),
                ),
                Container(
                  width: ResponsiveFlutter.of(context).scale(70),
                  height: ResponsiveFlutter.of(context).verticalScale(25),
                  child: Center(
                    child: Text(
                      "156".toString(),
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          color: Colors.white,
                          fontSize: ResponsiveFlutter.of(context).fontSize(2)),
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(0xFF1dbcba),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
