

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';


class HttpsRequest {
  // Future<http.Response> fetchPost(route, params) async {
  //   try {
  //     var uri = Uri.https('hustledev-001-site8.itempurl.com', route, params);
  //
  //
  //     print(uri);
  //     http.Response response = await http.post(uri).timeout(const Duration(seconds: 120));
  //
  //     return response;
  //   }
  //   catch (e) {
  //     print(e);
  //     throw('Internet Failed');
  //   }
  // }


  Future<http.Response> fetchPostData(route,jsonMap) async {


    var url = Uri.http('hustledev-001-site8.itempurl.com', route);

    print("rouitis ${url}");

    // var url ='http://hustledev-001-site8.itempurl.com/api/Customer/register';
    //encode Map to JSON

    var body = json.encode(jsonMap);
    var response = await http.post(url,
        headers: {"Content-Type": "application/json"},
        body: body

    );
    print("asasas");

    print("${response.statusCode}");
    print("${response.body}");
    return response;

  }


  Future<http.Response> fetchPostTokenData(route,jsonMap) async {

    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var token;
    token = prefs4.getString('token');
    print("token123${token}");

    var url = Uri.http('hustledev-001-site8.itempurl.com', route);

    print("rouitis ${url}");

    // var url ='http://hustledev-001-site8.itempurl.com/api/Customer/register';
    //encode Map to JSON

    var body = json.encode(jsonMap);
    var response = await http.post(url,
        headers: {"Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        },
        body: body

    );
    print("asasas");

    print("${response.statusCode}");
    print("${response.body}");
    return response;

  }


  Future<http.Response> fetchPostForgetPass(route,jsonMap) async {

    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var token;
    token = prefs4.getString('token');
    print("token123${token}");

    var url = Uri.http('hustledev-001-site8.itempurl.com', route);

    print("rouitis ${url}");

    // var url ='http://hustledev-001-site8.itempurl.com/api/Customer/register';
    //encode Map to JSON

    var body = json.encode(jsonMap);
    var response = await http.post(url,
        headers: {"Content-Type": "application/json",
          // 'Authorization': 'Bearer $token',
        },
        body: body

    );
    print("asasas");

    print("${response.statusCode}");
    print("${response.body}");
    return response;

  }


  Future<http.Response> fetchPostAllFriendsData(route) async {

    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var token;
    token = prefs4.getString('token');
    print("token123${token}");

    var url = Uri.http('hustledev-001-site8.itempurl.com', route);

    var response = await http.get(url,
        headers: {"Content-Type": "application/json",
          'Authorization': 'Bearer $token',
        },
    );
    print("asasas");
    print("${response.statusCode}");
    print("${response.body}");
    return response;

  }


  Future<http.Response> fetchPostMerchantsForCustomer(route,params) async {

    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var token;
    token = prefs4.getString('token');
    print("token123${token}");

    var url = Uri.http('hustledev-001-site8.itempurl.com', route,params);

    print("ye hai api url${url}");

    var response = await http.get(url,
      headers: {'Authorization': 'Bearer $token',
      },
    );
    print("asasas");
    print("${response.statusCode}");
    print("${response.body}");
    return response;

  }



  Future<http.Response> fetchPostMyAllFriendsData(route,jsonMap) async {
    SharedPreferences prefs4 = await SharedPreferences.getInstance();
    var token;
    token = prefs4.getString('token');
    print("token123${token}");

    var url = Uri.http('hustledev-001-site8.itempurl.com', route);

    var body = json.encode(jsonMap);

    var response = await http.post(url,
      headers: {"Content-Type": "application/json",
        'Authorization': 'Bearer $token'},

      body: body

    );
    print("asasas");
    print("${response.statusCode}");
    print("${response.body}");
    return response;

  }






}