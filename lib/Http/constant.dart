
import 'package:flutter/material.dart';

const primaryColor = "#f48333";
const secondaryColor = "#faa932";

Color hexToColor(String code) {
  return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}

class Constant{
  static String url = 'https://hustledev-001-site8.itempurl.com';
  static String KEY = 'starlet';
  static String route = '/api/';
  static String profile = route + 'Customer/changeProfile';
  static String newPass = route + 'Customer/sendNewPassword';
  static String changePass = route + 'Customer/changePassword';
  static String changeEmail = route + 'Customer/changeEmail';
  static String changePhoneNum = route + 'Customer/changePhoneNumber';
  static String friendsAll = route + 'FriendRequest/getSearchFriendList';
  static String myWallet = route + 'CustomerCashback/getCustomerCashbackPointItemAndMerchants';
  static String myItemsPoints = route + 'CustomerCashback/getCustomerCashbackPtsItemAndMerchantsForCustomer';
  static String loyaltyPoints = route + 'CustomerCashback/getCustomerCashbackPtsAndMerchantsListForCustomer';
  static String totalItems = route + 'CustomerCashback/getCustomerCashbackItemAndMerchantsListForCustomer';
  static String sendFriendsRequest = route + 'FriendRequest/friendRequestSend';
  static String myFriends = route + 'FriendRequest/GetAllFriendListById';
  static String accOrRejRequest = route + 'FriendRequest/FriendRequestFlagChange';
  static String requestFriends = route + 'FriendRequest/getAllFriendRequestedById';
  static String forgetPass = route + 'Customer/auth/customerForgotPassword';
  static String verifiCodeReg = route + 'Customer/sendVerificationCodeReg';
  static String verifiCode = route + 'Customer/sendVerificationCode';
  static String redeemPointSend = route + 'CashbackTransactions/cashbackTransactionWalletToBank';
  static String PinCodeSend = route + 'Customer/register';
  static String orders = 'orders/';
  static String login = route+'Customer/authenticate';
  static String contact = route+'contact';
  static String complaint = route+'complaint';
  static String signup = route+'Customer/reg-email-confirmation';
  static String updarteprofile = route+profile+'update';
  static String orderdetails = route+orders+'delivered';
  static String nonorderdetails = route+orders+'non-delivered';
  static String orderrecent = route+orders+'recent';


}