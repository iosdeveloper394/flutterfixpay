import 'dart:convert' as json;
import 'dart:io';
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/screenSeven.dart';
import 'package:fixpay/signUp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image/network.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Wallet extends StatefulWidget {
  bool checkResponse = false;

  @override
  _WalletState createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  List list = new List();
  int points = 234;
  int items = 432;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // apiMethod(context);
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      resizeToAvoidBottomInset: true,
      body: Container(
        height: screenSize.height,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(
                    bottom: ResponsiveFlutter.of(context).moderateScale(120)),
                child: Image.asset(
                  'assets/images/asset6@2x.png',
                  height: ResponsiveFlutter.of(context).verticalScale(100),
                  color: Color(0xFFfdda65),
                ),

                // Image(
                //   height: ResponsiveFlutter.of(context).verticalScale(100),
                //   // width: 80,
                //   image: AssetImage('assets/images/asset6@2x.png'),
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    height: ResponsiveFlutter.of(context).verticalScale(98),
                    //
                    // child: Image(
                    //   image: AssetImage('assets/images/asset3@3x.png'),
                    // ),

                    child: Image.asset(
                      'assets/images/asset3@3x.png',
                      color: Color(0xFF1dbcba),
                    ),
                  ),
                ),
                SizedBox(
                  height: ResponsiveFlutter.of(context).verticalScale(20),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: ResponsiveFlutter.of(context).scale(40),
                    ),
                    Container(
                      width: ResponsiveFlutter.of(context).scale(135),
                      height: ResponsiveFlutter.of(context).verticalScale(32),
                      child: Padding(
                        padding: EdgeInsets.only(
                            top: ResponsiveFlutter.of(context).moderateScale(7),
                            left:
                                ResponsiveFlutter.of(context).moderateScale(5)),
                        child: Text(
                          "What i have",

                          style: TextStyle(
                            fontSize: ResponsiveFlutter.of(context).fontSize(2),
                            fontFamily: 'Gotham',
                            // color: Color(0XFF000000),
                            color: Colors.blueGrey.shade800,
                            fontWeight: FontWeight.w500,

                            // fontWeight: FontWeight.w300,
                          ),

                          // style: TextStyle(
                          //     fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                          //   fontWeight: FontWeight.bold
                          // ),
                        ),
                      ),
                      decoration: BoxDecoration(
                        color: Color(0xFFfdda65),
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: ResponsiveFlutter.of(context).verticalScale(15),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: ResponsiveFlutter.of(context).scale(135),
                      height: ResponsiveFlutter.of(context).verticalScale(110),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            points.toString(),
                            style: TextStyle(
                              color: Color(0xFF1dbcba),
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(4),

                              fontFamily: 'Gotham',

                              // color: Color(0XFF5d5d5d),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            "Points",
                            style: TextStyle(
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(3.5),
                              fontFamily: 'Gotham',
                              color: Colors.blueGrey.shade800,
                              fontWeight: FontWeight.w400,
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(
                      width: ResponsiveFlutter.of(context).scale(10),
                    ),
                    Container(
                      width: ResponsiveFlutter.of(context).scale(135),
                      height: ResponsiveFlutter.of(context).verticalScale(110),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            items.toString(),
                            style: TextStyle(
                              color: Color(0xFF1dbcba),
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(4),

                              fontFamily: 'Gotham',

                              // color: Color(0XFF5d5d5d),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            "Items",
                            style: TextStyle(
                              // color: Color(0xFF1dbcba),
                              fontSize:
                                  ResponsiveFlutter.of(context).fontSize(3.5),

                              fontFamily: 'Gotham',
                              color: Colors.blueGrey.shade800,
                              fontWeight: FontWeight.w400,
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: ResponsiveFlutter.of(context).verticalScale(20),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: ResponsiveFlutter.of(context).scale(40),
                    ),
                    Text(
                      "Merchants",
                      style: TextStyle(
                        color: Colors.blueGrey.shade800,
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w400,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.2),
                      ),
                    ),
                  ],
                ),
                widget.checkResponse == true
                    ? notYet()
                    : Expanded(
                        child: Container(
                          width: ResponsiveFlutter.of(context).scale(300),
                          // height: ResponsiveFlutter.of(context).verticalScale(280),
                          padding: EdgeInsets.all(
                              ResponsiveFlutter.of(context).moderateScale(8)),
                          child: ListView.builder(
                            shrinkWrap: true,
                            padding: EdgeInsets.only(
                                top: ResponsiveFlutter.of(context)
                                    .moderateScale(10)),
                            // itemCount: list.length == null ? 0 : list.length,
                            itemCount: list.length = 40,
                            itemBuilder: (BuildContext context, int index) {
                              print("test1234 ${list}");
                              // print(list.length);

                              return listViewMethod(context, index);
                            },
                          ),
                        ),
                      )
              ],
            ),
            Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(40)),
                    child: Text(
                      "Wallet",
                      style: TextStyle(
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.6),
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w400,
                        // color: Color(0xFF1dbcba),
                        color: Color(0xFFffffff),
                        // fontWeight: FontWeight.w300,
                      ),

                      // style: TextStyle(
                      //   fontSize:  ResponsiveFlutter.of(context).fontSize(3),
                      //   color: Colors.white,
                      //   fontWeight: FontWeight.bold,
                      // ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> apiMethod(BuildContext context) async {
    SharedPreferences prefs3 = await SharedPreferences.getInstance();
    var id = prefs3.getString('cusId');
    print("id123${id}");

    progressShow(context);

    await HttpsRequest()
        .fetchPostAllFriendsData(Constant.myWallet)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);

        list = convertDataToJson['walletto']['merchant'];
        points = convertDataToJson['walletto']['totalBalance']['totalBalance'];
        // points = convertDataToJson['walletto'];
        items = convertDataToJson['walletto']['totalItem']['totalItem'];

        // print("walletList test123   ${list.length}");
        // print("walletList test123   ${list[0]["image"]}");
        // print("walletPoint test123   ${points}");

        if (list.length == null || list.length == 0) {
          widget.checkResponse = true;

          // Fluttertoast.showToast(
          //     msg: "You Dont't Have Merchants",
          //     toastLength: Toast.LENGTH_SHORT,
          //     gravity: ToastGravity.CENTER,
          //     timeInSecForIosWeb: 1);
        }

        setState(() {});
      } else {
        list = new List();
        Fluttertoast.showToast(
            msg: "No Internet",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }

  listViewMethod(BuildContext context, int index) {
    return Container(
      child: Padding(
        padding: EdgeInsets.only(
            bottom: ResponsiveFlutter.of(context).moderateScale(15)),
        child: GestureDetector(
          onTap: () async {
            // var merchantId;
            // merchantId = list[index]['merchantId'];
            //
            //
            // SharedPreferences prefs1 =
            //     await SharedPreferences.getInstance();
            // prefs1.setString('merchantId', merchantId);

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SevenScreen()),
            );
          },
          child: Container(
            margin: EdgeInsets.symmetric(
              horizontal: 5.0,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 1.0,
                  color: Colors.black26,
                  offset: Offset(0.0, 1.0),
                  spreadRadius: 1.0,
                )
              ],
            ),
            height: ResponsiveFlutter.of(context).verticalScale(50),
            // width: ResponsiveFlutter.of(context).scale(20),
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      left: ResponsiveFlutter.of(context).moderateScale(20)),
                  child: Container(
                    width: ResponsiveFlutter.of(context).scale(35),
                    height: ResponsiveFlutter.of(context).verticalScale(35),
                    child: CircleAvatar(
                      radius: 20.0,
                      backgroundImage: NetworkImage(
                          'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                      backgroundColor: Colors.transparent,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      left: ResponsiveFlutter.of(context).moderateScale(20)),
                  // child: Text(list[index]["brandName"].toString(),
                  child: Text(
                    "abc".toString(),
                    style: TextStyle(
                      fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                      color: Color(0XFF5d5d5d),
                      fontFamily: 'Gotham',
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ],
            ),

            // decoration: BoxDecoration(
            //   borderRadius: BorderRadius.circular(5),
            //   color: Colors.white,
            // ),
          ),
        ),
      ),
    );
  }

  notYet() {
    return Container(
      child: Column(
        children: [
          SizedBox(
            height: 60,
          ),
          Image(
            height: 106,
            width: 400,
            image: AssetImage('assets/images/asset12@3x.png'),
            // fit: BoxFit.cover,
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            "You Don't Have Merchant",
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
