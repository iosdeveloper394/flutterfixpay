


import 'dart:convert' as json ;
import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/friendProfile.dart';
import 'package:fixpay/friendRequest.dart';
import 'package:fixpay/searchFriend.dart';
import 'package:fixpay/signUp.dart';
import 'package:fixpay/test.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FriendAllList extends StatefulWidget {
  @override
  _FriendAllListState createState() => _FriendAllListState();
}

class _FriendAllListState extends State<FriendAllList>{

  List list = new List();
  List list1 = new List();


  // @override
  // void initState() {
  //   // TODO: implement initState
  //   apiMethod(context);
  //   super.initState();
  // }



  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return
      Scaffold(
        backgroundColor: Color(0xFFEBFFFE),


        body: SingleChildScrollView(
          child: Container(
            height: screenSize.height,

            child: Stack(

              children: [



                Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom:50.0),
                    child:


                    Image.asset('assets/images/asset6@2x.png',

                        height: 100,
                        // width: 80,
                        color: Color(0xFFfdda65),
                    ),



                    // Image(
                    //   height: 100,
                    //   // width: 80,
                    //   image: AssetImage('assets/images/asset6@2x.png'),
                    //   fit: BoxFit.cover,
                    // ),
                  ),
                ),

                Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [

                    Align(
                      alignment: Alignment.topCenter,
                      // child: Image(
                      //   image: AssetImage('assets/images/asset3@3x.png'),
                      //   // fit: BoxFit.cover,
                      // ),


                      child:
                      Image.asset('assets/images/asset3@3x.png',
                        color: Color(0xFF1dbcba),
                      ),

                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(20),
                    ),

                    Container(
                      height: ResponsiveFlutter.of(context).verticalScale(250),
                      width: ResponsiveFlutter.of(context).scale(300),
                      child: Column(
                        children: [


                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              height: ResponsiveFlutter.of(context).verticalScale(35),
                              child: Padding(
                                padding:  EdgeInsets.only(top: ResponsiveFlutter.of(context).moderateScale(7),
                                left: ResponsiveFlutter.of(context).moderateScale(15)),
                                child: Text("Friend Requests"
                                  ,
                                  style: TextStyle(
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.7),
                                    fontFamily: 'Gotham',
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFF1dbcba),
                                  ),),

                                  // style: TextStyle(
                                  //   fontSize: ResponsiveFlutter.of(context).fontSize(3),
                                  //   color: Color(0xFF1dbcba),
                                  //   fontWeight: FontWeight.bold,
                                  // ),),
                              ),
                            ),
                          ),

                          SizedBox(height: ResponsiveFlutter.of(context).verticalScale(5),
                          ),


                          Expanded(
                            child: ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top:ResponsiveFlutter.of(context).moderateScale(10)),
                              // itemCount: list.length == null ? 20 : list.length,
                              itemCount: list.length = 20,
                              // itemCount: list.length == null ? 20 : list.length,
                              itemBuilder: (BuildContext context, int index) {
                                // print("test1234 ${list}");
                                // print(list.length);

                                return listViewMethod(context, index);
                              },
                            ),
                          ),


                        ],
                      ) ,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,

                      ),
                    ),

                    SizedBox(
                      height: ResponsiveFlutter.of(context).verticalScale(20),
                    ),

                    Container(
                      height: ResponsiveFlutter.of(context).verticalScale(250),
                      width: ResponsiveFlutter.of(context).scale(300),
                      child: Column(
                        children: [


                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              height: ResponsiveFlutter.of(context).verticalScale(35),
                              child: Padding(
                                padding:  EdgeInsets.only(top: ResponsiveFlutter.of(context).moderateScale(7),
                                    left: ResponsiveFlutter.of(context).moderateScale(15)),
                                child: Text("Connect with Friends"
                                  ,


                                  style: TextStyle(
                                    fontSize: ResponsiveFlutter.of(context)
                                        .fontSize(2.7),
                                    fontFamily: 'Gotham',
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFF1dbcba),
                                  ),

                                  // style: TextStyle(
                                  //   fontSize: ResponsiveFlutter.of(context).fontSize(3),
                                  //   color: Color(0xFF1dbcba),
                                  //   fontWeight: FontWeight.bold,
                                  // ),
                                ),
                              ),
                            ),
                          ),

                          SizedBox( height: ResponsiveFlutter.of(context).verticalScale(5),
                          ),


                          Expanded(
                            child: ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: 10),
                              // itemCount: list.length == null ? 20 : list.length,
                              itemCount: list.length = 20,
                              // itemCount: list.length == null ? 20 : list.length,
                              itemBuilder: (BuildContext context, int index) {
                                print("test1234 ${list}");
                                // print(list.length);

                                return listViewMethod1(context, index);
                              },
                            ),
                          ),
                        ],
                      ) ,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),

                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding:  EdgeInsets.only(top:ResponsiveFlutter.of(context).moderateScale(40)),
                    child: Text("Friends",
                      style: TextStyle(
                        fontSize: ResponsiveFlutter.of(context)
                            .fontSize(2.7),
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w500,
                        color: Color(0xFFffffff),
                      ),

                      // style: TextStyle(
                      //   fontSize:  ResponsiveFlutter.of(context).fontSize(3),
                      //   color: Colors.white,
                      //   fontWeight: FontWeight.bold,
                      // ),
                    ),
                  ),
                ),


              ],
            ),
          ),
        ),
      );
  }

  listViewMethod(BuildContext context, int index) {

    // String requestedId = list[index]["friendRequestId"];
    //  print("testreq${requestedId}");

    return   Column(
      children: [
        Padding(
          padding:  EdgeInsets.only(top: ResponsiveFlutter.of(context).moderateScale(5)),
          child: Container(
            height: ResponsiveFlutter.of(context).verticalScale(60),
            color: Color(0xFFFFFFFE),
            child:  Stack(
              children: [
                GestureDetector(
                  onTap: (){
                    showDialog(
                        context: context,
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            scrollable: true,
                            title: Text('Successfully'),
                            content: Form(
                              child: Column(
                                children: <Widget>[

                                  Text(" You deleted this request.",
                                    style: TextStyle(

                                        fontWeight: FontWeight.bold

                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(


                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      Container(
                                        height: 30,
                                        width: 80,
                                        child: RaisedButton(

                                          color: Color(0xFF1dbcba),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15)),
                                          onPressed: () async {
                                            Navigator.of(context).pop();

                                          },

                                          child: Text("Ok"
                                            , style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white
                                            ),),
                                        ),
                                      ),



                                    ],)


                                ],
                              ),
                            ),

                          );
                        });
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                          right: ResponsiveFlutter.of(context).moderateScale(10)),
                      child:
                      Container(
                        // height: ResponsiveFlutter.of(context).verticalScale(38),
                        // width: ResponsiveFlutter.of(context).scale(40),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.red,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child:
                        Container(
                          // height: 20,
                          // width: 20,
                          padding: EdgeInsets.all(10.0),
                          child:Icon(Icons.clear,
                            size: ResponsiveFlutter.of(context).moderateScale(20),
                            color: Colors.red,
                          )
                        ),

                      ),

                    ),
                  ),
                ),


                GestureDetector(
                  onTap: (){
                    showDialog(
                        context: context,
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            scrollable: true,
                            title: Text('Congratulations',
                            style: TextStyle(
                              fontFamily: 'Gotham',
                              fontWeight: FontWeight.w400,

                              color: Color(0XFF5d5d5d),

                            ),),
                            content: Form(
                              child: Column(
                                children: <Widget>[

                                  Text("You and Ali, khan are now friends.",
                                    style: TextStyle(
                                        fontFamily: 'Gotham',
                                        color: Color(0XFF5d5d5d),
                                        fontWeight: FontWeight.w400,


                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(


                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      Container(
                                        height: 30,
                                        width: 80,
                                        child: RaisedButton(

                                          color: Color(0xFF1dbcba),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15)),
                                          onPressed: () async {
                                            Navigator.of(context).pop();

                                          },

                                          child: Text("Ok"
                                            , style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white
                                            ),),
                                        ),
                                      ),



                                    ],)


                                ],
                              ),
                            ),

                          );
                        });

                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                          // top:ResponsiveFlutter.of(context).moderateScale(5),
                          right: ResponsiveFlutter.of(context).moderateScale(60)),
                      child:
                      Container(

                        // height: ResponsiveFlutter.of(context).verticalScale(40),
                        // width: ResponsiveFlutter.of(context).scale(40),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFF1dbcba),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),

                        child:

                        Container(
                            // height: 20,
                            // width: 20,
                            padding: EdgeInsets.all(10.0),
                            child:Icon(Icons.check,
                              size: ResponsiveFlutter.of(context).moderateScale(20),
                              color: Color(0xFF1dbcba),
                            )
                        ),
                      ),
                    ),
                  ),
                ),



                Row(
                  children: [

                    // list[index]["image"] == null ?
                    // Icon(Icons.account_circle_outlined,
                    //   size: 50,
                    //   color: Color(0xFFfdda65),
                    // ) :
                    Center(
                      child: Padding(
                        padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                        child: CircleAvatar(
                          radius: 15.0,
                          backgroundImage:
                          NetworkImage('http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ),

                    Padding(
                      padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                      child: Text("Ali, Khan",


                        style: TextStyle(
                          fontSize: ResponsiveFlutter.of(context)
                              .fontSize(2),
                          color: Color(0XFF5d5d5d),

                          fontFamily: 'Gotham',
                          // fontWeight: FontWeight.w600,
                          // color: Color(0xFF1dbcba),
                          fontWeight: FontWeight.w300,

                        ),
                        // style:
                        // TextStyle(
                        //   fontSize: ResponsiveFlutter.of(context).moderateScale(16)
                        // ),
                      ),
                    ),
                  ],
                ),
              ],
            ),

          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top:5.0),
          child: Container(
            height: ResponsiveFlutter.of(context).verticalScale(60),
            color: Color(0xFFFFFFFE),

            child:  Stack(
              children: [

                GestureDetector(
                  onTap: (){
                    showDialog(
                        context: context,
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            scrollable: true,
                            title: Text('Successfully'),
                            content: Form(
                              child: Column(
                                children: <Widget>[

                                  Text(" You deleted this request.",
                                    style: TextStyle(
                                      fontFamily: 'Gotham',
                                      color: Color(0XFF5d5d5d),
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(


                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      Container(
                                        height: 30,
                                        width: 80,
                                        child: RaisedButton(

                                          color: Color(0xFF1dbcba),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15)),
                                          onPressed: () async {
                                            Navigator.of(context).pop();

                                          },

                                          child: Text("Ok"
                                            , style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white
                                            ),),
                                        ),
                                      ),



                                    ],)


                                ],
                              ),
                            ),

                          );
                        });
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                          right: ResponsiveFlutter.of(context).moderateScale(10)),
                      child:
                      Container(
                        // height: ResponsiveFlutter.of(context).verticalScale(38),
                        // width: ResponsiveFlutter.of(context).scale(40),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.red,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child:
                        Container(
                          // height: 20,
                          // width: 20,
                            padding: EdgeInsets.all(10.0),
                            child:Icon(Icons.clear,
                              size: ResponsiveFlutter.of(context).moderateScale(20),
                              color: Colors.red,
                            )
                        ),

                      ),

                    ),
                  ),
                ),

                

                GestureDetector(
                  onTap: (){
                    showDialog(
                        context: context,
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            scrollable: true,
                            title: Text('Please',style: TextStyle(
                              fontFamily: 'Gotham',
                              color: Color(0XFF5d5d5d),
                              fontWeight: FontWeight.w400,
                            ),),
                            content: Form(
                              child: Column(
                                children: <Widget>[

                                  Text("Wait for Owais to accept your friend request.",
                                    style: TextStyle(

                                      fontSize: ResponsiveFlutter.of(context)
                                          .fontSize(2),

                                      fontFamily: 'Gotham',
                                      color: Color(0XFF5d5d5d),
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(


                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      Container(
                                        height: 30,
                                        width: 80,
                                        child: RaisedButton(

                                          color: Color(0xFF1dbcba),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15)),
                                          onPressed: () async {
                                            Navigator.of(context).pop();

                                          },

                                          child: Text("Ok"
                                            , style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white
                                            ),),
                                        ),
                                      ),



                                    ],)


                                ],
                              ),
                            ),

                          );
                        });
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(right:ResponsiveFlutter.of(context).moderateScale(60)),
                      child:
                      Container(

                        height: ResponsiveFlutter.of(context).verticalScale(20),
                        width: ResponsiveFlutter.of(context).scale(60),
                        child: Center(
                          child: Text("Pending",
                            // style: TextStyle(
                            //     fontSize: ResponsiveFlutter.of(context).moderateScale(12),
                            //     color: Colors.white
                            // ),

                            style: TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .moderateScale(12),
                              fontFamily: 'Gotham',
                              // fontWeight: FontWeight.w600,
                              color: Color(0xFFffffff),
                              fontWeight: FontWeight.w300,
                            ),

                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Color(0xFFfdda65),
                          boxShadow: [
                            BoxShadow(color: Color(0xFFfdda65), spreadRadius: 3),
                          ],
                        ),
                      ),


                    ),
                  ),
                ),



                Row(
                  children: [


                    // list[index]["image"] == null ?
                    // Icon(Icons.account_circle_outlined,
                    //   size: 50,
                    //   color: Color(0xFFfdda65),
                    // ) :
                    Center(
                      child: Padding(
                        padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                        child: CircleAvatar(
                          radius: 15.0,
                          backgroundImage:
                          NetworkImage('http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ),

                    Padding(
                      padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                      child: Text("Owais",
                        // style: TextStyle(
                        //     fontSize: ResponsiveFlutter.of(context).moderateScale(16)
                        // ),

                        style: TextStyle(
                          color: Color(0XFF5d5d5d),

                          fontSize: ResponsiveFlutter.of(context)
                              .moderateScale(16),
                          fontFamily: 'Gotham',
                          // fontWeight: FontWeight.w600,
                          // color: Color(0xFF1dbcba),
                          fontWeight: FontWeight.w300,
                        ),


    ),
                    ),


                  ],
                ),

              ],
            ),


          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top:5.0),
          child: Container(
            height: 50,
            color: Color(0xFFFFFFFE),

            child:  Stack(

              children: [



                GestureDetector(
                  onTap: (){
                    showDialog(
                        context: context,
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            scrollable: true,
                            title: Text('Successfully',
                            style: TextStyle(
                              fontFamily: 'Gotham',
                              color: Color(0XFF5d5d5d),
                              fontWeight: FontWeight.w400,
                            ),),
                            content: Form(
                              child: Column(
                                children: <Widget>[

                                  Text(" You deleted this request.",
                                    style: TextStyle(
                                      fontFamily: 'Gotham',
                                      color: Color(0XFF5d5d5d),
                                      fontWeight: FontWeight.w300,

                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(


                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      Container(
                                        height: 30,
                                        width: 80,
                                        child: RaisedButton(

                                          color: Color(0xFF1dbcba),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15)),
                                          onPressed: () async {
                                            Navigator.of(context).pop();

                                          },

                                          child: Text("Ok"
                                            , style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white
                                            ),),
                                        ),
                                      ),



                                    ],)


                                ],
                              ),
                            ),

                          );
                        });
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                          right: ResponsiveFlutter.of(context).moderateScale(10)),
                      child:
                      Container(
                        // height: ResponsiveFlutter.of(context).verticalScale(38),
                        // width: ResponsiveFlutter.of(context).scale(40),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.red,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child:
                        Container(
                          // height: 20,
                          // width: 20,
                            padding: EdgeInsets.all(10.0),
                            child:Icon(Icons.clear,
                              size: ResponsiveFlutter.of(context).moderateScale(20),
                              color: Colors.red,
                            )
                        ),

                      ),

                    ),
                  ),
                ),


                GestureDetector(
                  onTap: (){
                    showDialog(
                        context: context,
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            scrollable: true,
                            title: Text('Congratulations'),
                            content: Form(
                              child: Column(
                                children: <Widget>[

                                  Text("You and Faraz, Khan are now friends.",
                                    style: TextStyle(

                                        color: Color(0XFF5d5d5d),
                                        fontWeight: FontWeight.bold

                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(


                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      Container(
                                        height: 30,
                                        width: 80,
                                        child: RaisedButton(

                                          color: Color(0xFF1dbcba),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15)),
                                          onPressed: () async {
                                            Navigator.of(context).pop();

                                          },

                                          child: Text("Ok"
                                            , style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white
                                            ),),
                                        ),
                                      ),



                                    ],)


                                ],
                              ),
                            ),

                          );
                        });
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                        // top:ResponsiveFlutter.of(context).moderateScale(5),
                          right: ResponsiveFlutter.of(context).moderateScale(60)),
                      child:
                      Container(

                        // height: ResponsiveFlutter.of(context).verticalScale(40),
                        // width: ResponsiveFlutter.of(context).scale(40),
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFF1dbcba),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),

                        child:

                        Container(
                          // height: 20,
                          // width: 20,
                            padding: EdgeInsets.all(10.0),
                            child:Icon(Icons.check,
                              size: ResponsiveFlutter.of(context).moderateScale(20),
                              color: Color(0xFF1dbcba),
                            )
                        ),
                      ),
                    ),
                  ),
                ),


                Row(
                  children: [


                    // list[index]["image"] == null ?
                    // Icon(Icons.account_circle_outlined,
                    //   size: 50,
                    //   color: Color(0xFFfdda65),
                    // ) :
                    Center(
                      child: Padding(
                        padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                        child: CircleAvatar(
                          radius: 15.0,
                          backgroundImage:
                          NetworkImage('http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ),

                    Padding(
                      padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                      child: Text("Faraz, Khan",
                        // style: TextStyle(
                        //     fontSize: ResponsiveFlutter.of(context).moderateScale(16)
                        // ),

                        style: TextStyle(

                          fontSize: ResponsiveFlutter.of(context)
                              .fontSize(2),
                          fontFamily: 'Gotham',
                          // fontWeight: FontWeight.w600,
                          // color: Color(0xFF1dbcba),
                          fontWeight: FontWeight.w300,
                        ),

                      ),
                    ),


                  ],
                ),

              ],
            ),


          ),
        ),

      ],
    );



  }

  listViewMethod1(BuildContext context, int index) {

    // String requestedId = list[index]["friendRequestId"];
    //  print("testreq${requestedId}");

    return   Column(
      children: [
        Padding(
          padding:  EdgeInsets.only(top:ResponsiveFlutter.of(context).moderateScale(5)),
          child: Container(
            height: ResponsiveFlutter.of(context).verticalScale(60),
            color: Color(0xFFFFFFFF),

            child:  Stack(

              children: [


                GestureDetector(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => FriendProfile()),
                    );
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                          right: ResponsiveFlutter.of(context).moderateScale(10)),
                      child:

                      Container(

                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFFfdda65),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),

                        child:

                        Container(
                          padding: EdgeInsets.all(10.0),
                          child: Image(
                            height: ResponsiveFlutter.of(context).verticalScale(20),
                            width: ResponsiveFlutter.of(context).scale(20),
                            image: AssetImage('assets/images/eyeglasses.png'),
                            fit: BoxFit.contain,
                          ),
                        ),

                      ),

                    ),
                  ),
                ),


                // Align(
                //   alignment: Alignment.centerRight,
                //   child: Padding(
                //     padding:  EdgeInsets.only(
                //         right: ResponsiveFlutter.of(context).moderateScale(58)),
                //     child:
                //
                //     Container(
                //       decoration: BoxDecoration(
                //           border: Border.all(
                //             color: Color(0xFF1dbcba),
                //           ),
                //           borderRadius: BorderRadius.all(Radius.circular(30))
                //       ),
                //
                //       child:
                //
                //       Container(
                //
                //           padding: EdgeInsets.all(10.0),
                //           child:Icon(Icons.share,
                //             size: ResponsiveFlutter.of(context).moderateScale(20),
                //             color: Color(0xFF1dbcba),
                //           )
                //       ),
                //
                //     ),
                //
                //   ),
                // ),


                Row(
                  children: [

                    // list[index]["image"] == null ?
                    // Icon(Icons.account_circle_outlined,
                    //   size: 50,
                    //   color: Color(0xFFfdda65),
                    // ) :
                    Center(
                      child: Padding(
                        padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                        child: CircleAvatar(
                          radius: 15.0,
                          backgroundImage:
                          NetworkImage('http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ),

                    Padding(
                      padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(20)),
                      child: Text("Ali, Khan",
                        // style: TextStyle(
                        //   fontSize: ResponsiveFlutter.of(context).moderateScale(16)
                        // ),
                        style: TextStyle(

                          fontSize: ResponsiveFlutter.of(context)
                              .fontSize(2),
                          color: Color(0XFF5d5d5d),

                          fontFamily: 'Gotham',
                          // fontWeight: FontWeight.w600,
                          // color: Color(0xFF1dbcba),
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),


                  ],
                ),

              ],
            ),


          ),
        ),
        Padding(
          padding:  EdgeInsets.only(top:ResponsiveFlutter.of(context).moderateScale(5)),
          child: Container(
            height: ResponsiveFlutter.of(context).verticalScale(60),
            color: Color(0xFFFFFFFE),
            child:  Stack(
              children: [


                GestureDetector(
                  onTap: (){
                    showDialog(
                        context: context,
                        builder: (BuildContext dialogContext) {
                          return AlertDialog(
                            scrollable: true,
                            title: Text('Friend Request',style:
                              TextStyle(

                                fontFamily: 'Gotham',
                                color: Color(0XFF5d5d5d),
                                fontWeight: FontWeight.w400,
                              ),),
                            content: Form(
                              child: Column(
                                children: <Widget>[

                                  Text("Friend request is sent to John diver..",
                                    style: TextStyle(

                                      fontFamily: 'Gotham',
                                      color: Color(0XFF5d5d5d),
                                      fontWeight: FontWeight.w300,

                                    ),
                                  ),

                                  SizedBox(height: 30,),

                                  Row(


                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      Container(
                                        height: 30,
                                        width: 80,
                                        child: RaisedButton(

                                          color: Color(0xFF1dbcba),
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(15)),
                                          onPressed: () async {
                                            Navigator.of(context).pop();

                                          },

                                          child: Text("Ok"
                                            , style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white
                                            ),),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),

                          );
                        });
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                          right: ResponsiveFlutter.of(context).moderateScale(10)),
                      child:

                      Container(

                        height: ResponsiveFlutter.of(context).verticalScale(20),
                        width: ResponsiveFlutter.of(context).scale(80),
                        child: Center(
                          child: Text("Add Friend",
                            // style: TextStyle(
                            //     fontSize: ResponsiveFlutter.of(context).moderateScale(12),
                            //     color: Colors.white
                            // ),

                            style: TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .moderateScale(12),
                              fontFamily: 'Gotham',
                              // fontWeight: FontWeight.w600,
                              color: Color(0xFFffffff),
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Color(0xFF1dbcba),
                          boxShadow: [
                            BoxShadow(color: Color(0xFF1dbcba), spreadRadius: 3),

                          ],
                        ),
                      ),


                    ),
                  ),
                ),



                Row(
                  children: [

                    // list[index]["image"] == null ?
                    // Icon(Icons.account_circle_outlined,
                    //   size: 50,
                    //   color: Color(0xFFfdda65),
                    // ) :
                    Center(
                      child: Padding(
                        padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                        child: CircleAvatar(
                          radius: 15.0,
                          backgroundImage:
                          NetworkImage('http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ),

                    Padding(
                      padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(20)),
                      child: Text("john diver",
                        // style: TextStyle(
                        //     fontSize: ResponsiveFlutter.of(context).moderateScale(16)
                        // ),
                        style: TextStyle(
                          color: Color(0XFF5d5d5d),

                          fontSize: ResponsiveFlutter.of(context)
                              .fontSize(2),
                          fontFamily: 'Gotham',
                          // fontWeight: FontWeight.w600,
                          // color: Color(0xFF1dbcba),
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),


          ),
        ),

        Padding(
          padding:  EdgeInsets.only(top:ResponsiveFlutter.of(context).moderateScale(5)),
          child: Container(
            height: ResponsiveFlutter.of(context).verticalScale(60),
            color: Color(0xFFFFFFFE),
            child:  Stack(
              children: [


                GestureDetector(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => FriendProfile()),
                    );
                    },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Padding(
                      padding:  EdgeInsets.only(
                          right: ResponsiveFlutter.of(context).moderateScale(10)),
                      child:
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFFfdda65),
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(30))
                        ),
                        child:
                        Container(
                          padding: EdgeInsets.all(10.0),
                          child: Image(
                            height: ResponsiveFlutter.of(context).verticalScale(20),
                            width: ResponsiveFlutter.of(context).scale(20),
                            image: AssetImage('assets/images/eyeglasses.png'),
                            fit: BoxFit.contain,
                          ),
                        ),

                      ),

                    ),
                  ),
                ),


                // Align(
                //   alignment: Alignment.centerRight,
                //   child: Padding(
                //     padding:  EdgeInsets.only(
                //         right: ResponsiveFlutter.of(context).moderateScale(58)),
                //     child:
                //
                //     Container(
                //       decoration: BoxDecoration(
                //           border: Border.all(
                //             color: Color(0xFF1dbcba),
                //           ),
                //           borderRadius: BorderRadius.all(Radius.circular(30))
                //       ),
                //
                //       child:
                //
                //       Container(
                //
                //           padding: EdgeInsets.all(10.0),
                //           child:Icon(Icons.share,
                //             size: ResponsiveFlutter.of(context).moderateScale(20),
                //             color: Color(0xFF1dbcba),
                //           )
                //       ),
                //
                //     ),
                //
                //   ),
                // ),


                Row(
                  children: [

                    // list[index]["image"] == null ?
                    // Icon(Icons.account_circle_outlined,
                    //   size: 50,
                    //   color: Color(0xFFfdda65),
                    // ) :
                    Center(
                      child: Padding(
                        padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(15)),
                        child: CircleAvatar(
                          radius: 15.0,
                          backgroundImage:
                          NetworkImage('http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ),

                    Padding(
                      padding:  EdgeInsets.only(left: ResponsiveFlutter.of(context).moderateScale(20)),
                      child: Text("Ali, Mirza",
                        // style: TextStyle(
                        //     fontSize: ResponsiveFlutter.of(context).moderateScale(16)
                        // ),
                        style: TextStyle(
                          color: Color(0XFF5d5d5d),

                          fontSize: ResponsiveFlutter.of(context)
                              .fontSize(2),
                          fontFamily: 'Gotham',
                          // fontWeight: FontWeight.w600,
                          // color: Color(0xFF1dbcba),
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),

      ],
    );



  }



}
