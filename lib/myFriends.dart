import 'dart:convert' as json;
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ItemPinCode.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/Redeem.dart';
import 'package:fixpay/friendProfile.dart';
import 'package:fixpay/redeemPinCode.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyFriends extends StatefulWidget {
  bool checkResponse = false;
  final bool isFromPoint;
  final bool isFromItem;
  final bool isFromShareFriend;
  final bool isFromFriends;

  MyFriends(
      {Key key,
      this.isFromPoint,
      this.isFromItem,
      this.isFromShareFriend,
      this.isFromFriends})
      : super(key: key);

  @override
  _MyFriendsState createState() => _MyFriendsState();
}

class _MyFriendsState extends State<MyFriends> {
  String id;
  List list = new List();
  List filteredList = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // apiSearchFriends(context);
  }

  void filter(String inputString) {
    print("ara hai $filteredList");
    // print("ara hai 2 ${list[0]['userName']}");

    filteredList = list
        .where((i) => i['userName'].toLowerCase().contains(inputString))
        .toList();

    print("ara hai 3 $filteredList");

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      resizeToAvoidBottomInset: false,
      body: Container(
        height: screenSize.height,
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 50.0),
                child: Image.asset(
                  'assets/images/asset6@3x.png',
                  height: ResponsiveFlutter.of(context).verticalScale(260),
                  color: Color(0xFFfdda65),
                ),

                // Image(
                //   height: 240,
                //   // width: 80,
                //   image: AssetImage('assets/images/asset6@3x.png'),
                //   fit: BoxFit.cover,
                // ),
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.only(top: 20.0),
            //   child: Align(
            //     alignment: Alignment.topRight,
            //     child: Container(
            //       child: Image(
            //         height: 80,
            //         width: 80,
            //         image: AssetImage('assets/images/12.png'),
            //         fit: BoxFit.cover,
            //       ),
            //     ),
            //   ),
            // ),
            Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Image.asset(
                    'assets/images/asset3@3x.png',
                    color: Color(0xFF1dbcba),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30.0),
                  child: Container(
                    height: 35,
                    width: 180,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Icon(
                            Icons.search,
                            size: 30,
                            color: Color(0xFF1dbcba),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Container(
                            height: 23,
                            width: 100,
                            child: TextField(
                              decoration: new InputDecoration.collapsed(
                                hintText: 'Search Friend',
                                hintStyle: TextStyle(
                                    color: Color(0xFF1dbcba), fontSize: 12),
                              ),
                              onChanged: (text) {
                                text = text.toLowerCase();
                                filter(text);
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                widget.checkResponse == true
                    ? notYet()
                    : Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 40),
                          child: Container(
                            // width: 260,
                            margin: EdgeInsets.symmetric(horizontal: 30.0),
                            child: ListView.builder(
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: 10),
                              itemCount: filteredList.length = 1,
                              // itemCount: filteredList.length == null ? 0 : filteredList.length,
                              itemBuilder: (BuildContext context, int index) {
                                print("test1234 ${filteredList.length}");
                                // print(list.length);

                                return listViewMethod(context, index);
                              },
                            ),
                          ),
                        ),
                      ),
              ],
            ),
            Stack(
              children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 50.0),
                    child: Text(
                      "My Friends",
                      style: TextStyle(
                        // color: Colors.blueGrey.shade800,
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                        fontSize: 18,
                        // fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Future<void> apiSearchFriends(BuildContext context) async {
    SharedPreferences prefs3 = await SharedPreferences.getInstance();
    id = prefs3.getString('cusId');
    print("id123${id}");
    progressShow(context);

    String _id = id;

    Map params = {'Id': _id};

    await HttpsRequest()
        .fetchPostMyAllFriendsData(Constant.myFriends, params)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {
        var convertDataToJson = json.jsonDecode(response.body);
        list = convertDataToJson['myfirendslist'];

        print("test123 ${list.length}");

        filteredList = list;
        print("ye hai filterlist api mai $filteredList");

        if (list.length == 0) {
          widget.checkResponse = true;
        }

        setState(() {});
      } else {
        list = new List();
        Fluttertoast.showToast(
            msg: "No Friend in List",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });
  }

  listViewMethod(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10.0,
      ),
      child: GestureDetector(
        onTap: () {
          widget.isFromFriends != true
              ? widget.isFromPoint == true
                  ? Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => Redeem()),
                    )
                  : Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => RedeemPinCode()),
                    )
              : Container();
        },
        child: Container(
          // height: ResponsiveFlutter.of(context).verticalScale(400),
          width: ResponsiveFlutter.of(context).scale(300),
          child: Stack(
            children: [
              widget.isFromShareFriend != true
                  ? Stack(
                      children: [
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Image(
                            height: 20,
                            width: 20,
                            image: AssetImage('assets/images/asset35.png'),
                            // fit: BoxFit.cover,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FriendProfile()),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(right: 22),
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: Image(
                                height: 20,
                                width: 20,
                                image: AssetImage('assets/images/asset36.png'),
                                // fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 280, top: 60),
                          child: Image(
                            height: 80,
                            width: 80,
                            image: AssetImage('assets/images/18.png'),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ],
                    )
                  : Container(
                      color: Colors.white,
                    ),

              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(5)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                      color: Color(0xFFFFFFFF),
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              // list[index]["image"] == null ?
                              // Icon(Icons.account_circle_outlined,
                              //   size: 50,
                              //   color: Color(0xFFfdda65),
                              // ) :
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: ResponsiveFlutter.of(context)
                                          .moderateScale(15)),
                                  child: CircleAvatar(
                                    radius: 15.0,
                                    backgroundImage: NetworkImage(
                                        'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.only(
                                    left: ResponsiveFlutter.of(context)
                                        .moderateScale(20)),
                                child: Text(
                                  "Ali, Khan",
                                  style: TextStyle(
                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .moderateScale(14)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(5)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                      color: Color(0xFFFFFFFE),
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              // list[index]["image"] == null ?
                              // Icon(Icons.account_circle_outlined,
                              //   size: 50,
                              //   color: Color(0xFFfdda65),
                              // ) :
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: ResponsiveFlutter.of(context)
                                          .moderateScale(15)),
                                  child: CircleAvatar(
                                    radius: 15.0,
                                    backgroundImage: NetworkImage(
                                        'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.only(
                                    left: ResponsiveFlutter.of(context)
                                        .moderateScale(20)),
                                child: Text(
                                  "john diver",
                                  style: TextStyle(
                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .moderateScale(14)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(5)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                      color: Color(0xFFFFFFFE),
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              // list[index]["image"] == null ?
                              // Icon(Icons.account_circle_outlined,
                              //   size: 50,
                              //   color: Color(0xFFfdda65),
                              // ) :
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: ResponsiveFlutter.of(context)
                                          .moderateScale(15)),
                                  child: CircleAvatar(
                                    radius: 15.0,
                                    backgroundImage: NetworkImage(
                                        'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.only(
                                    left: ResponsiveFlutter.of(context)
                                        .moderateScale(20)),
                                child: Text(
                                  "Deavek, Jksaeh",
                                  style: TextStyle(
                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .moderateScale(14)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(5)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                      color: Color(0xFFFFFFFF),
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              // list[index]["image"] == null ?
                              // Icon(Icons.account_circle_outlined,
                              //   size: 50,
                              //   color: Color(0xFFfdda65),
                              // ) :
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: ResponsiveFlutter.of(context)
                                          .moderateScale(15)),
                                  child: CircleAvatar(
                                    radius: 15.0,
                                    backgroundImage: NetworkImage(
                                        'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.only(
                                    left: ResponsiveFlutter.of(context)
                                        .moderateScale(20)),
                                child: Text(
                                  "Ali, Khan",
                                  style: TextStyle(
                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .moderateScale(14)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(5)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                      color: Color(0xFFFFFFFF),
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              // list[index]["image"] == null ?
                              // Icon(Icons.account_circle_outlined,
                              //   size: 50,
                              //   color: Color(0xFFfdda65),
                              // ) :
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: ResponsiveFlutter.of(context)
                                          .moderateScale(15)),
                                  child: CircleAvatar(
                                    radius: 15.0,
                                    backgroundImage: NetworkImage(
                                        'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.only(
                                    left: ResponsiveFlutter.of(context)
                                        .moderateScale(20)),
                                child: Text(
                                  "Ali, Khan",
                                  style: TextStyle(
                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .moderateScale(14)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(5)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                      color: Color(0xFFFFFFFF),
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              // list[index]["image"] == null ?
                              // Icon(Icons.account_circle_outlined,
                              //   size: 50,
                              //   color: Color(0xFFfdda65),
                              // ) :
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: ResponsiveFlutter.of(context)
                                          .moderateScale(15)),
                                  child: CircleAvatar(
                                    radius: 15.0,
                                    backgroundImage: NetworkImage(
                                        'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              ),

                              Padding(
                                padding: EdgeInsets.only(
                                    left: ResponsiveFlutter.of(context)
                                        .moderateScale(20)),
                                child: Text(
                                  "Ali, Khan",
                                  style: TextStyle(
                                      color: Colors.blueGrey.shade800,
                                      fontFamily: 'Gotham',
                                      fontWeight: FontWeight.w300,
                                      fontSize: ResponsiveFlutter.of(context)
                                          .moderateScale(14)),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: ResponsiveFlutter.of(context).moderateScale(5)),
                    child: Container(
                      height: ResponsiveFlutter.of(context).verticalScale(60),
                      color: Color(0xFFFFFFFF),
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              // list[index]["image"] == null ?
                              // Icon(Icons.account_circle_outlined,
                              //   size: 50,
                              //   color: Color(0xFFfdda65),
                              // ) :
                              Center(
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: ResponsiveFlutter.of(context)
                                          .moderateScale(15)),
                                  child: CircleAvatar(
                                    radius: 15.0,
                                    backgroundImage: NetworkImage(
                                        'http://hustledev-001-site8.itempurl.com/fixpayimages/customer/profileimages/5c7118cb-c42f-4217-b9a2-428407b5ced4_IMG20210213115902942BURST001.jpg'),
                                    backgroundColor: Colors.transparent,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    left: ResponsiveFlutter.of(context)
                                        .moderateScale(20)),
                                child: Align(
                                  child: Text(
                                    "Ali, Khan",
                                    style: TextStyle(
                                        color: Colors.blueGrey.shade800,
                                        fontFamily: 'Gotham',
                                        fontWeight: FontWeight.w300,
                                        fontSize: ResponsiveFlutter.of(context)
                                            .moderateScale(14)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),

              // Row(
              //   children: [
              //
              //     Padding(
              //       padding: const EdgeInsets.only(left:10.0),
              //       child: filteredList[index]["image"] == null ?
              //       Icon(Icons.account_circle_outlined,
              //         size: 50,
              //         color: Color(0xFF1dbcba),
              //       ) :
              //       CircleAvatar(
              //         radius: 25.0,
              //         backgroundImage:
              //         NetworkImage('http://hustledev-001-site8.itempurl.com/'+filteredList[index]["image"]),
              //         backgroundColor: Colors.transparent,
              //       )
              //     ),
              //
              //     Padding(
              //       padding: const EdgeInsets.only(left:10.0,top:20),
              //       child: Column(
              //         crossAxisAlignment: CrossAxisAlignment.start, // for left side
              //         children: [
              //           Text( filteredList[index]["userName"],
              //             style: TextStyle(
              //             ),
              //           ),
              //
              //           Text("Lorem ipsum is simple dummy text",
              //             style: TextStyle(
              //                 fontSize: 8
              //             ),
              //           ),
              //
              //           SizedBox(
              //             height: 2,
              //           ),
              //
              //           Text("of the printing and typesetting industry",
              //             style: TextStyle(
              //                 fontSize: 8
              //             ),
              //           ),
              //
              //         ],
              //       ),
              //     ),
              //   ],
              // ),
            ],
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12), color: Colors.white),
        ),
      ),
    );
  }

  notYet() {
    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Container(
        child: Column(
          children: [
            SizedBox(
              height: 60,
            ),
            Image(
              height: 106,
              width: 400,
              image: AssetImage('assets/images/asset12@3x.png'),
              // fit: BoxFit.cover,
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              "You Don't Have Friend",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
