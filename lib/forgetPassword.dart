import 'dart:async';
import 'dart:convert' as json;
import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:fixpay/signIn.dart';
import 'package:fixpay/verification.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ForgetPasswordScreen extends StatefulWidget {
  final Color backgroundColor = Colors.white;
  final TextStyle styleTextUnderTheLoader = TextStyle(
      fontFamily: 'Gotham',
      fontSize: 18.0,
      fontWeight: FontWeight.bold,
      color: Colors.black);

  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  final emailForget = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Color(0xFFEBFFFE),
      body: SingleChildScrollView(
        child: Container(
          height: screenSize.height,
          child: Column(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: ResponsiveFlutter.of(context).verticalScale(98),

                  child: Image.asset(
                    'assets/images/asset3@3x.png',
                    color: Color(0xFF1dbcba),
                  ),

                  // Image(
                  //   image: AssetImage('assets/images/asset3@3x.png'),
                  // ),
                ),
              ),

              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),

              Text(
                "Forget Password?",
                style: TextStyle(
                    fontFamily: 'Gotham',
                    fontSize: ResponsiveFlutter.of(context).fontSize(2.8),
                    fontWeight: FontWeight.w400),
              ),

              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),

              // Image(
              //   height: ResponsiveFlutter.of(context).verticalScale(110),
              //   width: ResponsiveFlutter.of(context).scale(110),
              //   image: AssetImage('assets/images/asset12@3x.png'),
              //   // fit: BoxFit.cover,
              // ),

              Image.asset(
                'assets/images/asset12@3x.png',
                height: ResponsiveFlutter.of(context).verticalScale(110),
                width: ResponsiveFlutter.of(context).scale(110),
                color: Color(0xFF1dbcba),
              ),

              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),

              Container(
                padding: EdgeInsets.symmetric(horizontal: 23.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "Enter the Email Address or ",
                      style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                        color: Color(0XFF5d5d5d),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),



              Container(
                padding: EdgeInsets.symmetric(horizontal: 23.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "Phone Number Associated with",
                      style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                        color: Color(0XFF5d5d5d),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),

              Container(
                padding: EdgeInsets.symmetric(horizontal: 23.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "your account.",
                      style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                        color: Color(0XFF5d5d5d),
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),

              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),

              Container(
                padding: EdgeInsets.symmetric(horizontal: 23.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Center(
                    child: Text(
                      "We will email you a link to reset your password.",
                      style: TextStyle(
                          fontFamily: 'Gotham',
                          fontWeight: FontWeight.w300,
                          fontSize: ResponsiveFlutter.of(context).fontSize(2.5),
                        color: Color(0XFF5d5d5d),),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),



              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(20),
              ),

              Container(
                  height: ResponsiveFlutter.of(context).verticalScale(50),
                  margin: EdgeInsets.symmetric(horizontal: 60.0),
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(20.0),
                      color: Colors.white),
                  child: Row(
                    children: [
                      SizedBox(
                        width: ResponsiveFlutter.of(context).scale(25),
                      ),
                      Image(
                        height: ResponsiveFlutter.of(context).verticalScale(28),
                        width: ResponsiveFlutter.of(context).scale(28),
                        image: AssetImage('assets/images/artboard16.png'),
                        fit: BoxFit.contain,
                      ),
                      SizedBox(
                        width: ResponsiveFlutter.of(context).scale(20),
                      ),
                      Expanded(
                        child: Container(
                          height: ResponsiveFlutter.of(context).verticalScale(30),
                          child: Align(
                            alignment: Alignment.centerLeft,
                            child: TextField(
                              maxLines: 1,
                              controller: emailForget,
                              style: TextStyle(
                                color: Color(0XFF5d5d5d),
                                fontFamily: 'Gotham',
                                fontWeight: FontWeight.w300,
                                fontSize:
                                    ResponsiveFlutter.of(context).fontSize(1.5),
                              ),
                              decoration: InputDecoration(
                                  isDense: true,
                                  border: InputBorder.none,
                                  hintText: 'Email Address/ Username'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),

              SizedBox(
                height: ResponsiveFlutter.of(context).verticalScale(25),
              ),
              Container(
                height: ResponsiveFlutter.of(context).verticalScale(40),
                width: ResponsiveFlutter.of(context).scale(150),
                child: RaisedButton(
                  color: Color(0xFFfdda65),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  onPressed: () async {
                    if (emailForget.text == "" || emailForget.text == null) {
                      Fluttertoast.showToast(
                          msg: "Enter Your Email",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1);
                    } else {
                      String _email = emailForget.text;

                      Map params = {'Email': _email};

                      print("aaaa${_email}");

                      progressShow(context);
                      await HttpsRequest()
                          .fetchPostForgetPass(Constant.forgetPass, params)
                          .then((response) async {
                        progressHide(context);

                        print("asasas ${response.statusCode}");
                        if (response.statusCode == 200) {
                          SharedPreferences prefs1 =
                              await SharedPreferences.getInstance();
                          prefs1.setString('emailForget', _email);

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VerificationScreen()),
                          );
                        } else {
                          Fluttertoast.showToast(
                              msg: "Enter Your Correct Email",
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.CENTER,
                              timeInSecForIosWeb: 1);
                        }
                      });
                    }
                  },
                  child: Text(
                    "Send",
                    style: TextStyle(
                        fontFamily: 'Gotham',
                        fontWeight: FontWeight.w300,
                        fontSize: ResponsiveFlutter.of(context).fontSize(2.0),
                        color: Colors.white),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
