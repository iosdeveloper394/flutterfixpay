

import 'dart:convert' as json ;

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/ProgressBar/progressBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchFriends extends StatefulWidget {
  bool checkResponse = false;

  @override
  _SearchFriendsState createState() => _SearchFriendsState();
}


class _SearchFriendsState extends State<SearchFriends>{
  List list = new List();
  List filteredList = List();
  String userId;
  String friendId;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    apiSearchFriends(context);
    Data();
  }

  void filter(String inputString) {

    print("ara hai $filteredList");
    // print("ara hai 2 ${list[0]['userName']}");

    filteredList =
        list.where((i) => i['userName'].toLowerCase().contains(inputString)).toList();


    print("ara hai 3 $filteredList");

    setState(() {});

  }


  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return
      Scaffold(
        backgroundColor: Color(0xFFEBFFFE),
        resizeToAvoidBottomInset: false,


        body: Container(
          height: screenSize.height,

          child: Stack(

            children: [


              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom:50.0),
                  child: Image(
                    height: 240,
                    // width: 80,
                    image: AssetImage('assets/images/asset6@3x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(top:20.0),
                child: Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    child: Image(
                      height: 80,
                      width: 80,
                      image: AssetImage('assets/images/12.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),


              Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Align(
                    alignment: Alignment.topCenter,
                    child:
              Image.asset('assets/images/asset3@3x.png',
                color: Color(0xFF1dbcba),
              ),
                  ),

                  SizedBox(height: 30,),

                  Padding(
                    padding: const EdgeInsets.only(left:30.0),
                    child: Container(
                      height: 35,
                      width: 180,
                      child: Row(
                        children: [

                          Padding(
                            padding: const EdgeInsets.only(left:10.0),
                            child: Icon(Icons.search,
                              size: 30,
                              color: Color(0xFF1dbcba),
                            ),
                          ),


                          Padding(
                            padding: const EdgeInsets.only(left:10.0),
                            child: Container(
                              height: 23,
                              width: 100,
                              child:

                              TextField(
                                decoration: new InputDecoration.collapsed(
                                    hintText: 'Search Friend',
                                    hintStyle: TextStyle(
                                      color: Color(0xFF1dbcba),
                                      fontSize: 12
                                    ),
                                ),

                                    onChanged: (text) {
                                      text = text.toLowerCase();

                                      filter(text);
                                    },
                                  ),


                            ),
                          ),
                        ],
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: Colors.white
                      ),
                    ),
                  ),


                  SizedBox(
                    height: 20,
                  ),

                  widget.checkResponse == true ? notYet() :

                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 30.0),

                      child: ListView.builder(
                        shrinkWrap: true,
                        padding: EdgeInsets.only(top: 10),
                        itemCount: filteredList.length == null ? 0 : filteredList.length,
                        itemBuilder: (BuildContext context, int index) {
                          print("test1234 ${filteredList}");
                          // print(list.length);

                          return listViewMethod(context, index);
                        },
                      ),
                    ),
                  ),
                ],
              ),


              Stack(children: [
                Align(
                  alignment: Alignment.topCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(top:50.0),
                    child: Text("Search Friends",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ),
              ],)


            ],
          ),
        ),
      );


  }

  Future<void> apiSearchFriends(BuildContext context)  async {

    SharedPreferences prefs3 = await SharedPreferences.getInstance();
    var id = prefs3.getString('cusId');
    print("id123${id}");

    progressShow(context);

    await HttpsRequest()
        .fetchPostAllFriendsData(Constant.friendsAll)
        .then((response) async {
          progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {


        var convertDataToJson = json.jsonDecode(response.body);
        list = convertDataToJson['friendlistto'];
        // list.add(value)
        print("test123 ${list.length}");
        filteredList = list;
        print("ye hai filterlist api mai $filteredList");

        setState(() {});


      }else{
        list = new List();
        Fluttertoast.showToast(
            msg: "No Friend in List",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });

  }


  Future<void> apiSearchFriends2(BuildContext context)  async {

    // SharedPreferences prefs3 = await SharedPreferences.getInstance();
    // var id = prefs3.getString('cusId');
    // print("id123${id}");

    progressShow(context);


    print("ara hai api 2 pr");

    await HttpsRequest()
        .fetchPostAllFriendsData(Constant.friendsAll)
        .then((response) async {
      progressHide(context);

      print("asasas ${response.statusCode}");
      if (response.statusCode == 200) {


        var convertDataToJson = json.jsonDecode(response.body);
        list = convertDataToJson['friendlistto'];
        // list.add(value)
        print("test123 ${list.length}");
        filteredList = list;
        print("ye hai filterlist api mai $filteredList");


        if(list.length == 0){

          widget.checkResponse = true;
        }

        setState(() {});

      }else{
        list = new List();
        Fluttertoast.showToast(
            msg: "Internet Issue",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1);
      }
    });

  }


  listViewMethod(BuildContext context, int index) {

    return  Padding(
      padding: const EdgeInsets.only(top:10.0),
      child:

      GestureDetector(

        onTap: (){

         friendId = filteredList[index]['customerId'].toString();
          setState(() {
          });

          _showMyDialog(index);

        },

        child: Container(
          height: 80,
          width: 300,
          child: Stack(
            children: [

              Align(
                alignment: Alignment.bottomRight,
                child: Image(
                  height: 20,
                  width: 20,
                  image: AssetImage('assets/images/asset35.png'),
                  // fit: BoxFit.cover,
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(right: 22),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: Image(
                    height: 20,
                    width: 20,
                    image: AssetImage('assets/images/asset36.png'),
                    // fit: BoxFit.cover,
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left:280, top:60),
                child: Image(
                  height: 80,
                  width: 80,
                  image: AssetImage('assets/images/18.png'),
                  fit: BoxFit.cover,
                ),
              ),

              Row(
                children: [

                  Padding(
                    padding: const EdgeInsets.only(left:10.0),
                    child: filteredList[index]["image"] == null ?
                    Icon(Icons.account_circle_outlined,
                      size: 50,
                      color: Color(0xFF1dbcba),
                    ) :

                    CircleAvatar(
                      radius: 25.0,
                      backgroundImage:
                      NetworkImage('http://hustledev-001-site8.itempurl.com/'+filteredList[index]["image"]),
                      backgroundColor: Colors.transparent,
                    )
                    ),
                  Padding(
                    padding: const EdgeInsets.only(left:10.0,top:20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start, // for left side
                      children: [
                        Text(

                          filteredList[index]["userName"].toString(),

                          style: TextStyle(
                          ),
                        ),

                        Text("Lorem ipsum is simple dummy text",
                          style: TextStyle(
                              fontSize: 8
                          ),
                        ),

                        SizedBox(
                          height: 2,
                        ),

                        Text("of the printing and typesetting industry",
                          style: TextStyle(
                              fontSize: 8
                          ),
                        ),

                      ],
                    ),
                  ),
                ],
              ),



            ],
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              color: Colors.white
          ),
        ),
      ),
    );

   }

  Future<void> _showMyDialog(int index) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Send Friend Request'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[

                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 40,
                  width: 100,
                  child: RaisedButton(

                    color: Color(0xFFfdda65),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    onPressed: () async {

                      String _userId = userId;
                      String _friendId = friendId;

                        print("ye hai userId  ${_userId}");
                        print("ye hai FriendId ${_friendId}");


                        Map params = {
                          'CustomerRequestedId': _userId,
                          'CustomerRecievedId': _friendId,
                        };

                      progressShow(context);

                      await HttpsRequest()
                            .fetchPostTokenData(Constant.sendFriendsRequest, params)
                            .then((response) async {


                          print("asasas ${response.statusCode}");

                          if (response.statusCode == 200) {

                            Fluttertoast.showToast(
                                msg: "Request Sent successfully",
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);
                            progressHide(context);

                            print("yesyes ara hai");

                            apiSearchFriends2(context);

                            Navigator.of(context).pop();

                          } else {
                            Fluttertoast.showToast(
                                msg: "Issue",
                                toastLength: Toast
                                    .LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIosWeb: 1);
                          }
                        }
                        );
                      },

                    child: Text("Send Friend Request"
                      , style: TextStyle(
                          fontSize: 16,
                          color: Colors.white
                      ),),
                  ),
                ),


              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Close'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],

        );
      },
    );
  }

  Future<void> Data() async {


    SharedPreferences prefs3 = await SharedPreferences.getInstance();
    userId = prefs3.getString('cusId');
    print("id123${userId}");

  }
  notYet() {

    return Padding(
      padding: const EdgeInsets.only(right: 20),
      child: Container(

        child: Column(
          children: [

            SizedBox(height: 60,),
            Image(
              height: 106,
              width: 400,
              image: AssetImage('assets/images/asset12@3x.png'),
              // fit: BoxFit.cover,
            ),
            SizedBox(
              height: 30,
            ),

            Text("No One In Search Friend",
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold

              ),
            ),
          ],
        ),

      ),
    );


  }




}

