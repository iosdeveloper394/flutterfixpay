


import 'dart:convert' as json ;
import 'dart:io';

import 'package:fixpay/Http/constant.dart';
import 'package:fixpay/Http/http.dart';
import 'package:fixpay/forgetPassword.dart';
import 'package:fixpay/friendRequest.dart';
import 'package:fixpay/myFriends.dart';
import 'package:fixpay/searchFriend.dart';
import 'package:fixpay/signUp.dart';
import 'package:fixpay/test.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Friends extends StatefulWidget {
  @override
  _FriendsState createState() => _FriendsState();
}

class _FriendsState extends State<Friends>{

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return
      Scaffold(
        backgroundColor: Color(0xFFEBFFFE),
        resizeToAvoidBottomInset: false,


        body: Container(
          height: screenSize.height,

          child: Stack(

            children: [

              Align(
                alignment: Alignment.topCenter,
                child:
              Image.asset('assets/images/asset3@3x.png',
                color: Color(0xFF1dbcba),
              ),
              ),


              SizedBox(
                height: 15,
              ),

              Padding(
                padding: const EdgeInsets.only(top:50.0),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text("Friends",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),



              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(bottom:50.0),
                  child: Image(
                    height: 100,
                    // width: 80,
                    image: AssetImage('assets/images/asset6@2x.png'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),



              Padding(
                padding: const EdgeInsets.only(top:20.0),
                child: Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    child: Image(
                      height: 80,
                      width: 80,
                      image: AssetImage('assets/images/12.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),

              Align(
                alignment: Alignment.center,
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [

                    SizedBox(height: 70,),


                    SizedBox(
                      height: 160,
                    ),


                    GestureDetector(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MyFriends(isFromFriends: true)),
                        );
                      },

                      child: Container(
                        width: 240,
                        child:
                        Center(child: Text("My Friends",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17
                          ),
                        ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xFFfdda65),
                        ),
                        height: 40,
                      ),
                    ),


                    SizedBox(
                      height: 30,
                    ),


                    GestureDetector(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => SearchFriends()),
                          );
                        },
                      child: Container(

                        width: 240,
                        child:
                        Center(child: Text("Search friends",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17
                        ),
                        ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xFFfdda65),
                        ),
                        height: 40,
                      ),
                    ),

                    SizedBox(
                      height: 30,
                    ),


                    new GestureDetector(
                      onTap: (){

                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => FriendRequest()),
                        );

                      },
                      child: Container(

                        width: 240,
                        child:
                        Center(child: Text("Friend Requests",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 17
                          ),
                        ),
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xFFfdda65),
                        ),
                        height: 40,
                      ),
                    ),

                  ],
                ),
              ),

            ],
          ),
        ),
      );
  }

}
