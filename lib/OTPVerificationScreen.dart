// import 'package:flutter/services.dart';
// import 'dart:async';
// import 'package:location/location.dart';
// import 'package:wee_app/Network/DTOs/VerifyOTPDTO.dart';
// import 'package:wee_app/SessionManager/SessionManager.dart';
// import 'package:flutter/material.dart';
// import 'package:wee_app/Constant/constant.dart';
// import 'package:wee_app/Screen/HomeScreen.dart';
// import 'package:wee_app/UseCases/UserUseCase.dart';
// import 'package:wee_app/Validator/Validator.dart';
// import 'package:wee_app/Widget/CustomLoader.dart';
// import 'package:wee_app/Widget/Toast.dart';
// import 'package:wee_app/Widget/ProgressBar.dart';
// import 'package:wee_app/Models/CustomerModel.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
// import 'package:pin_code_fields/pin_code_fields.dart';
// import 'package:flutter/gestures.dart';
// import 'SignupScreen.dart';
//
// class OTPVerificationScreen extends StatefulWidget {
//   final String phoneNumber;
//   final int verificationCode;
//
//   OTPVerificationScreen({Key key, @required this.phoneNumber, @required this.verificationCode}) : super(key: key);
//
//   @override
//   _OTPVerificationScreenState createState() => _OTPVerificationScreenState();
// }
//
// class _OTPVerificationScreenState extends State<OTPVerificationScreen> {
//   final codeAController = TextEditingController();
//   final codeBController = TextEditingController();
//   final codeCController = TextEditingController();
//   final codeDController = TextEditingController();
//   final codeEController = TextEditingController();
//   final codeFController = TextEditingController();
//   Validator validator;
//   int secondsRemaining = 60;
//   bool enableResend = false;
//   Timer timer;
//
//   var onTapRecognizer;
//
//   TextEditingController textEditingController =
//       TextEditingController(); // ..text = "123456";
//   StreamController<ErrorAnimationType> errorController;
//
//   bool hasError = false;
//   String currentText = "";
//   final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
//   final formKey = GlobalKey<FormState>();
//
//   FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
//   new FlutterLocalNotificationsPlugin();
//
//
//   @override
//   void initState() {
//     onTapRecognizer = TapGestureRecognizer()
//       ..onTap = () {
//         Navigator.pop(context);
//       };
//     errorController = StreamController<ErrorAnimationType>();
//
//     super.initState();
//     timer = Timer.periodic(Duration(seconds: 1), (_) {
//       if (secondsRemaining != 0) {
//         setState(() {
//           secondsRemaining--;
//         });
//       } else {
//         setState(() {
//           enableResend = true;
//         });
//       }
//     });
//
//     widget.verificationCode==001 ? print("Dont Hit Verification Code in OTP Screen"): phoneNumberCheckerForNotification(context);
//   }
//
//   @override
//   void dispose() {
//     errorController.close();
//     timer.cancel();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.blue.shade50,
//       resizeToAvoidBottomInset: false,
//       key: scaffoldKey,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         bottomOpacity: 0.0,
//         elevation: 1.0,
//         leading: IconButton(
//             icon: Icon(Icons.arrow_back, color: Colors.black),
//             onPressed: () {
//               Future.delayed(Duration.zero, () {
//                 Navigator.pop(context);
//               });
//             }),
//         centerTitle: true,
//         title: Text(
//           "Phone Confirmation",
//           style: TextStyle(color: Colors.black),
//         ),
//       ),
//       body: Container(
//         height: MediaQuery.of(context).size.height,
//         width: MediaQuery.of(context).size.width,
//         color: Colors.white,
//         child: Stack(
//           children: [
//             Column(
//               children: <Widget>[
//                 SizedBox(height: 50),
//                 Padding(
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8),
//                   child: RichText(
//                     text: TextSpan(
//                         text: "Enter the code sent via SMS to ",
//                         children: [
//                           TextSpan(
//                               text: widget.phoneNumber,
//                               style: TextStyle(
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 15)),
//                         ],
//                         style: TextStyle(color: Colors.black54, fontSize: 15)),
//                     textAlign: TextAlign.center,
//                   ),
//                 ),
//                 SizedBox(
//                   height: 20,
//                 ),
//                 Form(
//                   key: formKey,
//                   child: Padding(
//                       padding:
//                           const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
//                       child: PinCodeTextField(
//                         appContext: context,
//                         inputFormatters: [FilteringTextInputFormatter.digitsOnly],
//                         pastedTextStyle: TextStyle(
//                           color: Colors.green.shade600,
//                           fontWeight: FontWeight.bold,
//                         ),
//                         length: 6,
//                         obscureText: false,
//                         obscuringCharacter: '*',
//                         blinkWhenObscuring: true,
//                         animationType: AnimationType.fade,
//                         // validator: (v) {
//                         //   if (v.length < 3) {
//                         //     return "I'm from validator";
//                         //   } else {
//                         //     return null;
//                         //   }
//                         // },
//                         pinTheme: PinTheme(
//                           shape: PinCodeFieldShape.box,
//                           borderRadius: BorderRadius.circular(5),
//                           inactiveColor: hexToColor(backgroundColor),
//                           activeColor: hexToColor(backgroundColor),
//                           disabledColor: hexToColor(backgroundColor),
//                           inactiveFillColor: hexToColor(backgroundColor),
//                           selectedColor: hexToColor(backgroundColor),
//                           selectedFillColor: hexToColor(backgroundColor),
//                           fieldHeight: 60,
//                           fieldWidth: 50,
//                           activeFillColor: hexToColor(backgroundColor),
//                         ),
//                         cursorColor: Colors.black,
//                         animationDuration: Duration(milliseconds: 300),
//                         enableActiveFill: true,
//                         errorAnimationController: errorController,
//                         controller: textEditingController,
//                         keyboardType: TextInputType.number,
//                         textInputAction:TextInputAction.go,
//                         boxShadows: [
//                           BoxShadow(
//                             offset: Offset(0, 1),
//                             color: Colors.black12,
//                             blurRadius: 10,
//                           )
//                         ],
//                         onCompleted: (v) {
//                           print("Completed");
//                           print(currentText);
//                           validateData(context);
//                           // gotoHomeScreen(context);
//                         },
//                         onChanged: (value) {
//                           print(value);
//                           setState(() {
//                             currentText = value;
//                           });
//                         },
//                         beforeTextPaste: (text) {
//                           print("Allowing to paste $text");
//                           //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
//                           //but you can show anything you want here, like your pop up saying wrong paste format or etc
//                           return true;
//                         },
//                       )),
//                 ),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     FlatButton(
//                       child: Text(
//                         'Resend Code :',
//                         style: TextStyle(
//                           fontSize: 16.0,
//                           color: Colors.grey,
//                           fontWeight: FontWeight.w500,
//                         ),
//                       ),
//                       onPressed: enableResend ? _resendCode : null,
//                     ),
//                     Text(
//                       '$secondsRemaining',
//                       style: TextStyle(
//                         fontSize: 16.0,
//                         color: Colors.grey,
//                         fontWeight: FontWeight.w500,
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//             Align(
//               alignment: FractionalOffset.bottomCenter,
//               child: Padding(
//                 padding: EdgeInsets.all(50.0),
//                 child: Container(
//                   height: 45.0,
//                   width: MediaQuery.of(context).size.width * 0.95,
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(22.0),
//                     border: Border.all(color: Colors.black, width: 2.0),
//                   ),
//                   child: RaisedButton(
//                     color: hexToColor(secondaryColor),
//                     onPressed: () {
//                       validateData(context);
//                     },
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(22.0),
//                     ),
//                     child: Text(
//                       "Continue",
//                       style: TextStyle(
//                           color: Colors.black,
//                           fontWeight: FontWeight.bold,
//                           fontSize: 18.0
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   void gotoHomeScreen(BuildContext context) {
//
//   }
//
//   void _resendCode() {
//     //other code here
//     setState(() {
//       secondsRemaining = 60;
//       enableResend = false;
//     });
//   }
//
//   validateData(BuildContext context) {
//     formKey.currentState.validate();
//     // conditions for validating
//     if (currentText.length != 6 ) {
//       errorController.add(ErrorAnimationType.shake); // Triggering error shake animation
//       setState(() {hasError = true;});
//     } else {
//       setState(() {
//         hasError = false;
//         // scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Aye!!"), duration: Duration(seconds: 2),));
//       });
//     // }
//     //
//     //
//     //
//     //
//     // FocusManager.instance.primaryFocus.unfocus();
//     // validator = Validator(context);
//     //
//     // String _codeA = codeAController.text;
//     // String _codeB = codeBController.text;
//     // String _codeC = codeCController.text;
//     // String _codeD = codeDController.text;
//     // String _codeE = codeEController.text;
//     // String _codeF = codeFController.text;
//     //
//     // if ((_codeA.isEmpty) ||
//     //     (_codeB.isEmpty) ||
//     //     (_codeC.isEmpty) ||
//     //     (_codeD.isEmpty) ||
//     //     (_codeE.isEmpty) ||
//     //     (_codeF.isEmpty)) {
//     //   Toast.show("Please fill all fields", context,
//     //       duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//     // } else {
//     //   String concatLetters = _codeA+_codeB+_codeC+_codeD+_codeE+_codeF;
//       String concatLetters = currentText;
//       print(concatLetters);
//       verifyOTPApi(context,concatLetters);
//     }
//   }
//
//   void verifyOTPApi(BuildContext context, String otp) {
//     UserUseCase userUseCase = new UserUseCase();
//     showAlertDialog(context);
//     print("OTP IS $otp, \n Phone is ${widget.phoneNumber}");
//     userUseCase.verifyOTP(otp, widget.phoneNumber).then((value) {
//       hideLoader(context);
//       if(value.statusCode==200){
//         if(value.data.data!=null){
//           saveSessionAndGotoHome(context,value.data);
//         }else{
//           showToast("Data is null", context);
//         }
//       }else if(value.statusCode==404){
//         gotoSignupScreen(context);
//         showToast("Please register", context);
//       } else if(value.statusCode==400) {
//         showToast("Incorrect OTP", context);
//       } else{
//         showToast("Something went wrong", context);
//       }
//     });
//   }
//
//   void gotoSignupScreen(BuildContext context) {
//     Navigator.push(
//       context,
//       MaterialPageRoute(builder: (context) => SignupScreen()),
//     );
//   }
//
//   void saveSessionAndGotoHome(BuildContext context, VerifyOTPDTO data) {
//     CustomerModel customerModel = new CustomerModel(fullName: data.data.customer.fullName,phoneNumber: data.data.customer.phoneNumber, email: data.data.customer.email, walletdto: data.data.customer.walletdto);
//
//     int result = customerModel.walletdto.walletAmount + customerModel.walletdto.bonusAmount;
//
//     new SessionManager().setUserInfo(customerModel.fullName, customerModel.email, customerModel.phoneNumber, data.data.token, result.toString());
//     gotoHomeScreen(context);
//
//     if(data.data.ridedto!=null){
//             Navigator.of(context).pushAndRemoveUntil(
//                 MaterialPageRoute(
//                     builder: (context) => HomeScreen(
//                           rideDTOFromMain: data.data.ridedto,
//                         ),
//                     fullscreenDialog: true),
//                 (Route<dynamic> route) => false);
//     SessionManager().setCurrentRideModel(data.data.ridedto);
//     }else Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => HomeScreen(),fullscreenDialog: true), (Route<dynamic> route) => false);
//
//
//   }
//
//   Future<void> _demoNotification(String title, String body) async {
//     var androidPlatformChannelSpecifics = AndroidNotificationDetails(
//         'channel_ID', 'channel name', 'channel description',
//         importance: Importance.max,
//         playSound: true,
//         // sound: RawResourceAndroidNotificationSound(sound),
//         showProgress: true,
//         enableVibration: true,
//         styleInformation: BigTextStyleInformation(''),
//         priority: Priority.high,
//         ticker: 'test ticker');
//
//     var iOSChannelSpecifics = IOSNotificationDetails();
//     var MACChannelSpecifics = MacOSNotificationDetails();
//     var platformChannelSpecifics = NotificationDetails(android:
//     androidPlatformChannelSpecifics, iOS: iOSChannelSpecifics, macOS:MACChannelSpecifics);
//     await flutterLocalNotificationsPlugin
//         .show(0, title, body, platformChannelSpecifics, payload: 'test');
//   }
//
//   Future onSelectNotification(String payload) async {
//     showDialog(
//       context: context,
//       builder: (_) {
//         return new AlertDialog(
//           title: Text("Verification Code"),
//           content: Text("${widget.verificationCode}"),
//         );
//       },
//     );
//   }
//
//   void showNotification(String title, String body) async {
//     await _demoNotification(title, body);
//   }
//
//   void phoneNumberCheckerForNotification(BuildContext context) {
//     if(widget.phoneNumber.startsWith("92")){
//       // Notification
//       var initializationSettingsAndroid = new AndroidInitializationSettings('@mipmap/ic_launcher');
//       var initializationSettingsIOS = new IOSInitializationSettings();
//       var initializationSettingsMAC = new MacOSInitializationSettings();
//       var initializationSettings = new InitializationSettings(
//           android: initializationSettingsAndroid,
//           iOS: initializationSettingsIOS,
//           macOS: initializationSettingsMAC
//       );
//       flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: onSelectNotification);
//       showNotification("Verification Code", "Your verification code for the Wee Scooter App is: ${widget.verificationCode}");
//     }else print("Do nothing for the notification");
//   }
// }
